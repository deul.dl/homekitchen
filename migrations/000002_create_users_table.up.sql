CREATE TABLE IF NOT EXISTS users (
  "uuid" uuid PRIMARY KEY NOT NULL,
  "phone_number" varchar(20) NOT NULL UNIQUE,
  "email" varchar(40) NOT NULL,
  "password_hash" varchar(255) NOT NULL,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);
