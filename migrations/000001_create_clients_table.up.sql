CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS "clients" (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "phone_number" varchar(20) NOT NULL UNIQUE,
  "code" varchar(6) NOT NULL,
  "divice_token" VARCHAR(255),
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);
