ALTER TABLE stories
DROP COLUMN started_at,
DROP COLUMN ended_at;

ALTER TABLE stories
  ADD started_at timestamp with time zone,
  ADD ended_at timestamp with time zone;
