ALTER TABLE stories
DROP COLUMN started_at,
DROP COLUMN ended_at;

ALTER TABLE stories
  ADD started_at INTEGER,
  ADD ended_at INTEGER;
