ALTER TABLE stories
DROP COLUMN is_delivery;

DROP TABLE IF EXISTS delivery_prices;

DROP TABLE IF EXISTS store_deliveries;
