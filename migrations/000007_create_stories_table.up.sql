CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS "stories" (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "user_uuid" uuid NOT NULL,
  "name" varchar(255) NOT NULL,
  "description" varchar(255) NOT NULL,
  "image_source" varchar(255) NOT NULL,
  "phone_number" varchar(255) NOT NULL,
  "is_turn_on" BOOLEAN NOT NULL,
  "started_at" timestamp with time zone,
  "ended_at" timestamp with time zone,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);
