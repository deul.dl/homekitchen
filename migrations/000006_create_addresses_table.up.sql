CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS "addresses" (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "customer_uuid" uuid NOT NULL,
  "country" varchar(255),
  "city" varchar(255) NOT NULL,
  "porch" varchar(255),
  "street" varchar(255) NOT NULL,
  "house" varchar(255),
  "apartment" varchar(255),
  "is_primary" BOOLEAN NOT NULL,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now(),
  "deleted_at" timestamp with time zone
);
