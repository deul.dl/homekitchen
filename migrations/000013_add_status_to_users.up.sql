ALTER TABLE "users"
ADD COLUMN "status" VARCHAR(35);


UPDATE users SET "status" = 'in_moderation';


ALTER TABLE "users"
ALTER COLUMN "status" SET NOT NULL;
