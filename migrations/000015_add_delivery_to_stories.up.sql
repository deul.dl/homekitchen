CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

ALTER TABLE "stories"
ADD COLUMN "is_delivery" BOOLEAN DEFAULT false;

CREATE TABLE IF NOT EXISTS store_deliveries (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "store_uuid" uuid NOT NULL,
  "user_uuid" uuid NOT NULL,
  "type" VARCHAR(25) NOT NULL DEFAULT 'hk_delivery_service',
  "price" REAL,
  "delivery_price_uuid" character varying(36),
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS delivery_prices (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "country_code" VARCHAR(5) NOT NULL,
  "country" VARCHAR(155) NOT NULL,
  "city" VARCHAR(155) NOT NULL,
  "price" REAL,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);

INSERT INTO delivery_prices
  ("country_code", "country", "city", "price")
  VALUES ('KZ', 'Kazachstan', 'Topar', 250);

INSERT INTO
  store_deliveries ("store_uuid", "user_uuid") 
    SELECT uuid, user_uuid
      FROM stories;

UPDATE store_deliveries
SET delivery_price_uuid = delivery_prices.uuid
FROM delivery_prices;
