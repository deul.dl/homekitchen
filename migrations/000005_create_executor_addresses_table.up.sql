CREATE TABLE IF NOT EXISTS "executor_addresses" (
  "uuid" uuid PRIMARY KEY NOT NULL,
  "city" varchar(255) NOT NULL,
  "timezone" varchar(10) NOT NULL,
  "longitude" DECIMAL,
  "latitude" DECIMAL,
  "location_code" varchar(20),
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);
