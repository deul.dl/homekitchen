CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS orders (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "status" varchar(20) NOT NULL,
  "time" timestamp with time zone NOT NULL,
  "executor_uuid" uuid NOT NULL,
  "customer_uuid" uuid NOT NULL,
  "store_uuid" uuid NOT NULL,
  "total_price" REAL NOT NULL,
  "address_uuid" uuid NOT NULL,
  "description" varchar(560),
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now(),
  "deleted_at" timestamp with time zone
);

CREATE TABLE IF NOT EXISTS order_statuses (
  "id" serial PRIMARY KEY,
  "order_uuid" uuid NOT NULL,
  "status" varchar(20) NOT NULL,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);
