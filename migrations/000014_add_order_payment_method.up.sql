CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS order_payment_methods (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "order_uuid" uuid NOT NULL,
  "total_price" REAL NOT NULL,
  "discount" REAL NOT NULL,
  "delivery_price" REAL NOT NULL,
  "amount" REAL NOT NULL,
  "type" varchar(40) NOT NULL,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);
