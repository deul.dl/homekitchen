CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS roles (
  "id" serial PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  created_at timestamp with time zone NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS user_roles (
  role_id INTEGER NOT NULL,
  user_uuid uuid NOT NULL
);

INSERT INTO roles (name) VALUES ('Administation');
INSERT INTO roles (name) VALUES ('Executor');
