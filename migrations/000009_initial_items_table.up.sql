CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS items (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "user_uuid" uuid NOT NULL,
  "store_uuid" uuid NOT NULL,
  "name" varchar(255) NOT NULL,
  "description" varchar(255),
  "price" REAL NOT NULL,
  "cost" REAL NOT NULL,
  "category_id" INTEGER NOT NULL,
  "is_turn_on" BOOLEAN NOT NULL,
  "image_source" varchar(255),
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now(),
  "deleted_at" timestamp with time zone
);
