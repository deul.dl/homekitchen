ALTER TABLE "order_items" ADD "cost" REAL NOT NULL DEFAULT 0;
ALTER TABLE "order_payment_methods" ADD "total_cost" REAL DEFAULT 0;
