CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS order_items (
  "uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "order_uuid" uuid NOT NULL,
  "item_uuid" uuid NOT NULL,
  "name" varchar(255) NOT NULL,
  "price" REAL NOT NULL,
  "count" INTEGER NOT NULL,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);
