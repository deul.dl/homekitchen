CREATE TABLE IF NOT EXISTS categories (
  id serial PRIMARY KEY,
  "position" INTEGER  NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "updated_at" timestamp with time zone NOT NULL DEFAULT now(),
  "created_at" timestamp with time zone NOT NULL DEFAULT now()
);

INSERT INTO categories (name, position) VALUES ('Pizzas', 1);
INSERT INTO categories (name, position) VALUES ('Shawarmas', 2);
INSERT INTO categories (name, position) VALUES ('Sushis', 3);
INSERT INTO categories (name, position) VALUES ('Hamburgers', 4);
INSERT INTO categories (name, position) VALUES ('Cakes', 5);
INSERT INTO categories (name, position) VALUES ('Desert', 6);
INSERT INTO categories (name, position) VALUES ('Drinks', 7);
INSERT INTO categories (name, position) VALUES ('Snacks', 8);
INSERT INTO categories (name, position) VALUES ('FirstMeals', 9);
INSERT INTO categories (name, position) VALUES ('SecondMeals', 10);
INSERT INTO categories (name, position) VALUES ('MainCourses', 11);
