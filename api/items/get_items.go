package items

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service"
)

type ItemsOutput struct {
	Items []ItemOutput `json:"items"`
}

func getItemsByParams(w http.ResponseWriter, r *http.Request) {
	var field string
	for key := range r.URL.Query() {
		field = key
	}
	value := r.URL.Query().Get(field)

	items, err := store_service.GetItemService(r.Context()).
		GetItemsByField(field, value)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}
	var itemsOutput []ItemOutput
	for _, item := range items {
		itemsOutput = append(itemsOutput, ItemOutput{
			UUID:        item.UUID,
			UserUUID:    item.UserUUID,
			StoreUUID:   item.StoreUUID,
			Name:        item.Name,
			Description: item.Description,
			Price:       item.Price,
			Cost:        item.Cost,
			ImageSource: item.ImageSource.String,
			IsTurnOn:    item.IsTurnOn,
			UpdatedAt:   item.UpdatedAt,
			CategoryID:  item.CategoryID,
		})
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &ItemsOutput{itemsOutput},
		Message: "Got items were successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
