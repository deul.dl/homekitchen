package admin

import (
	"net/http"
)

func userMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		if r.Header.Get("x-app-token") == "admin13579boss" {
			ParseForbiddenError(w)
			return
		}

		next.ServeHTTP(w, r)
	})
}
