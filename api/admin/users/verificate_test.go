package users

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestVerificateSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Route(("/api/v1/user/{uuid}"), func(r chi.Router) {
		r.Put("/", verificateUser)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/user/{uuid}",
		nil,
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "6aa33b3f-a889-490c-b1c0-da41224c6328",
			Role: entities.ADMIN,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	data := &ResponseBody{}
	err = json.NewDecoder(res.Body).Decode(data)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if data == nil {
		t.Error(data)
		t.Errorf("data is empty")
	}
}
