package users

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/pkg/user_service"
)

func verificateUser(w http.ResponseWriter, r *http.Request) {
	userUUID := chi.URLParam(r, "uuid")

	if err := user_service.New(r.Context()).
		Verificate(userUUID); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{
		Data: userUUID, Message: "Updated status of user is successfully"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)

}
