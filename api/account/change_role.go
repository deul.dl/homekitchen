package account

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/homekitchen/homekitchen/pkg/auth_service"
)

type ChangeRoleInput struct {
	Role string `json:"role"`
}

func changeRole(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &ChangeRoleInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}

	cookie, err := r.Cookie("csrfToken")
	if err != nil {
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*ChangeRoleInput)
	authSuccess, err := auth_service.New(r.Context()).
		ChangeRole(cookie.Value, data.Role)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie = &http.Cookie{Name: "csrfToken", Path: "/", Value: authSuccess.Token, Expires: expiration}
	http.SetCookie(w, cookie)

	encode, err := json.Marshal(&ResponseBody{Data: authSuccess})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
