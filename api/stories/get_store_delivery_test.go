package stories

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/joho/godotenv"
)

func TestGetStoreDelivery(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Get("/api/v1/store/{storeUUID}", getStoreDelivery)
	ts := httptest.NewServer(r)
	defer ts.Close()

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	req, err := http.NewRequest(
		"GET",
		ts.URL+"/api/v1/store/48531a0e-5f42-4d90-980b-17522f678bd7",
		nil,
	)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}
	responseBody := &ResponseBody{Data: &StoreOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	t.Log(responseBody)
}
