package stories

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetStoreByCategoryID(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Get("/api/v1/stories/category/{categoryID}", getStories)
	ts := httptest.NewServer(r)
	defer ts.Close()

	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "store",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	req, err := http.NewRequest(
		"GET",
		ts.URL+"/api/v1/stories/category/1?city=Mountain%20View",
		nil,
	)
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}
	responseBody := &ResponseBody{Data: &StoriesOutput{}}
	if err = json.Unmarshal(body, responseBody); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if len(responseBody.Data.(*StoriesOutput).Stories) == 0 {
		t.Error("Stories is empty")
	}
}
