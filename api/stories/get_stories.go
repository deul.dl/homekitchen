package stories

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/pkg/store_service"
)

type StoriesOutput struct {
	Stories []*Store `json:"stories"`
}

func getStories(w http.ResponseWriter, r *http.Request) {
	categoryID, err := strconv.Atoi(chi.URLParam(r, "categoryID"))
	if err != nil {
		log.Println(err)
		ParseError(w, err)
	}
	city := r.URL.Query().Get("city")
	stories, err := store_service.New(r.Context()).
		FindStories(&store_service.StoreFilter{
			CategoryID: int64(categoryID),
			City:       city,
		})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}
	var storiesOutput []*Store

	for _, store := range stories {
		storiesOutput = append(storiesOutput, &Store{
			UUID:        store.UUID,
			Name:        store.Name,
			Description: store.Description.String,
			ImageSource: store.ImageSource,
			UserUUID:    store.UserUUID,
			StartedAt:   store.StartedAt.Int64,
			EndedAt:     store.EndedAt.Int64,
		})
	}

	encode, err := json.Marshal(
		&ResponseBody{Data: &StoriesOutput{storiesOutput}})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
