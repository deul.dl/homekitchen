package stories

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
)

type key string

const (
	MC_KEY    key = "mc_key"
	STORE_KEY key = "store_key"
)

type StoreOutput struct {
	Store *Store `json:"store"`
}

type Store struct {
	UUID        string `json:"uuid"`
	UserUUID    string `json:"userUUID"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ImageSource string `json:"imageSource"`
	IsTurnOn    bool   `json:"isTurnOn"`
	PhoneNumber string `json:"phoneNumber"`
	StartedAt   int64  `json:"startedAt"`
	EndedAt     int64  `json:"endedAt"`
}

type StoreDelivery struct {
	UUID              string  `json:"uuid"`
	StoreUUID         string  `json:"storeUUID"`
	UserUUID          string  `json:"userUUID"`
	Type              string  `json:"type"`
	Price             float64 `json:"price"`
	DeliveryPriceUUID string  `json:"deliveryPriceUUID"`
	CreatedAt         string  `json:"createdAt"`
	UpdatedAt         string  `json:"updatedAt"`
}

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func Initial(r chi.Router) {
	r.Route("/", func(r chi.Router) {
		r.Get("/category/{categoryID}", getStories)
		r.Get("/{storeUUID}", getStoreByUUID)
		r.Get("/{storeUUID}/delivery", getStoreDelivery)
	})
}
