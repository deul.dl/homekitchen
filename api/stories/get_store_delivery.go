package stories

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/pkg/store_service"
)

func getStoreDelivery(w http.ResponseWriter, r *http.Request) {
	storeUUID := chi.URLParam(r, "storeUUID")
	print(storeUUID)

	storeDelivery, err := store_service.New(r.Context()).
		GetStoreDeliveryByStoreUUID(storeUUID)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	storeDeliveryOutput := &StoreDelivery{
		UUID:              storeDelivery.UUID,
		StoreUUID:         storeDelivery.StoreUUID,
		UserUUID:          storeDelivery.UserUUID,
		Type:              storeDelivery.Type,
		Price:             storeDelivery.Price.Float64,
		DeliveryPriceUUID: storeDelivery.DeliveryPriceUUID.String,
		CreatedAt:         storeDelivery.CreatedAt.String(),
		UpdatedAt:         storeDelivery.UpdatedAt.String(),
	}

	encode, err := json.Marshal(
		&ResponseBody{Data: storeDeliveryOutput})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
