package stories

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/pkg/store_service"
)

func getStoreByUUID(w http.ResponseWriter, r *http.Request) {
	storeUUID := chi.URLParam(r, "storeUUID")

	store, err := store_service.New(r.Context()).GetStoreByField("uuid", storeUUID)
	if err != nil {
		log.Println(err)
		HandleNotFound(w, r)
		return
	}
	outputStore := &Store{
		UUID:        store.UUID,
		UserUUID:    store.UserUUID,
		Name:        store.Name,
		Description: store.Description.String,
		PhoneNumber: store.PhoneNumber,
		ImageSource: store.ImageSource,
		IsTurnOn:    store.IsTurnOn,
		StartedAt:   store.StartedAt.Int64,
		EndedAt:     store.EndedAt.Int64,
	}
	encode, err := json.Marshal(
		&ResponseBody{Data: &StoreOutput{Store: outputStore}})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
