package customer

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/api/customer/addresses"
	"github.com/homekitchen/homekitchen/api/customer/orders"

	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

		if mc["role"] != entities.CUSTOMER {
			ParseForbiddenError(w)
			return
		}

		ctx := context.WithValue(r.Context(), entities.USER_UUID, mc["uuid"])
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func Initial(r chi.Router) {
	r.Use(Middleware)
	r.Route("/orders", orders.Initial)
	r.Route("/addresses", addresses.Initial)
}
