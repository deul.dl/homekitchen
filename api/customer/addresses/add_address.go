package addresses

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/address_service"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
)

type AddAddressInput struct {
	Street    string `json:"street"`
	House     string `json:"house"`
	Apartment string `json:"apartment"`
	Porch     string `json:"porch"`
	City      string `json:"city"`
	Country   string `json:"country"`
	Primary   bool   `json:"primary"`
}

type AddressAddedOutput struct {
	Address *Address `json:"address"`
}

func addAddress(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &AddAddressInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*AddAddressInput)
	address := &entities.Address{
		CustomerUUID: mc["uuid"].(string),
		Street:       data.Street,
		House:        data.House,
		Apartment:    data.Apartment,
		Porch:        data.Porch,
		City:         data.City,
		Country:      data.Country,
		Primary:      data.Primary,
	}

	if err = address_service.
		New(r.Context()).
		AddAddress(address); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Data: &AddressAddedOutput{
		Address: &Address{
			UUID:         address.UUID,
			Street:       address.Street,
			House:        address.House,
			Apartment:    address.Apartment,
			Porch:        address.Porch,
			City:         address.City,
			Country:      address.Country,
			CustomerUUID: address.CustomerUUID,
			Primary:      address.Primary,
			CreatedAt:    address.CreatedAt,
			UpdatedAt:    address.CreatedAt,
			DeletedAt:    address.DeletedAt,
		}}})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)

}
