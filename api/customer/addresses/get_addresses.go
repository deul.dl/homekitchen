package addresses

import (
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/address_service"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
)

type AddressesOutput struct {
	Addresses *Address `json:"addresses"`
}

func getAddresses(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

	addresses, err := address_service.New(r.Context()).
		GetAddresses(mc["uuid"].(string))
	if err != nil {
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Data: copyAddresses(addresses)})
	if err != nil {
		ParseError(w, err)
		return
	}
	w.Write(encode)
}

func copyAddresses(addresses entities.Addresses) (addressesOutput []*Address) {
	for _, address := range addresses {
		addressOutput := &Address{
			UUID:         address.UUID,
			City:         address.City,
			Street:       address.Street,
			House:        address.House,
			Porch:        address.Porch,
			Apartment:    address.Apartment,
			CustomerUUID: address.CustomerUUID,
			Primary:      address.Primary,
			CreatedAt:    address.CreatedAt,
			UpdatedAt:    address.UpdatedAt,
			DeletedAt:    address.DeletedAt,
		}
		addressesOutput = append(addressesOutput, addressOutput)
	}
	return
}
