package addresses

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/pkg/address_service"
)

type PrimaryAddressSutOutput struct {
	UUID string `json:"uuid"`
}

func setPrimaryAddress(w http.ResponseWriter, r *http.Request) {
	addressUUID := chi.URLParam(r, "uuid")
	if err := address_service.New(r.Context()).SetPrimary(addressUUID); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &PrimaryAddressSutOutput{UUID: addressUUID},
		Message: "Set primary is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)

}
