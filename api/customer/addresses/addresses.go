package addresses

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/go-chi/chi"
)

type Address struct {
	UUID         string    `json:"uuid"`
	Street       string    `json:"street"`
	House        string    `json:"house"`
	Apartment    string    `json:"apartment"`
	Porch        string    `json:"porch"`
	City         string    `json:"city"`
	Country      string    `json:"country"`
	CustomerUUID string    `json:"customerUUID"`
	Primary      bool      `json:"primary"`
	CreatedAt    time.Time `json:"createdAt"`
	UpdatedAt    time.Time `json:"updatedAt"`
	DeletedAt    time.Time `json:"deletedAt"`
}

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func Initial(r chi.Router) {
	r.Get("/", getAddresses)
	r.Post("/add-address", addAddress)
	r.Route("/{uuid}", func(r chi.Router) {
		r.Use(addressMiddleware)
		r.Put("/", updateAddress)
		r.Delete("/", removeAddress)
		r.Put("/primary", setPrimaryAddress)
	})
}
