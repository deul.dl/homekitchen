package addresses

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestSetPrimarySuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Route("/api/v1/addresses/{uuid}", func(r chi.Router) {
		r.Put("/", setPrimaryAddress)
	})

	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/addresses/c8cd63f8-8149-4fe0-9cc3-500f0be3c9e0",
		nil,
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "6aa33b3f-a889-490c-b1c0-da41224c6328",
			Role: entities.CUSTOMER,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	defer res.Body.Close()
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	data := &ResponseBody{Data: &PrimaryAddressSutOutput{}}
	err = json.NewDecoder(res.Body).Decode(data)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if data.Data.(*PrimaryAddressSutOutput).UUID == "" {
		t.Error("Address is not empty")
	}
}
