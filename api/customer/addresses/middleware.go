package addresses

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/pkg/address_service"
)

func addressMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := address_service.New(r.Context()).
			GetAddressByUUID(chi.URLParam(r, "uuid"))
		if err != nil {
			HandleNotFound(w, r)
			return
		}

		next.ServeHTTP(w, r)
	})
}
