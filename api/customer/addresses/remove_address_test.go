package addresses

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/auth_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestRemoveAddressSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Delete("/api/v1/addresses/f8e87af3-153f-467c-afdb-ea8cb55bd8cf", removeAddress)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"DELETE",
		ts.URL+"/api/v1/addresses/f8e87af3-153f-467c-afdb-ea8cb55bd8cf",
		nil,
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "6aa33b3f-a889-490c-b1c0-da41224c6328",
			Role: entities.CUSTOMER,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	defer res.Body.Close()
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	data := &ResponseBody{Data: &auth_service.AuthSuccess{}}
	err = json.NewDecoder(res.Body).Decode(data)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if data == nil {
		t.Error(data)
		t.Errorf("data is empty")
	}

}
