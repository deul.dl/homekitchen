package addresses

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/address_service"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
)

type UpdateAddressInput struct {
	UUID      string `json:"uuid"`
	Street    string `json:"street"`
	House     string `json:"house"`
	Apartment string `json:"apartment"`
	Porch     string `json:"porch"`
	City      string `json:"city"`
	Country   string `json:"country"`
	Primary   bool   `json:"primary"`
}

type AddressUpdatedOutput struct {
	Address *Address `json:"address"`
}

func updateAddress(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &UpdateAddressInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*UpdateAddressInput)

	address := &entities.Address{
		UUID:      data.UUID,
		Street:    data.Street,
		House:     data.House,
		Apartment: data.Apartment,
		Porch:     data.Porch,
		City:      data.City,
		Country:   data.Country,
		Primary:   data.Primary,
	}

	if err = address_service.
		New(r.Context()).
		UpdateAddress(address); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Data: &AddressUpdatedOutput{
		Address: &Address{
			UUID:         address.UUID,
			Street:       address.Street,
			House:        address.House,
			Apartment:    address.Apartment,
			Porch:        address.Porch,
			City:         address.City,
			Country:      address.Country,
			CustomerUUID: address.CustomerUUID,
			Primary:      address.Primary,
			CreatedAt:    address.CreatedAt,
			UpdatedAt:    address.CreatedAt,
			DeletedAt:    address.DeletedAt,
		}}})

	w.Write(encode)
}
