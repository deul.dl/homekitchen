package addresses

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/pkg/address_service"
)

type RemoveAddressOutput struct {
	UUID string `json:"uuid"`
}

func removeAddress(w http.ResponseWriter, r *http.Request) {
	uuid := chi.URLParam(r, "uuid")
	if err := address_service.New(r.Context()).RemoveAddress(uuid); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &RemoveAddressOutput{UUID: uuid},
		Message: "Deleted is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)

}
