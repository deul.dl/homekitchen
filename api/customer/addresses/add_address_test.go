package addresses

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestAddAddressSuccess(t *testing.T) {

	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	addressBody := map[string]interface{}{
		"street":    "Ponamareve",
		"house":     "house 15",
		"apartment": "32",
		"porch":     "2",
		"city":      "Topar",
		"country":   "Kazachstan",
		"primary":   true,
	}
	requestBody := map[string]map[string]interface{}{
		"data": addressBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Post("/api/v1/addresses/add-address", addAddress)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"POST",
		ts.URL+"/api/v1/addresses/add-address",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.CUSTOMER,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	defer res.Body.Close()
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	data := &ResponseBody{Data: &AddressAddedOutput{}}
	err = json.NewDecoder(res.Body).Decode(data)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	if data.Data.(*AddressAddedOutput).Address.UUID == "" {
		t.Error("Address is not empty")
	}
}
