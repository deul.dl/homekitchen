package orders

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type OrderStatusesOutput struct {
	OrderStatuses []*OrderStatusOutput `json:"orderStatuses"`
}

func getOrderStatuses(w http.ResponseWriter, r *http.Request) {
	order := r.Context().Value(ORDER_KEY).(*entities.Order)

	orderStatuses, err := order_service.New(r.Context()).
		GetOrderStatuses(order.UUID)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	orderStatusesOutput := copyOrderStatuses(orderStatuses)

	encode, err := json.Marshal(&ResponseBody{
		Data:    &OrderStatusesOutput{orderStatusesOutput},
		Message: "Got is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func copyOrderStatuses(orderStatuses entities.OrderStatuses) (orderStatusesOutput []*OrderStatusOutput) {
	for _, orderStatus := range orderStatuses {
		orderStatusesOutput = append(orderStatusesOutput, &OrderStatusOutput{
			OrderUUID: orderStatus.OrderUUID,
			Status:    orderStatus.Status,
		})
	}

	return
}
