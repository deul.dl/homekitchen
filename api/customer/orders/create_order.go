package orders

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type CreateOrderInput struct {
	StoreUUID     string            `json:"storeUUID"`
	ExecutorUUID  string            `json:"executorUUID"`
	AddressUUID   string            `json:"addressUUID"`
	Description   string            `json:"description"`
	PaymentMethod string            `json:"paymentMethod"`
	Items         []*OrderItemInput `json:"items"`
}

type OrderItemInput struct {
	ItemUUID string  `json:"itemUUID"`
	Name     string  `json:"name"`
	Price    float64 `json:"price"`
	Count    int64   `json:"count"`
	Cost     float64 `json:"cost"`
}

func createOrder(w http.ResponseWriter, r *http.Request) {
	userUUID := r.Context().Value(entities.USER_UUID).(string)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		log.Print(err)
		return
	}

	requestData := &RequestBody{Data: &CreateOrderInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		log.Print(err)
		return
	}

	data := requestData.Data.(*CreateOrderInput)

	order := &entities.Order{
		StoreUUID:    data.StoreUUID,
		ExecutorUUID: data.ExecutorUUID,
		CustomerUUID: userUUID,
		AddressUUID:  handles.NewNullString(data.AddressUUID),
		Description:  data.Description,
		PaymentMethod: &entities.OrderPaymentMethod{
			Type: data.PaymentMethod,
		},
		OrderItems: copyOrderItemsCreated(data.Items),
	}

	if err := order_service.
		New(r.Context()).CreateOrder(order); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{
		Data: &OrderOutput{
			UUID:         order.UUID,
			StoreUUID:    order.StoreUUID,
			Status:       order.Status,
			Time:         order.Time,
			ExecutorUUID: order.ExecutorUUID,
			CustomerUUID: order.CustomerUUID,
			TotalPrice:   order.TotalPrice,
			AddressUUID:  order.AddressUUID.String,
			Description:  order.Description,
			CreatedAt:    order.CreatedAt,
			UpdatedAt:    order.UpdatedAt,
		},
		Message: "Created is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func copyOrderItemsCreated(itemsInput []*OrderItemInput) (items []*entities.OrderItem) {
	for _, item := range itemsInput {
		items = append(items, &entities.OrderItem{
			ItemUUID: item.ItemUUID,
			Name:     item.Name,
			Price:    item.Price,
			Count:    item.Count,
			Cost:     item.Cost,
		})
	}
	return items
}
