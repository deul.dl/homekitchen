package orders

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrderStatusesTest(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Use(orderMiddleware)
	r.Route("/api/v1/orders/{orderUUID}", func(r chi.Router) {
		r.Get("/statuses", getOrderStatuses)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "40a0b81f-94d5-4a0a-a283-6021ee36298a",
			Role: entities.CUSTOMER,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	req, err := http.NewRequest(
		"GET",
		ts.URL+"/api/v1/orders/30db6106-0bd7-4329-8f9e-d145f63cddb6/statuses",
		nil,
	)
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}
	responseBody := &ResponseBody{Data: &OrderStatusesOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if len(responseBody.Data.(*OrderStatusesOutput).OrderStatuses) == 0 {
		t.Error("Order Items is empty")
	}
	t.Log(responseBody)

}
