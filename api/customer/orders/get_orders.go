package orders

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type OrdersOutput struct {
	Orders []*OrderOutput `json:"orders"`
}

func getCustomerOrders(w http.ResponseWriter, r *http.Request) {
	orders, err := order_service.New(r.Context()).
		GetCustomerOrders()
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	ordersOutput := mapOrdersToOutput(orders)

	encode, err := json.Marshal(&ResponseBody{
		Data:    &OrdersOutput{Orders: ordersOutput},
		Message: "Got customer orders were successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func mapOrdersToOutput(orders entities.Orders) (ordersOutput []*OrderOutput) {
	for _, order := range orders {
		ordersOutput = append(ordersOutput, newOrderOutput(order))
	}

	return
}
