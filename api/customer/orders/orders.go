package orders

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type key string

const (
	MC_KEY    key = "mc_key"
	ORDER_KEY key = "order_key"
)

type OrderOutput struct {
	UUID         string    `json:"uuid"`
	StoreUUID    string    `json:"storeUUID"`
	Status       string    `json:"status"`
	Time         time.Time `json:"time"`
	ExecutorUUID string    `json:"executorUUID"`
	CustomerUUID string    `json:"customerUUID"`
	TotalPrice   float64   `json:"totalPrice"`
	AddressUUID  string    `json:"addressUUID"`
	Description  string    `json:"description"`

	CreatedAt time.Time       `json:"createdAt"`
	UpdatedAt time.Time       `json:"updatedAt"`
	Customer  *CustomerOutput `json:"customer"`
	Address   *AddressOutput  `json:"address"`
}

func newOrderOutput(order *entities.Order) *OrderOutput {
	orderOutput := &OrderOutput{
		UUID:         order.UUID,
		StoreUUID:    order.StoreUUID,
		CustomerUUID: order.CustomerUUID,
		ExecutorUUID: order.ExecutorUUID,
		Time:         order.Time,
		Status:       order.Status,
		TotalPrice:   order.TotalPrice,
		AddressUUID:  order.AddressUUID.String,
		Description:  order.Description,
		CreatedAt:    order.CreatedAt,
		UpdatedAt:    order.UpdatedAt,
	}

	if order.Address.UUID.Valid {
		orderOutput.Address = &AddressOutput{
			UUID:      order.Address.UUID.String,
			City:      order.Address.City.String,
			Street:    order.Address.Street.String,
			House:     order.Address.House.String,
			Porch:     order.Address.Porch.String,
			Apartment: order.Address.Apartment.String,
		}
	}

	return orderOutput
}

type CustomerOutput struct {
	PhoneNumber string `json:"phoneNumber"`
}

type AddressOutput struct {
	PhoneNumber string `json:"phoneNumber"`
	UUID        string `json:"uuid"`
	Street      string `json:"street"`
	House       string `json:"house"`
	Apartment   string `json:"apartment"`
	Porch       string `json:"porch"`
	City        string `json:"city"`
	Country     string `json:"country"`
}

type OrderItemOutput struct {
	OrderUUID string    `json:"orderUUID"`
	Count     int64     `json:"count"`
	ItemUUID  string    `json:"itemUUID"`
	Name      string    `json:"name"`
	Price     float64   `json:"price"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type OrderStatusOutput struct {
	OrderUUID string `json:"orderUUID"`
	Status    string `json:"status"`
}

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func orderMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		uuid := chi.URLParam(r, "orderUUID")
		order, err := order_service.New(r.Context()).
			GetStorage().
			GetOrderByUUID(uuid)
		if err != nil {
			log.Println(err)
			HandleNotFound(w, r)
			return
		}

		ctx := context.WithValue(r.Context(), ORDER_KEY, order)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func Initial(r chi.Router) {
	r.Get("/", getCustomerOrders)
	r.Post("/create", createOrder)
	r.Route("/{orderUUID}", func(r chi.Router) {
		r.Use(orderMiddleware)
		r.Put("/complete", completeOrder)
		r.Put("/cancel", cancelOrder)
		r.Get("/items", getOrderItems)
		r.Get("/statuses", getOrderStatuses)
		r.Get("/payment", getOrderPaymentMethod)
	})

}
