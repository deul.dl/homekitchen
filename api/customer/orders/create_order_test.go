package orders

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/api/test"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestCreateOrderSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	r := chi.NewRouter()
	r.Use(test.Middleware)
	r.Post("/create-order", createOrder)

	data := map[string]interface{}{
		"storeUUID":     "51c94f11-f606-4c65-9149-c6ddc5dbffd4",
		"executorUUID":  "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
		"addressUUID":   "c6f1d604-86ea-494c-b273-50417e7c4a8f",
		"description":   "1",
		"paymentMethod": entities.IN_CASH_TYPE,
		"items": []map[string]interface{}{
			{
				"itemUUID": "cc95b2e0-251d-49e0-bc12-ea217317d59f",
				"name":     "Item 1",
				"price":    123,
				"cost":     123,
				"count":    1,
			},
		},
	}
	requestBody := map[string]map[string]interface{}{
		"data": data,
	}
	res2B, _ := json.Marshal(requestBody)

	ts := httptest.NewServer(r)
	defer ts.Close()
	req, err := http.NewRequest(
		"POST",
		ts.URL+"/create-order",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.CUSTOMER,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := &http.Cookie{Name: "csrfToken", Path: "/", Value: token, Expires: expiration}
	req.AddCookie(cookie)
	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &OrderOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	t.Log(responseBody)
}

func TestCreateOrderWithoutAddressSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	r := chi.NewRouter()
	r.Use(test.Middleware)
	r.Post("/create-order", createOrder)

	data := map[string]interface{}{
		"storeUUID":     "51c94f11-f606-4c65-9149-c6ddc5dbffd4",
		"executorUUID":  "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
		"description":   "1",
		"paymentMethod": entities.IN_CASH_TYPE,
		"items": []map[string]interface{}{
			{
				"itemUUID": "cc95b2e0-251d-49e0-bc12-ea217317d59f",
				"name":     "Item 1",
				"price":    123,
				"count":    1,
			},
		},
	}
	requestBody := map[string]map[string]interface{}{
		"data": data,
	}
	res2B, _ := json.Marshal(requestBody)

	ts := httptest.NewServer(r)
	defer ts.Close()
	req, err := http.NewRequest(
		"POST",
		ts.URL+"/create-order",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.CUSTOMER,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := &http.Cookie{Name: "csrfToken", Path: "/", Value: token, Expires: expiration}
	req.AddCookie(cookie)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &OrderOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	t.Log(responseBody)
}
