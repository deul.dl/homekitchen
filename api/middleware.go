package api

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/db"
)

type key string

const (
	MC_KEY key = "mc_key"
)

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		log.Println("Open Connections:", db.Pool.Stats().OpenConnections)
		log.Println("In Use:", db.Pool.Stats().InUse)

		next.ServeHTTP(w, r)
	})
}
