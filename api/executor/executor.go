package executor

import (
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/api/executor/items"
	"github.com/homekitchen/homekitchen/api/executor/orders"
	"github.com/homekitchen/homekitchen/api/executor/statistic"
	"github.com/homekitchen/homekitchen/api/executor/stories"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type StoreOutput struct {
	Store *Store
}

type Store struct {
	UUID        string
	UserUUID    string
	Name        string
	Description string
	ImageSource string
	IsTurnOn    bool
	PhoneNumber string
}

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

		if mc["role"] != entities.EXECUTOR {
			ParseForbiddenError(w)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func Initial(r chi.Router) {
	r.Use(Middleware)
	r.Route("/orders", orders.Initial)
	r.Route("/stories", stories.Initial)
	r.Route("/items", items.Initial)
	r.Route("/statistic", statistic.Initial)
}
