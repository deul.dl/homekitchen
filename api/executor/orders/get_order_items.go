package orders

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type OrderItemsOutput struct {
	OrderItems []*OrderItemOutput `json:"orderItems"`
}

func getOrderItems(w http.ResponseWriter, r *http.Request) {
	order := r.Context().Value(ORDER_KEY).(*entities.Order)

	orderItems, err := order_service.New(r.Context()).
		GetOrderItemsByOrderUUID(order.UUID)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	orderItemsOutput := mapOrderItemsToOutput(orderItems)

	encode, err := json.Marshal(&ResponseBody{
		Data:    &OrderItemsOutput{orderItemsOutput},
		Message: "Got items of order were successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func mapOrderItemsToOutput(orderItems entities.OrderItems) (orderItemsOutput []*OrderItemOutput) {
	for _, item := range orderItems {
		orderItemsOutput = append(orderItemsOutput, &OrderItemOutput{
			OrderUUID: item.OrderUUID,
			Price:     item.Price,
			Name:      item.Name,
			Count:     item.Count,
			ItemUUID:  item.ItemUUID,
			CreatedAt: item.CreatedAt,
			UpdatedAt: item.UpdatedAt,
		})
	}

	return
}
