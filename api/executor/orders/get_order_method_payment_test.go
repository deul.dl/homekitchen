package orders

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrderPaymentMethodSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Route("/api/v1/orders/{orderUUID}", func(r chi.Router) {
		r.Use(orderMiddleware)
		r.Put("/payment-method", getOrderPaymentMethod)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/orders/2dc04d39-6e04-41f7-ba1f-129f48bf9fec/payment-method",
		nil,
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.CUSTOMER,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &OrderPaymentMethodOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	if responseBody.Data.(*OrderPaymentMethodOutput).OrderPaymentMethod == nil {
		t.Error("OrderPaymentMethod is empty")
	}
	t.Log(responseBody)
}
