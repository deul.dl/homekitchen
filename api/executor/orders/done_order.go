package orders

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

func doneOrder(w http.ResponseWriter, r *http.Request) {
	order := r.Context().Value(ORDER_KEY).(*entities.Order)

	if err := order_service.
		New(r.Context()).DoneOrder(order); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{
		Data: &OrderStatusOutput{
			OrderUUID: order.UUID,
			Status:    entities.DONE_STATUS,
		},
		Message: "Done is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
