package orders

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestStartOrderSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()
	r.Route("/api/v1/orders/{orderUUID}", func(r chi.Router) {
		r.Use(orderMiddleware)
		r.Put("/start", startOrder)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/orders/8a1d4632-4cc8-40d7-a494-fa24e3c2f76d/start",
		nil,
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "fc9fe6d0-5c71-48ca-bc0c-bc5c754925a1",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &OrderStatusOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	t.Log(responseBody)
}
