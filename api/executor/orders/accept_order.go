package orders

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

func acceptOrder(w http.ResponseWriter, r *http.Request) {
	order := r.Context().Value(ORDER_KEY).(*entities.Order)

	if err := order_service.
		New(r.Context()).AcceptOrder(order); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{
		Data: &OrderStatusOutput{
			OrderUUID: order.UUID,
			Status:    entities.ACCEPTED_STATUS,
		},
		Message: "Accepted is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
