package orders

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/order_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type OrderPaymentMethodOutput struct {
	OrderPaymentMethod *OrderPaymentMethod `json:"orderPaymentMethod"`
}

type OrderPaymentMethod struct {
	UUID          string  `json:"uuid"`
	OrderUUID     string  `json:"orderUUID"`
	Type          string  `json:"type"`
	Amount        float64 `json:"amount"`
	TotalPrice    float64 `json:"totalPrice"`
	DeliveryPrice float64 `json:"deliveryPrice"`
}

func getOrderPaymentMethod(w http.ResponseWriter, r *http.Request) {
	order := r.Context().Value(ORDER_KEY).(*entities.Order)

	orderPaymentMethod, err := order_service.New(r.Context()).
		GetOrderPaymentMethod(order.UUID)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	orderPaymentMethodOutput := &OrderPaymentMethod{
		UUID:          orderPaymentMethod.UUID,
		TotalPrice:    orderPaymentMethod.TotalPrice,
		Amount:        orderPaymentMethod.Amount,
		OrderUUID:     orderPaymentMethod.OrderUUID,
		Type:          orderPaymentMethod.Type,
		DeliveryPrice: orderPaymentMethod.DeliveryPrice,
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &OrderPaymentMethodOutput{orderPaymentMethodOutput},
		Message: "Got is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
