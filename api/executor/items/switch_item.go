package items

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

func switchItem(w http.ResponseWriter, r *http.Request) {
	item := r.Context().Value(ITEM_KEY).(*entities.Item)
	var outputItem *ItemOutput
	if item.IsTurnOn {
		if err := store_service.
			GetItemService(r.Context()).TurnOff(item.UUID); err != nil {
			log.Println(err)
			ParseError(w, err)
			return
		}
		outputItem = &ItemOutput{
			UUID:        item.UUID,
			UserUUID:    item.UserUUID,
			StoreUUID:   item.StoreUUID,
			Name:        item.Name,
			Description: item.Description,
			Price:       item.Price,
			CategoryID:  item.CategoryID,
			IsTurnOn:    false,
			ImageSource: item.ImageSource.String,
		}
	} else {
		if err := store_service.
			GetItemService(r.Context()).TurnOn(item.UUID); err != nil {
			log.Println(err)
			ParseError(w, err)
			return
		}
		outputItem = &ItemOutput{
			UUID:        item.UUID,
			UserUUID:    item.UserUUID,
			StoreUUID:   item.StoreUUID,
			Description: item.Description,
			Name:        item.Name,
			Price:       item.Price,
			CategoryID:  item.CategoryID,
			IsTurnOn:    true,
			ImageSource: item.ImageSource.String,
		}
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    outputItem,
		Message: "Switched is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
