package items

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestAddItem(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	if err != nil {
		// d9739d52-2fcd-452b-a004-bcbfd0dbb41d
		t.Error(err)
		return
	}

	data := map[string]interface{}{
		"name":        "Item",
		"description": "my item",
		"price":       1000,
		"cost":        500,
		"is_turn_on":  true,
		"categoryId":  1,
		"storeUUID":   uuid.New().String(),
	}
	requestBody := map[string]map[string]interface{}{
		"data": data,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Use(ItemsMiddleware)
	r.Post("/api/v1/items/add", addItem)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"POST",
		ts.URL+"/api/v1/items/add",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &ItemOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	t.Log(responseBody)
}
