package items

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/store_service"
)

type key string

const ITEM_KEY key = "item_key"

type ItemOutput struct {
	UUID        string    `json:"uuid"`
	UserUUID    string    `json:"userUUID"`
	CategoryID  int64     `json:"categoryID"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Price       float64   `json:"price"`
	Cost        float64   `json:"cost"`
	ImageSource string    `json:"imageSource"`
	IsTurnOn    bool      `json:"isTurnOn"`
	StoreUUID   string    `json:"storeUUID"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedAt"`
}

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func ItemsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

		if _, err := store_service.New(r.Context()).
			GetStoreByUserUUID(mc["uuid"].(string)); err != nil {
			ParseForbiddenError(w)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func ItemMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

		uuid := chi.URLParam(r, "itemUUID")
		ctx := context.WithValue(r.Context(), "user_uuid", mc["uuid"])
		item, err := store_service.GetItemService(ctx).
			GetItemByUUID(uuid)
		if err != nil {
			log.Print(err)
			log.Print("!SIGNAL!")
			HandleNotFound(w, r)
			return
		}

		ctx = context.WithValue(ctx, ITEM_KEY, item)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func Initial(r chi.Router) {
	r.Use(ItemsMiddleware)
	r.Get("/", getItems)
	r.Post("/add", addItem)
	r.Route("/{itemUUID}", func(r chi.Router) {
		r.Use(ItemMiddleware)
		r.Put("/", updateItem)
		r.Put("/upload", updateImage)
		r.Put("/switch", switchItem)
	})
}
