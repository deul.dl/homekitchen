package items

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type AddItemInput struct {
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
	Cost        float64 `json:"cost"`
	IsTurnOn    bool    `json:"isTurnOn"`
	CategoryID  int64   `json:"categoryId"`
	StoreUUID   string  `json:"storeUUID"`
}

func addItem(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		log.Print(err)
		return
	}

	requestData := &RequestBody{Data: &AddItemInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		log.Print(err)
		return
	}

	data := requestData.Data.(*AddItemInput)

	item := &entities.Item{
		UserUUID:    mc["uuid"].(string),
		Name:        data.Name,
		Description: data.Description,
		Price:       data.Price,
		Cost:        data.Cost,
		IsTurnOn:    data.IsTurnOn,
		CategoryID:  data.CategoryID,
		StoreUUID:   data.StoreUUID,
	}

	if err = store_service.GetItemService(r.Context()).AddItem(item); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	outputItem := &ItemOutput{
		UUID:        item.UUID,
		UserUUID:    item.UserUUID,
		StoreUUID:   item.StoreUUID,
		Name:        item.Name,
		Description: item.Description,
		Price:       item.Price,
		Cost:        item.Cost,
		ImageSource: item.ImageSource.String,
		IsTurnOn:    item.IsTurnOn,
		UpdatedAt:   item.UpdatedAt,
		CategoryID:  item.CategoryID,
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    outputItem,
		Message: "Added is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
