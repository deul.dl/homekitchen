package items

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/homekitchen/homekitchen/internal/filehelper"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

func updateImage(w http.ResponseWriter, r *http.Request) {
	item := r.Context().Value(ITEM_KEY).(*entities.Item)

	source, err := filehelper.UploadFile(
		r,
		fmt.Sprintf("%s/store/items", item.UserUUID),
		"original_"+time.Now().Format("20060102_15_04_05")+"_"+item.UUID+".jpg",
		item.ImageSource.String,
	)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err = store_service.GetItemService(r.Context()).
		UpdateItemImage(item.UUID, source); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	outputItem := &ItemOutput{
		UUID:        item.UUID,
		UserUUID:    item.UserUUID,
		StoreUUID:   item.StoreUUID,
		Name:        item.Name,
		Description: item.Description,
		Price:       item.Price,
		ImageSource: source,
		IsTurnOn:    item.IsTurnOn,
		CategoryID:  item.CategoryID,
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    outputItem,
		Message: "Uploded is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
