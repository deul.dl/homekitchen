package items

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type UpdateItemInput struct {
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
	Cost        float64 `json:"cost"`
	IsTurnOn    bool    `json:"isTurnOn"`
	CategoryID  int64   `json:"categoryId"`
}

func updateItem(w http.ResponseWriter, r *http.Request) {
	item := r.Context().Value(ITEM_KEY).(*entities.Item)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		log.Print(err)
		return
	}

	requestData := &RequestBody{Data: &UpdateItemInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		log.Print(err)
		return
	}

	data := requestData.Data.(*UpdateItemInput)

	item.Name = data.Name
	item.Description = data.Description
	item.CategoryID = data.CategoryID
	item.Price = data.Price
	item.Cost = data.Cost
	fmt.Println(data.Cost)
	if err = store_service.
		GetItemService(r.Context()).
		UpdateItem(item); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	outputItem := &ItemOutput{
		UUID:        item.UUID,
		UserUUID:    item.UserUUID,
		StoreUUID:   item.StoreUUID,
		Name:        item.Name,
		Description: item.Description,
		Price:       item.Price,
		Cost:        item.Cost,
		ImageSource: item.ImageSource.String,
		IsTurnOn:    item.IsTurnOn,
		UpdatedAt:   item.UpdatedAt,
		CategoryID:  item.CategoryID,
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    outputItem,
		Message: "Updated is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
