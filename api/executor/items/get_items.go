package items

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/items"
)

type ItemsOutput struct {
	Items []ItemOutput `json:"items"`
}

func getItems(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

	items, err := store_service.GetItemService(r.Context()).
		FindItems(&items.Filter{
			UserUUID: mc["uuid"].(string),
		})
	if err != nil {
		ParseError(w, err)
		return
	}

	var itemsOutput []ItemOutput
	for _, item := range items {
		itemsOutput = append(itemsOutput, ItemOutput{
			UUID:        item.UUID,
			UserUUID:    item.UserUUID,
			StoreUUID:   item.StoreUUID,
			Name:        item.Name,
			Description: item.Description,
			Price:       item.Price,
			Cost:        item.Cost,
			ImageSource: item.ImageSource.String,
			IsTurnOn:    item.IsTurnOn,
			UpdatedAt:   item.UpdatedAt,
			CategoryID:  item.CategoryID,
		})
	}
	fmt.Println(itemsOutput)
	encode, err := json.Marshal(&ResponseBody{
		Data:    &ItemsOutput{Items: itemsOutput},
		Message: "Got items were successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
