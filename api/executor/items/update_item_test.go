package items

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateItemSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	data := map[string]interface{}{
		"name":        "Item Updated",
		"description": "my item",
		"price":       1000,
		"cost":        1000,
		"isTurnOn":    true,
		"categoryId":  1,
	}
	requestBody := map[string]map[string]interface{}{
		"data": data,
	}
	res2B, _ := json.Marshal(requestBody)
	r := chi.NewRouter()
	r.Route("/api/v1/items/{itemUUID}", func(r chi.Router) {
		r.Use(ItemMiddleware)
		r.Put("/", updateItem)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/items/7ccbd474-511b-49bd-80ff-25f35bc7680a",
		bytes.NewBuffer(res2B),
	)

	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &ItemOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	t.Log(responseBody)
}
