package stories

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestCreateStoreSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	path := "../../../assets/images/placeholder.jpg"
	file, err := os.Open(path)
	if err != nil {
		t.Error(err)
		return
	}

	defer file.Close()
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	if err != nil {
		writer.Close()
		t.Error(err)
		return
	}
	io.Copy(part, file)
	writer.WriteField("name", "Request Store 1")
	writer.WriteField("description", "Description")
	writer.WriteField("phoneNumber", "+77056278085")
	writer.WriteField("startedAt", "3600")
	writer.WriteField("endedAt", "7200")
	writer.Close()

	r := chi.NewRouter()
	r.Use(StoriesMiddleware)
	r.Post("/api/v1/stories/create-store", createStore)

	ts := httptest.NewServer(r)
	defer ts.Close()

	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	req, err := http.NewRequest(
		"POST",
		ts.URL+"/api/v1/stories/create-store",
		body,
	)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	req.Header.Add(
		"x-app-token",
		token,
	)
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		t.Errorf("error %v", res.Body)
		return
	}

	t.Log(body.String())
}
