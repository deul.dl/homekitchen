package stories

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type UpdateStoreInput struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	PhoneNumber string `json:"phoneNumber"`
	StartedAt   int64  `json:"startedAt"`
	EndedAt     int64  `json:"endedAt"`
}

func updateStore(w http.ResponseWriter, r *http.Request) {
	store := r.Context().Value(STORE_KEY).(*entities.Store)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &UpdateStoreInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}
	data := requestData.Data.(*UpdateStoreInput)

	store = &entities.Store{
		UUID:        store.UUID,
		UserUUID:    store.UserUUID,
		Name:        data.Name,
		Description: handles.NewNullString(data.Description),
		PhoneNumber: data.PhoneNumber,
		ImageSource: store.ImageSource,
		IsTurnOn:    store.IsTurnOn,
		StartedAt:   handles.NewNullInt64(data.StartedAt),
		EndedAt:     handles.NewNullInt64(data.EndedAt),
	}

	if err = store_service.
		New(r.Context()).UpdateStore(store); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	outputStore := &Store{
		UUID:        store.UUID,
		UserUUID:    store.UserUUID,
		Name:        store.Name,
		Description: store.Description.String,
		PhoneNumber: store.PhoneNumber,
		ImageSource: store.ImageSource,
		IsTurnOn:    store.IsTurnOn,
		StartedAt:   store.StartedAt.Int64,
		EndedAt:     store.EndedAt.Int64,
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &StoreOutput{Store: outputStore},
		Message: "Updated is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
