package stories

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

func switchStore(w http.ResponseWriter, r *http.Request) {
	store := r.Context().Value(STORE_KEY).(*entities.Store)
	var outputStore *Store
	if store.IsTurnOn {
		if err := store_service.
			New(r.Context()).TurnOff(store.UUID); err != nil {
			log.Println(err)
			ParseError(w, err)
			return
		}
		outputStore = &Store{
			UUID:        store.UUID,
			UserUUID:    store.UserUUID,
			Name:        store.Name,
			Description: store.Description.String,
			PhoneNumber: store.PhoneNumber,
			IsTurnOn:    false,
			ImageSource: store.ImageSource,
			StartedAt:   store.StartedAt.Int64,
			EndedAt:     store.EndedAt.Int64,
		}
	} else {
		if err := store_service.
			New(r.Context()).TurnOn(store.UUID); err != nil {
			log.Println(err)
			ParseError(w, err)
			return
		}
		outputStore = &Store{
			UUID:        store.UUID,
			UserUUID:    store.UserUUID,
			Name:        store.Name,
			Description: store.Description.String,
			PhoneNumber: store.PhoneNumber,
			IsTurnOn:    true,
			ImageSource: store.ImageSource,
			StartedAt:   store.StartedAt.Int64,
			EndedAt:     store.EndedAt.Int64,
		}
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &StoreOutput{Store: outputStore},
		Message: "Switched is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
