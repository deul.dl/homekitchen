package stories

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type key string

const STORE_KEY key = "store_key"

type StoreOutput struct {
	Store *Store `json:"store"`
}

type Store struct {
	UUID        string `json:"uuid"`
	UserUUID    string `json:"userUUID"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ImageSource string `json:"imageSource"`
	IsTurnOn    bool   `json:"isTurnOn"`
	PhoneNumber string `json:"phoneNumber"`
	StartedAt   int64  `json:"startedAt"`
	EndedAt     int64  `json:"endedAt"`
}

type StoreDelivery struct {
	UUID              string  `json:"uuid"`
	StoreUUID         string  `json:"storeUUID"`
	UserUUID          string  `json:"userUUID"`
	Type              string  `json:"type"`
	Price             float64 `json:"price"`
	DeliveryPriceUUID string  `json:"deliveryPriceUUID"`
	CreatedAt         string  `json:"createdAt"`
	UpdatedAt         string  `json:"updatedAt"`
}

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func StoriesMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

		if _, err := users.New(r.Context()).
			GetUserByUUID(mc["uuid"].(string)); err != nil {
			ParseForbiddenError(w)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func StoreMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)
		store, err := store_service.New(r.Context()).
			GetStoreByUserUUID(mc["uuid"].(string))
		if err != nil {
			HandleNotFound(w, r)
			return
		}

		ctx := context.WithValue(r.Context(), STORE_KEY, store)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func Initial(r chi.Router) {

	r.Route("/", func(r chi.Router) {
		r.Use(StoriesMiddleware)
		r.Post("/create", createStore)

		r.Route("/store", func(r chi.Router) {
			r.Use(StoreMiddleware)
			r.Get("/", getStore)
			r.Put("/", updateStore)
			r.Put("/upload", updateImage)
			r.Put("/switch", switchStore)
			r.Get("/delivery", getStoreDelivery)
			r.Put("/delivery/price", updatePriceOfDelivery)
		})
	})
}
