package stories

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/homekitchen/homekitchen/internal/filehelper"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

func updateImage(w http.ResponseWriter, r *http.Request) {
	store := r.Context().Value(STORE_KEY).(*entities.Store)

	source, err := filehelper.
		UploadFile(r,
			fmt.Sprintf("%s/store", store.UserUUID),
			"original_"+time.Now().Format("20060102_15_04_05")+"_"+store.UUID+".jpg",
			store.ImageSource)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err = store_service.
		New(r.Context()).
		UpdateStoreImage(store.UUID, source); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	outputStore := &Store{
		UUID:        store.UUID,
		UserUUID:    store.UserUUID,
		Name:        store.Name,
		Description: store.Description.String,
		PhoneNumber: store.PhoneNumber,
		ImageSource: source,
		IsTurnOn:    store.IsTurnOn,
		StartedAt:   store.StartedAt.Int64,
		EndedAt:     store.EndedAt.Int64,
	}

	encode, err := json.Marshal(&ResponseBody{
		Data: StoreOutput{
			Store: outputStore,
		},
		Message: "Uploded is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
