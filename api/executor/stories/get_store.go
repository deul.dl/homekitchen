package stories

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

func getStore(w http.ResponseWriter, r *http.Request) {
	store := r.Context().Value(STORE_KEY).(*entities.Store)

	outputStore := &Store{
		UUID:        store.UUID,
		UserUUID:    store.UserUUID,
		Name:        store.Name,
		Description: store.Description.String,
		PhoneNumber: store.PhoneNumber,
		ImageSource: store.ImageSource,
		IsTurnOn:    store.IsTurnOn,
		StartedAt:   store.StartedAt.Int64,
		EndedAt:     store.EndedAt.Int64,
	}

	encode, err := json.Marshal(
		&ResponseBody{Data: &StoreOutput{Store: outputStore}})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
