package stories

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"

	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/internal/filehelper"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

func createStore(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

	source, err := filehelper.UploadFile(r, fmt.Sprintf("%s/store", mc["uuid"].(string)), "store.jpg", "")
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	startedAt, err := strconv.ParseInt(r.FormValue("startedAt"), 10, 32)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	endedAt, err := strconv.ParseInt(r.FormValue("endedAt"), 10, 32)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	store := &entities.Store{
		UserUUID:    mc["uuid"].(string),
		Name:        r.FormValue("name"),
		Description: handles.NewNullString(r.FormValue("description")),
		PhoneNumber: r.FormValue("phoneNumber"),
		IsTurnOn:    true,
		ImageSource: source,
		StartedAt:   handles.NewNullInt64(startedAt),
		EndedAt:     handles.NewNullInt64(endedAt),
	}

	if err = store_service.New(r.Context()).CreateStore(store); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	outputStore := &Store{
		UUID:        store.UUID,
		UserUUID:    store.UserUUID,
		Name:        store.Name,
		Description: store.Description.String,
		PhoneNumber: store.PhoneNumber,
		ImageSource: store.ImageSource,
		IsTurnOn:    store.IsTurnOn,
		StartedAt:   store.StartedAt.Int64,
		EndedAt:     store.EndedAt.Int64,
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &StoreOutput{Store: outputStore},
		Message: "Created is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
