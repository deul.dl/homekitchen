package stories

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateStoreSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	data := map[string]interface{}{
		"name":        "Victor's cafe",
		"house":       "description",
		"phoneNumber": "+77056278085",
		"endedAt":     0,
		"startedAt":   3600,
	}
	requestBody := map[string]map[string]interface{}{
		"data": data,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Use(StoreMiddleware)
	r.Put("/api/v1/stories/store", updateStore)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/stories/store",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)

		responseBody := &ResponseBody{Data: &StoreOutput{}}
		err = json.NewDecoder(res.Body).Decode(responseBody)
		if err != nil {
			t.Errorf("Expected nil, received %s", err.Error())
		}

		t.Error(responseBody.Message)

		return
	}

	responseBody := &ResponseBody{Data: &StoreOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	t.Log(responseBody)
}
