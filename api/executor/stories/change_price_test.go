package stories

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdatePriceOfDelivery(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}
	data := map[string]float64{
		"price": 1000,
	}
	requestBody := map[string]map[string]float64{
		"data": data,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Use(StoreMiddleware)
	r.Put("/change-price", updatePriceOfDelivery)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/change-price",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	if responseBody.Data.(map[string]interface{})["price"].(float64) != data["price"] {
		t.Log(responseBody.Data.(map[string]interface{})["price"])
		t.Log(data["price"])
		t.Errorf("Price is not changed")
	}
}
