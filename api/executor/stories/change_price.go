package stories

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type UpdatePriceInput struct {
	Price float64 `json:"price"`
}

func updatePriceOfDelivery(w http.ResponseWriter, r *http.Request) {
	store := r.Context().Value(STORE_KEY).(*entities.Store)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &UpdatePriceInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*UpdatePriceInput)

	if err = store_service.New(r.Context()).
		UpdatePriceOfDelivery(store.UUID, data.Price); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	storeDelivery, err := store_service.New(r.Context()).
		GetStoreDeliveryByStoreUUID(store.UUID)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	storeDeliveryOutput := &StoreDelivery{
		UUID:              storeDelivery.UUID,
		StoreUUID:         storeDelivery.UUID,
		UserUUID:          storeDelivery.UserUUID,
		Type:              storeDelivery.Type,
		Price:             storeDelivery.Price.Float64,
		DeliveryPriceUUID: storeDelivery.DeliveryPriceUUID.String,
		CreatedAt:         storeDelivery.CreatedAt.String(),
		UpdatedAt:         storeDelivery.UpdatedAt.String(),
	}

	encode, err := json.Marshal(&ResponseBody{Data: storeDeliveryOutput})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
