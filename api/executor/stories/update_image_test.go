package stories

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateImageOfSourceSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	path := "../../test_files/space.jpg"
	file, err := os.Open(path)
	if err != nil {
		t.Error(err)
		return
	}

	defer file.Close()
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(path))
	if err != nil {
		writer.Close()
		t.Error(err)
		return
	}
	io.Copy(part, file)
	writer.Close()

	r := chi.NewRouter()
	r.Use(StoreMiddleware)
	r.Put("/api/v1/stories/store", updateImage)
	ts := httptest.NewServer(r)
	defer ts.Close()

	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "8e2faff8-237e-4d7a-8100-69258d8c42f9",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/stories/store",
		body,
	)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	req.Header.Add(
		"x-app-token",
		token,
	)
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		t.Errorf("error %v", res.Body)
		return
	}

	t.Log(body.String())
}
