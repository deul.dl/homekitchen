package stories

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/auth_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestSwitchSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}
	itemBody := map[string]interface{}{
		"uuid": "f8e87af3-153f-467c-afdb-ea8cb55bd8cf",
	}
	requestBody := map[string]map[string]interface{}{
		"data": itemBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Use(StoreMiddleware)
	r.Put("/api/v1/stories/store", switchStore)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/api/v1/stories/store",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "8e2faff8-237e-4d7a-8100-69258d8c42f9",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	defer res.Body.Close()
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &auth_service.AuthSuccess{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	t.Log(responseBody)
}
