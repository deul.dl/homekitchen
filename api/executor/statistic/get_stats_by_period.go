package statistic

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/homekitchen/homekitchen/pkg/statistic_service"
)

type StatsInput struct {
	From string `json:"from"`
	To   string `json:"to"`
}

type StatsData struct {
	OrderCounts int64   `json:"orderCounts"`
	Profit      float64 `json:"profit"`
	Revenue     float64 `json:"revenue"`
	Day         string  `json:"day"`
}

func getStatsByPeriod(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &StatsInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*StatsInput)

	from, err := time.Parse(time.RFC3339, data.From)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}
	to, err := time.Parse(time.RFC3339, data.To)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	statsOfList, err := statistic_service.
		New(r.Context()).
		GetDataByPeriod(from, to)

	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}
	var statsOutput []*StatsData
	for _, stats := range statsOfList {
		statsOutput = append(statsOutput, &StatsData{
			OrderCounts: stats.OrderCounts,
			Profit:      stats.Profit,
			Revenue:     stats.Revenue,
			Day:         stats.Day,
		})
	}
	log.Println(statsOutput)
	encode, err := json.Marshal(&ResponseBody{statsOutput, ""})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
