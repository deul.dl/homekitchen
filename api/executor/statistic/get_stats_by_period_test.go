package statistic

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/api/test"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestGetStatsByPeriod(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	statsBody := map[string]interface{}{
		"from": "2021-07-23T00:00:00+00:00",
		"to":   "2021-07-23T23:59:59+00:00",
	}
	requestBody := map[string]map[string]interface{}{
		"data": statsBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Use(test.Middleware)
	r.Use(Middleware)
	r.Post("/api/v1/stats", getStatsByPeriod)

	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"POST",
		ts.URL+"/api/v1/stats",
		bytes.NewBuffer(res2B),
	)
	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "1e0225f1-fb11-4839-b378-7daf28351b1c",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := &http.Cookie{Name: "csrfToken", Path: "/", Value: token, Expires: expiration}
	req.AddCookie(cookie)

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}
	responseBody := &ResponseBody{}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

}
