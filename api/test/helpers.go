package test

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json")
		cookie, err := r.Cookie("csrfToken")
		if err != nil {
			ParseError(w, err)
			return
		}

		_, mc, err := ojwt.FromToken(cookie.Value)
		if err != nil {
			ParseError(w, err)
			return
		}

		if mc["role"] != entities.EXECUTOR {
			ParseForbiddenError(w)
			return
		}
		ctx := context.WithValue(r.Context(), auth.MC_KEY, mc)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
