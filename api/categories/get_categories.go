package categories

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type CategoriesOutput struct {
	Categories entities.Categories `json:"categories"`
}

func getCategories(w http.ResponseWriter, r *http.Request) {
	categories, err := store_service.
		New(r.Context()).
		GetCategories()
	if err != nil {
		log.Print(err)
		ParseError(w, err)
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &CategoriesOutput{Categories: categories},
		Message: "Got is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func getCategoriesWithItems(w http.ResponseWriter, r *http.Request) {
	categories, err := store_service.
		New(r.Context()).
		GetIsNotCategories()
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &CategoriesOutput{Categories: categories},
		Message: "Got is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
