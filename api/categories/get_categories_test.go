package categories

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/joho/godotenv"
)

func TestGetCategoriesSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	r := chi.NewRouter()
	r.Get("/api/v1/categories", getCategories)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"GET",
		ts.URL+"/api/v1/categories",
		nil,
	)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &CategoriesOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if len(responseBody.Data.(*CategoriesOutput).Categories) == 0 {
		t.Error("Categories is empty")
	}
	t.Log(responseBody)
}

func TestGetCategoriesWithItemsSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	r := chi.NewRouter()
	r.Get("/api/v1/categories", getCategoriesWithItems)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"GET",
		ts.URL+"/api/v1/categories",
		nil,
	)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &CategoriesOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if len(responseBody.Data.(*CategoriesOutput).Categories) == 0 {
		t.Error("Categories is empty")
	}
	t.Log(responseBody)
}
