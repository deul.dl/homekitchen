package api

import (
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/homekitchen/homekitchen/api/account"
	"github.com/homekitchen/homekitchen/api/admin"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/api/categories"
	"github.com/homekitchen/homekitchen/api/customer"
	"github.com/homekitchen/homekitchen/api/executor"
	"github.com/homekitchen/homekitchen/api/home"
	"github.com/homekitchen/homekitchen/api/items"
	"github.com/homekitchen/homekitchen/api/stories"
	"github.com/homekitchen/homekitchen/api/user"
)

func Initial() {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Accept", "Authorization", "Content-Type", "X-App-Token"},

		ExposedHeaders:   []string{"Content-Type", "Set-Cookie", "Cookie"},
		AllowCredentials: true,
		MaxAge:           16000, // Maximum value not ignored by any of major browsers
	})

	r.Use(cors.Handler)

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Use(middleware.Compress(5, "image/png", "image/jpg"))

	r.Route("/api/v1", func(r chi.Router) {
		r.Use(commonMiddleware)
		r.Route("/auth", auth.Initial)
		r.Route("/user", func(r chi.Router) {
			r.Use(auth.Middleware)
			user.Initial(r)
		})
		r.Route("/account", func(r chi.Router) {
			r.Use(auth.Middleware)
			account.Initial(r)
		})
		r.Route("/stories", stories.Initial)
		r.Route("/items", items.Initial)
		r.Route("/categories", categories.Initial)
		r.Route("/executor", func(r chi.Router) {
			r.Use(auth.Middleware)
			executor.Initial(r)
		})
		r.Route("/customer", func(r chi.Router) {
			r.Use(auth.Middleware)
			customer.Initial(r)
		})
		r.Route("/home", home.Initial)
		r.Route("/admin", admin.Initial)
	})

	r.Get("/images/*", func(w http.ResponseWriter, r *http.Request) {
		img, err := ioutil.ReadFile("./assets" + r.RequestURI)
		if err != nil {
			log.Print(err)
			HandleNotFound(w, r)
			return
		}

		w.Header().Set("Content-Type", http.DetectContentType(img))
		w.Write(img)
	})

	r.NotFound(HandleNotFound)

	http.ListenAndServe(":8080", r)
}
