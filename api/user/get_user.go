package user

import (
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	userService "github.com/homekitchen/homekitchen/pkg/user_service"
)

type UserResponese struct {
	UUID        string   `json:"uuid"`
	PhoneNumber string   `json:"phoneNumber"`
	Email       string   `json:"email"`
	Roles       []string `json:"roles"`
	CurrentRole string   `json:"currentRole"`
	FirstName   string   `json:"firstName"`
	LastName    string   `json:"lastName"`
	Status      string   `json:"status"`
}

func getUser(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)
	var response *UserResponese

	user, _ := userService.
		New(r.Context()).
		GetUserRoles(mc["uuid"].(string))
	roles := []string{entities.CUSTOMER}
	if user != nil {
		roles = append(roles, user.Roles[0].Name)
		response = &UserResponese{
			UUID:        user.UUID,
			PhoneNumber: user.PhoneNumber.String,
			Email:       user.Email,
			Roles:       roles,
			FirstName:   user.Executor.FirstName.String,
			LastName:    user.Executor.LastName.String,
			Status:      user.Status.String,
		}
	} else {
		response = &UserResponese{
			UUID:  mc["uuid"].(string),
			Roles: roles,
		}
	}

	if mc["role"].(string) == entities.EXECUTOR {
		response.CurrentRole = entities.EXECUTOR
	} else {
		response.CurrentRole = entities.CUSTOMER
	}

	encode, err := json.Marshal(&ResponseBody{Data: response})
	if err != nil {
		ParseError(w, err)
		return
	}
	w.Write(encode)
}
