package user

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/user_service"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type PhoneNumberInput struct {
	PhoneNumber    string `json:"phoneNumber"`
	OldPhoneNumber string `json:"oldPhoneNumber"`
	Code           string `json:"code"`
}

type PhoneNumberOutput struct {
	UUID        string `json:"uuid"`
	PhoneNumber string `json:"phoneNumber"`
	Code        string `json:"code"`
}

func updatePhoneNumber(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &PhoneNumberInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*PhoneNumberInput)

	client := &entities.Client{
		UUID:        handles.NewNullString(mc["uuid"].(string)),
		PhoneNumber: handles.NewNullString(data.PhoneNumber),
		Code:        handles.NewNullString(data.Code),
	}
	if err = user_service.
		New(r.Context()).
		UpdatePhoneNumber(client, data.OldPhoneNumber); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Data: &PhoneNumberOutput{
		UUID:        client.UUID.String,
		PhoneNumber: client.PhoneNumber.String,
		Code:        client.Code.String,
	}})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
