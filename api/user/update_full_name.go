package user

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/user_service"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type FullNameInput struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

type FullNameOutput struct {
	UUID      string `json:"uuid"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

func updateFullName(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &FullNameInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*FullNameInput)
	ctx := r.Context()
	userService := user_service.New(ctx)

	executor := &entities.Executor{
		UUID:      mc["uuid"].(string),
		FirstName: handles.NewNullString(data.FirstName),
		LastName:  handles.NewNullString(data.LastName),
	}
	if err = userService.GetStorage().UpdateExecutor(executor); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Data: &FullNameOutput{
		UUID:      executor.UUID,
		FirstName: executor.FirstName.String,
		LastName:  executor.LastName.String,
	}})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
