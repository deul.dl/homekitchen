package user

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/api/auth"
	"github.com/homekitchen/homekitchen/pkg/user_service"
)

type EmailInput struct {
	Email string `json:"email"`
}

type EmailOutput struct {
	UUID string `json:"uuid"`
}

func updateEmail(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(auth.MC_KEY).(jwt.MapClaims)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &EmailInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*EmailInput)
	ctx := r.Context()
	userService := user_service.New(ctx)

	if err = userService.GetStorage().UpdateEmail(mc["uuid"].(string), data.Email); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Data: &EmailOutput{UUID: mc["uuid"].(string)}, Message: "Updated email is successfully"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)

}
