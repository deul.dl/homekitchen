package user

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestPutDevicesSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()

	r.Put("/devices", putDevices)
	ts := httptest.NewServer(r)
	defer ts.Close()

	body := map[string]interface{}{
		"fmtToken": "asdasd asdhk",
	}
	requestBody := map[string]map[string]interface{}{
		"data": body,
	}
	res2B, _ := json.Marshal(requestBody)
	req, err := http.NewRequest(
		"PUT",
		ts.URL+"/devices",
		bytes.NewBuffer(res2B),
	)

	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	req.Header.Add("x-app-token", token)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
}
