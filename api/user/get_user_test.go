package user

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestExecutorUserSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	r := chi.NewRouter()

	r.Get("/api/v1/user", getUser)
	ts := httptest.NewServer(r)
	defer ts.Close()

	token, err := ojwt.CreateToken(
		&ojwt.TokenInput{
			UUID: "6aa33b3f-a889-490c-b1c0-da41224c6328",
			Role: entities.EXECUTOR,
		})
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	req, err := http.NewRequest(
		"GET",
		ts.URL+"/api/v1/user",
		nil,
	)
	req.Header.Add(
		"x-app-token",
		token,
	)

	res, err := client.Do(req)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	responseData := &ResponseBody{Data: &UserResponese{}}
	if err = json.Unmarshal(body, responseData); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}
	if responseData == nil {
		t.Errorf("data is empty")
		return
	}
}
