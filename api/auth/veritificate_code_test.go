package auth

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/joho/godotenv"
)

func TestVerificatePhoneNumberSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	signCodedBody := map[string]interface{}{
		"phoneNumber": "+77056278086",
		"code":        "code",
	}

	requestBody := map[string]map[string]interface{}{
		"data": signCodedBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Post("/api/v1/auth/verificate-code", sendCode)
	ts := httptest.NewServer(r)
	defer ts.Close()
	res, err := http.Post(ts.URL+"/api/v1/auth/verificate-code", "application/json", bytes.NewBuffer(res2B))

	if err != nil {
		t.Error(err)
		return
	}
	defer res.Body.Close()
	data := &ResponseBody{}
	err = json.NewDecoder(res.Body).Decode(data)
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	if err := json.Unmarshal(body, data); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if data == nil {
		t.Errorf("data is empty")
	}

}
