package auth

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/pkg/auth_service"
	"github.com/joho/godotenv"
)

func TestLoginWithSmsCodeSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	loginBody := map[string]interface{}{
		"phoneNumber": "+77056278085",
		"key":         "148370",
		"type":        auth_service.SMS_CODE_TYPE,
	}
	requestBody := map[string]map[string]interface{}{
		"data": loginBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Post("/api/v1/auth/login", login)
	ts := httptest.NewServer(r)
	defer ts.Close()
	res, err := http.Post(ts.URL+"/api/v1/auth/login", "application/json", bytes.NewBuffer(res2B))
	if err != nil {
		t.Error(err)
		return
	}

	data := &ResponseBody{Data: &auth_service.AuthSuccess{}}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	if err := json.Unmarshal(body, data); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if data == nil {
		t.Errorf("data is empty")
		return
	}
}

func TestSuccessLoginSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
	}

	loginBody := map[string]interface{}{
		"phoneNumber": "+77056278085",
		"key":         "123456a",
	}

	requestBody := map[string]map[string]interface{}{
		"data": loginBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Post("/api/v1/auth/login", login)
	ts := httptest.NewServer(r)
	defer ts.Close()
	res, err := http.Post(ts.URL+"/api/v1/auth/login", "application/json", bytes.NewBuffer(res2B))
	if err != nil {
		t.Error(err)
		return
	}
	defer res.Body.Close()

	data := &ResponseBody{Data: &auth_service.AuthSuccess{}}
	err = json.NewDecoder(res.Body).Decode(data)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if data != nil {
		t.Error(data)
		t.Errorf("data is empty")
	}
}
