package auth

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/homekitchen/homekitchen/pkg/auth_service"
	"github.com/joho/godotenv"
)

func TestSignupSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	signupBody := map[string]interface{}{
		"phoneNumber":  "+77056278085",
		"password":     "123456a",
		"email":        "vityadm199824@gmail.com",
		"city":         "Topar",
		"timezone":     "+6(UTC)",
		"longitude":    0.1,
		"latitude":     0.0,
		"locationCode": "KZ",
		"firstName":    "Victor",
		"lastName":     "Dmtrishen",
		"code":         "781594",
	}
	requestBody := map[string]map[string]interface{}{
		"data": signupBody,
	}
	res2B, _ := json.Marshal(requestBody)
	r := chi.NewRouter()
	r.Post("/signup", signup)
	ts := httptest.NewServer(r)
	defer ts.Close()

	res, err := http.Post(ts.URL+"/signup", "application/json", bytes.NewBuffer(res2B))
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}
	data := &ResponseBody{Data: &auth_service.AuthSuccess{}}
	if err := json.Unmarshal(body, data); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if data == nil {
		t.Errorf("data is empty")
		return
	}
}
