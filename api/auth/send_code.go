package auth

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/homekitchen/homekitchen/pkg/auth_service"
)

type SendCodeInput struct {
	PhoneNumber string `json:"phoneNumber"`
}

func sendCode(w http.ResponseWriter, r *http.Request) {
	mc := r.Context().Value(MC_KEY).(jwt.MapClaims)

	_, err := auth_service.
		New(r.Context()).
		SendCodeByUUID(mc["uuid"].(string))
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Message: "Sended code is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func sendCodeForLogin(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &SendCodeInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*SendCodeInput)

	_, err = auth_service.
		New(r.Context()).
		SendCode(data.PhoneNumber)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Message: "Sended code is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func sendCodeForSignup(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &SendCodeInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*SendCodeInput)

	_, err = auth_service.
		New(r.Context()).
		SendCodeForSignup(data.PhoneNumber)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(&ResponseBody{Message: "Sended code is successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
