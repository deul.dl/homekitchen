package auth

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/auth_service"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type SignUpOutput struct {
	PhoneNumber  string  `json:"phoneNumber"`
	Password     string  `json:"password"`
	Email        string  `json:"email"`
	City         string  `json:"city"`
	Timezone     string  `json:"timezone"`
	Longitude    float64 `json:"longitude"`
	Latitude     float64 `json:"latitude"`
	LocationCode string  `json:"locationCode"`
	FirstName    string  `json:"firstName"`
	LastName     string  `json:"lastName"`
	Code         string  `json:"code"`
	IIN          string  `json:"iin"`
}

func signup(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &SignUpOutput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}
	data := requestData.Data.(*SignUpOutput)
	ctx := r.Context()
	authService := auth_service.New(ctx)
	if err := authService.VerificateCode(data.PhoneNumber, data.Code); err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	user := &entities.User{
		PhoneNumber: handles.NewNullString(data.PhoneNumber),
		Email:       data.Email,
		Executor: &entities.Executor{
			LastName:  handles.NewNullString(data.LastName),
			FirstName: handles.NewNullString(data.FirstName),
			IIN:       handles.NewNullString(data.IIN),
			ExecutorAddress: &entities.ExecutorAddress{
				City:         handles.NewNullString(data.City),
				Timezone:     handles.NewNullString(data.Timezone),
				Longitude:    data.Longitude,
				Latitude:     data.Latitude,
				LocationCode: data.LocationCode,
			},
		},
	}

	result, err := authService.SignUp(&auth_service.SignUpInput{
		User:     user,
		Password: data.Password,
	})

	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := &http.Cookie{Name: "csrfToken", Path: "/", Value: result.Token, Expires: expiration}
	http.SetCookie(w, cookie)

	encode, err := json.Marshal(&ResponseBody{})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}
	w.Write(encode)
}
