package auth

import (
	"encoding/json"

	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/homekitchen/homekitchen/pkg/auth_service"
)

func login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &auth_service.LoginInput{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}

	data := requestData.Data.(*auth_service.LoginInput)
	ctx := r.Context()
	authService := auth_service.New(ctx)

	result, err := authService.Login(data)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := &http.Cookie{Name: "csrfToken", Path: "/", Value: result.Token, Expires: expiration}
	http.SetCookie(w, cookie)

	encode, err := json.Marshal(&ResponseBody{})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}
