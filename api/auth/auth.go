package auth

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/homekitchen/homekitchen/internal/ojwt"
)

type key string

const (
	MC_KEY key = "mc_key"
)

type RequestBody struct {
	Data interface{} `json:"data"`
}

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

type ResponseBodyError struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

func ParseForbiddenError(w http.ResponseWriter) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "403 Forbidden"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusForbidden)
	w.Write(encode)
}

func ParseError(w http.ResponseWriter, err error) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: err.Error()},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}

	w.WriteHeader(http.StatusBadRequest)
	w.Write(encode)
}

func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	encode, jsonErr := json.Marshal(
		&ResponseBodyError{Message: "Not Found"},
	)

	if jsonErr != nil {
		http.Error(w, jsonErr.Error(), http.StatusBadRequest)
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write(encode)
}

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		cookie, err := r.Cookie("csrfToken")
		if err != nil {
			ParseError(w, err)
			return
		}

		_, mc, err := ojwt.FromToken(cookie.Value)
		if err != nil {
			ParseError(w, err)
			return
		}

		ctx := context.WithValue(r.Context(), MC_KEY, mc)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func Initial(r chi.Router) {

	r.Post("/signup", signup)
	r.Route("/send-code", func(r chi.Router) {
		r.Post("/check", sendCode)
		r.Post("/signup", sendCodeForSignup)
		r.Post("/login", sendCodeForLogin)
	})
	r.Post("/logout", func(rw http.ResponseWriter, r *http.Request) {
		expiration := time.Now().Add(-365 * 24 * time.Hour)
		c := http.Cookie{
			Name:    "csrfToken",
			MaxAge:  -1,
			Expires: expiration}
		http.SetCookie(rw, &c)

		encode, err := json.Marshal(&ResponseBody{})
		if err != nil {
			log.Println(err)
			ParseError(rw, err)
			return
		}

		rw.Write(encode)
	})
	r.Post("/verificate-code", verificateCode)
	r.Post("/login", login)
}
