package auth

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"

	"github.com/joho/godotenv"
)

func TestSendCodeTest(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}
	signCodedBody := map[string]interface{}{
		"phoneNumber": "+77056278085",
	}
	requestBody := map[string]map[string]interface{}{
		"data": signCodedBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Post("/api/v1/auth/send-code", sendCode)
	ts := httptest.NewServer(r)
	defer ts.Close()
	res, err := http.Post(ts.URL+"/api/v1/auth/send-code", "application/json", bytes.NewBuffer(res2B))
	if err != nil {
		t.Error(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	data := &RequestBody{}
	if err := json.Unmarshal(body, data); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if data == nil {
		t.Errorf("data is empty")
	}
}

func TestSendCodeForSignupTest(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}
	signCodedBody := map[string]interface{}{
		"phoneNumber": "+77056278085",
	}
	requestBody := map[string]map[string]interface{}{
		"data": signCodedBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Post("/api/v1/auth/send-code", sendCodeForSignup)
	ts := httptest.NewServer(r)
	defer ts.Close()
	res, err := http.Post(ts.URL+"/api/v1/auth/send-code", "application/json", bytes.NewBuffer(res2B))
	if err != nil {
		t.Error(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	data := &RequestBody{}
	if err := json.Unmarshal(body, data); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if data == nil {
		t.Errorf("data is empty")
	}
}

func TestSendCodeForLoginTest(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}
	signCodedBody := map[string]interface{}{
		"phoneNumber": "+77056278085",
	}
	requestBody := map[string]map[string]interface{}{
		"data": signCodedBody,
	}
	res2B, _ := json.Marshal(requestBody)

	r := chi.NewRouter()
	r.Post("/api/v1/auth/send-code", sendCodeForLogin)
	ts := httptest.NewServer(r)
	defer ts.Close()
	res, err := http.Post(ts.URL+"/api/v1/auth/send-code", "application/json", bytes.NewBuffer(res2B))
	if err != nil {
		t.Error(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Error(err)
		return
	}

	data := &RequestBody{}
	if err := json.Unmarshal(body, data); err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
		return
	}

	if data == nil {
		t.Errorf("data is empty")
	}
}
