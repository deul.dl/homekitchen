package auth

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/auth_service"
)

type VerificatedCodeData struct {
	PhoneNumber string `json:"phoneNumber"`
	Code        string `json:"code"`
}

type VerificatedPhoneNumberSuccess struct {
	IsValid bool   `json:"isValid"`
	Message string `json:"message"`
}

func verificateCode(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		ParseError(w, err)
		return
	}

	requestData := &RequestBody{Data: &VerificatedCodeData{}}
	if err := json.Unmarshal(body, requestData); err != nil {
		ParseError(w, err)
		return
	}
	data := requestData.Data.(*VerificatedCodeData)
	ctx := r.Context()
	authService := auth_service.New(ctx)

	err = authService.
		VerificateCode(data.PhoneNumber, data.Code)
	if err != nil {
		ParseError(w, err)
		return
	}

	encode, err := json.Marshal(
		&ResponseBody{
			Data: &VerificatedPhoneNumberSuccess{
				IsValid: true,
				Message: "Your phone number were verificated succcess!"}})

	if err != nil {
		log.Println(err)
		ParseError(w, err)
	}

	w.Write(encode)

}
