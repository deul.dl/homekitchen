package home

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
	"github.com/joho/godotenv"
)

func TestGetStoriesWithItemsSuccess(t *testing.T) {
	client := &http.Client{}
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	city := "Mountain%20View"
	r := chi.NewRouter()
	r.Get("/home", getStories)
	ts := httptest.NewServer(r)
	defer ts.Close()

	req, err := http.NewRequest(
		"GET",
		ts.URL+"/home?city="+city,
		nil,
	)

	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}

	res, err := client.Do(req)
	if res.StatusCode != http.StatusOK || err != nil {
		t.Errorf("Expected %d, received %d", http.StatusOK, res.StatusCode)
		return
	}

	responseBody := &ResponseBody{Data: &StoriesOutput{}}
	err = json.NewDecoder(res.Body).Decode(responseBody)
	if err != nil {
		t.Errorf("Expected nil, received %s", err.Error())
	}
	if len(responseBody.Data.(*StoriesOutput).Stories) == 0 {
		t.Error("Stories is empty")
	}
	t.Log(responseBody)
}
