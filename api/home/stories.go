package home

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type ItemOutput struct {
	UUID        string  `json:"uuid"`
	Name        string  `json:"name"`
	StoreUUID   string  `json:"storeUUID"`
	UserUUID    string  `json:"userUUID"`
	Price       float64 `json:"price"`
	Cost        float64 `json:"cost"`
	Description string  `json:"description"`
	ImageSource string  `json:"imageSource"`
}

type StoreOutput struct {
	UUID        string        `json:"uuid"`
	Name        string        `json:"name"`
	Description string        `json:"description"`
	ImageSource string        `json:"imageSource"`
	StartedAt   int64         `json:"startedAt"`
	EndedAt     int64         `json:"endedAt"`
	Items       []*ItemOutput `json:"items"`
}

type StoriesOutput struct {
	Stories []*StoreOutput `json:"stories"`
}

func getStories(w http.ResponseWriter, r *http.Request) {
	city := r.URL.Query().Get("city")

	stories, err := store_service.
		NewStoreItemService(r.Context()).
		GetStoriesWithItemsByCity(city)
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}
	var storiesOutput []*StoreOutput
	for _, store := range stories {
		storiesOutput = append(storiesOutput, &StoreOutput{
			UUID:        store.UUID,
			Name:        store.Name,
			Description: store.Description.String,
			ImageSource: store.ImageSource,
			StartedAt:   store.StartedAt,
			EndedAt:     store.EndedAt,
			Items:       copyItems(store.Items),
		})
	}

	encode, err := json.Marshal(&ResponseBody{
		Data:    &StoriesOutput{Stories: storiesOutput},
		Message: "Got stories were successfully!"})
	if err != nil {
		log.Println(err)
		ParseError(w, err)
		return
	}

	w.Write(encode)
}

func copyItems(items entities.Items) (itemsOutput []*ItemOutput) {
	for _, item := range items {
		itemsOutput = append(itemsOutput, &ItemOutput{
			UUID:        item.UUID,
			Name:        item.Name,
			Description: item.Description,
			Price:       item.Price,
			Cost:        item.Cost,
			UserUUID:    item.UserUUID,
			StoreUUID:   item.StoreUUID,
			ImageSource: item.ImageSource.String,
		})
	}
	return
}
