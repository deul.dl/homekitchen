package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/homekitchen/homekitchen/api"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/joho/godotenv"
)

var env string

func main() {
	flag.StringVar(&env, "env", "prod", "type of production")

	// parse
	flag.Parse()
	fmt.Println(env)
	fmt.Println(os.Args)

	err := godotenv.Load("." + env + ".env")
	if err != nil {
		panic("Error loading .env file")
	}

	if err := db.Connect(); err != nil {
		log.Fatal(err)
	}

	defer db.Pool.Close()

	api.Initial()
}
