FROM golang:1.15.6 as builder

LABEL version="1.5"

COPY . /go/src/github.com/homekitchen/homekitchen

WORKDIR /go/src/github.com/homekitchen/homekitchen

ARG MODE=prod

ENV CGO_ENABLED=0
ENV GO111MODULE=on
ENV GOFLAGS=-mod=vendor
ENV GOOS=linux
ENV GOARCH=amd64
ENV ENV_MODE=${MODE}

RUN go mod vendor
RUN go mod download
RUN go mod verify

RUN go build -o /homekitchen .

RUN ls /homekitchen

EXPOSE 8080


RUN echo ${ENV_MODE}

CMD /homekitchen -env ${ENV_MODE}
