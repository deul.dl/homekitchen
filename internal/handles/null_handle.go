package handles

import (
	"database/sql"
	"reflect"
	"time"
)

type NullInt64 struct {
	sql.NullInt64
}

func NewNullInt64(value int64) NullInt64 {
	if value == 0 {
		return NullInt64{sql.NullInt64{}}
	}

	return NullInt64{sql.NullInt64{value, true}}
}

func (ni *NullInt64) Scan(value interface{}) error {
	var i sql.NullInt64
	if err := i.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ni = NullInt64{sql.NullInt64{i.Int64, false}}
	} else {
		*ni = NullInt64{sql.NullInt64{i.Int64, true}}
	}
	return nil
}

// NullBool is an alias for sql.NullBool data type
type NullBool struct {
	sql.NullBool
}

func NewNullBool(value bool) NullBool {
	if reflect.TypeOf(value) == nil {
		return NullBool{sql.NullBool{}}
	}

	return NullBool{sql.NullBool{value, true}}
}

func (nb *NullBool) Scan(value interface{}) error {
	var b sql.NullBool
	if err := b.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*nb = NullBool{sql.NullBool{b.Bool, false}}
	} else {
		*nb = NullBool{sql.NullBool{b.Bool, true}}
	}

	return nil
}

type NullFloat64 struct {
	sql.NullFloat64
}

func NewNullFloat64(value float64) NullFloat64 {
	if reflect.TypeOf(value) == nil {
		return NullFloat64{sql.NullFloat64{}}
	}

	return NullFloat64{sql.NullFloat64{value, true}}
}

func (nf *NullFloat64) Scan(value interface{}) error {
	var f sql.NullFloat64
	if err := f.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*nf = NullFloat64{sql.NullFloat64{f.Float64, false}}
	} else {
		*nf = NullFloat64{sql.NullFloat64{f.Float64, true}}
	}

	return nil
}

// NullString is an alias for sql.NullString data type
type NullString struct {
	sql.NullString
}

func NewNullString(value interface{}) NullString {
	if value == nil || value == "" {
		return NullString{sql.NullString{}}
	}

	return NullString{sql.NullString{value.(string), true}}
}

func (ns *NullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ns = NullString{sql.NullString{s.String, false}}
	} else {
		*ns = NullString{sql.NullString{s.String, true}}
	}

	return nil
}

type NullTime struct {
	sql.NullTime
}

func NewNullTime(value interface{}) NullTime {
	if value == nil {
		return NullTime{sql.NullTime{}}
	}

	return NullTime{sql.NullTime{value.(time.Time), true}}
}

// MarshalJSON for NullTime
func (ns *NullTime) Scan(value interface{}) error {
	var s sql.NullTime
	if err := s.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ns = NullTime{sql.NullTime{s.Time, false}}
	} else {
		*ns = NullTime{sql.NullTime{s.Time, true}}
	}

	return nil
}

// author - https://gist.github.com/rsudip90/45fad7d8959c58bcc91d464873b50013
