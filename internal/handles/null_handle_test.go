package handles

import (
	"fmt"
	"testing"
)

func TestNullString(t *testing.T) {

	value, _ := NewNullString("Hello, World!").Value()

	if value != "Hello, World!" {
		fmt.Println(value)
		t.Errorf("No `Hello, World!`")
	}
}

func TestNullInt64(t *testing.T) {

	value, _ := NewNullInt64(1234).Value()

	if value != int64(1234) {
		fmt.Println(value)
		t.Errorf("No `1234`")
	}
}

func TestNullBool(t *testing.T) {

	value, _ := NewNullBool(true).Value()

	if value != true {
		fmt.Println(value)
		t.Errorf("No `1234`")
	}
}
