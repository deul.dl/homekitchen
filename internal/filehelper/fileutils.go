package filehelper

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func UploadFile(r *http.Request, path, filename, oldpath string) (source string, err error) {
	file, _, err := r.FormFile("file")
	if err != nil {
		return
	}
	if oldpath != "" {
		os.Remove("assets/" + oldpath)
	}
	source = fmt.Sprintf("images/%s/", path)
	source += filename

	if _, errOfPath := os.Stat("assets/" + source); os.IsNotExist(errOfPath) {
		if err = os.MkdirAll(filepath.Dir("assets/"+source), 0777); err != nil {
			log.Println(source)
			return "", err
		}
	}

	dst, err := os.Create("assets/" + source)
	defer dst.Close()
	if err != nil {
		return
	}

	// Copy the uploaded file to the created file on the filesystem
	if _, err = io.Copy(dst, file); err != nil {
		return
	}

	return
}
