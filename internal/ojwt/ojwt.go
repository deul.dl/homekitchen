package ojwt

import (
	"github.com/dgrijalva/jwt-go"

	"errors"
	"time"
)

const signKey string = "homeKitchen_e3e1bf63f928cc5a9289eb80107c031167b2bdbf"

type TokenInput struct {
	UUID string
	Role string
}

func CreateToken(input *TokenInput) (token string, err error) {

	str := jwt.New(jwt.SigningMethodHS256)

	claims := make(jwt.MapClaims)

	claims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()
	claims["iat"] = time.Now().Unix()
	claims["uuid"] = input.UUID
	claims["role"] = input.Role

	str.Claims = claims

	token, err = str.SignedString([]byte(signKey))

	return

}

func DecodeToken(tnk string) (token *jwt.Token, err error) {
	token, err = jwt.Parse(tnk, keyFunc)
	return
}

func keyFunc(t *jwt.Token) (interface{}, error) {
	return []byte(signKey), nil
}

func FromToken(tnk string) (token *jwt.Token, mc jwt.MapClaims, err error) {

	token, err = DecodeToken(tnk)

	switch err.(type) {
	case nil:
		if !token.Valid {
			return nil, nil, errors.New("Valid token")
		}

		if tokenClaims, ok := token.Claims.(jwt.MapClaims); ok {
			mc = tokenClaims
		} else {
			return nil, nil, errors.New("unknown type of Claims")
		}

		return
	case *jwt.ValidationError:
		vErr := err.(*jwt.ValidationError)

		switch vErr.Errors {
		case jwt.ValidationErrorExpired:
			return nil, nil, errors.New("Token has expired!")
		default:
			return nil, nil, errors.New("Error parsing token!")
		}
	default:
		return nil, nil, errors.New("Unable to parse token!")
	}

	return

}
