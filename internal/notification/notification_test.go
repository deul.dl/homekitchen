package notification

import (
	"fmt"
	"os"
	"testing"

	"github.com/joho/godotenv"
)

func TestPushNotification(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}
	fmt.Println(os.Getenv("FMS_KEY"))
	token := "fyAAmlJiRaunPeldsR4NIS:APA91bGTANrv0MwN_54OZRFdWDa5BUA-RGrCf34H4sjLniGMpANuc37NBtNA0P_bbr1kp3KL_xjO0RdikzpYg2RQd7e99Can6v_A8h9JO1CHp9QeEOyl2D_amU5DPVcppRiuv6Sj3Ziz"
	err = NewFirebaseMessaging(token).Send(&Notification{
		Title:     "Title 1",
		Body:      "Hello, World",
		Status:    RECEIVED_STATUS,
		OrderUUID: "1234",
	})
	if err != nil {
		t.Error(err)
		return
	}
}

func TestSendSmsCode(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	_, err = SendSMS("+77056278085", "123456")
	if err != nil {
		t.Error(err)
		return
	}
}
