package notification

import (
	"log"
	"os"

	"github.com/appleboy/go-fcm"
)

// ROOT is the baseURL of the Twilio API
const ROOT = "https://api.twilio.com"

// VERSION is the version of the Twilio API
const VERSION = "2010-04-01"

const (
	RECEIVED_STATUS    = "received"
	ACCEPTED_STATUS    = "accepted"
	PROCEESSED_STATUS  = "processing"
	DONE_STATUS        = "ready"
	TRANSACTION_STATUS = "paid"
	COMPLETED_STATUS   = "completed"
	CANCELED_STATUS    = "canceled"
	REJECTED_STATUS    = "rejected"
)

type Notification struct {
	Title     string
	Body      string
	Status    string
	OrderUUID string
}

type Nofification interface {
	Send(notification *Notification) error
}

type FirebaseNotification struct {
	Nofification

	FmtToken string
}

func NewFirebaseMessaging(token string) Nofification {
	return &FirebaseNotification{FmtToken: token}
}

func (fm *FirebaseNotification) Send(notification *Notification) (err error) {
	msg := &fcm.Message{
		To: fm.FmtToken,
		Notification: &fcm.Notification{
			Title: notification.Title,
			Body:  notification.Body,
		},
		Data: map[string]interface{}{
			"status":     notification.Status,
			"order_uuid": notification.OrderUUID,
		},
	}

	// Create a FCM client to send the message.
	client, err := fcm.NewClient(os.Getenv("FMS_KEY"))
	if err != nil {
		log.Println(err)
		return
	}

	// Send the message and receive the response without retries.
	response, err := client.Send(msg)
	if err != nil {
		log.Println(err)
		return
	}

	log.Printf("%#v\n", response)
	return
}
