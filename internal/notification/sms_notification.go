package notification

import (
	"log"
	"os"

	"github.com/google/uuid"
	"github.com/sfreiberg/gotwilio"
)

func SendSMS(phoneNumber, code string) (string, error) {
	if os.Getenv("TYPE") == "dev" {
		return uuid.New().String(), nil
	}

	twilio := gotwilio.NewTwilioClient(os.Getenv("ACCOUNT_SID"), os.Getenv("TWILLO_AUTH_TOKEN"))
	message := "HK:" + code
	res, exception, err := twilio.SendSMS(
		"+12028165158", phoneNumber,
		message, "https://demo.twilio.com/owl.png", os.Getenv("ACCOUNT_SID"))
	if err != nil {
		return "", err
	}
	if exception != nil {
		log.Println(exception)
	}
	return res.Sid, nil
}
