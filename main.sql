--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.addresses (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    customer_uuid uuid NOT NULL,
    country character varying(255),
    city character varying(255) NOT NULL,
    porch character varying(255),
    street character varying(255) NOT NULL,
    house character varying(255),
    apartment character varying(255),
    is_primary boolean NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone
);


ALTER TABLE public.addresses OWNER TO homekitchen;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    "position" integer NOT NULL,
    name character varying(255) NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.categories OWNER TO homekitchen;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: homekitchen
--

CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO homekitchen;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homekitchen
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.clients (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    phone_number character varying(20) NOT NULL,
    code character varying(6) NOT NULL,
    divice_token character varying(255),
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.clients OWNER TO homekitchen;

--
-- Name: delivery_prices; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.delivery_prices (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    country_code character varying(5) NOT NULL,
    country character varying(155) NOT NULL,
    city character varying(155) NOT NULL,
    price real,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.delivery_prices OWNER TO homekitchen;

--
-- Name: executor_addresses; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.executor_addresses (
    uuid uuid NOT NULL,
    city character varying(255) NOT NULL,
    timezone character varying(10) NOT NULL,
    longitude numeric,
    latitude numeric,
    location_code character varying(20),
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.executor_addresses OWNER TO homekitchen;

--
-- Name: executors; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.executors (
    uuid uuid NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    iin character varying(50)
);


ALTER TABLE public.executors OWNER TO homekitchen;

--
-- Name: items; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.items (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_uuid uuid NOT NULL,
    store_uuid uuid NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    price real NOT NULL,
    cost real NOT NULL,
    category_id integer NOT NULL,
    is_turn_on boolean NOT NULL,
    image_source character varying(255),
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone
);


ALTER TABLE public.items OWNER TO homekitchen;

--
-- Name: order_items; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.order_items (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    order_uuid uuid NOT NULL,
    item_uuid uuid NOT NULL,
    name character varying(255) NOT NULL,
    price real NOT NULL,
    count integer NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    cost real DEFAULT 0 NOT NULL
);


ALTER TABLE public.order_items OWNER TO homekitchen;

--
-- Name: order_payment_methods; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.order_payment_methods (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    order_uuid uuid NOT NULL,
    total_price real NOT NULL,
    discount real NOT NULL,
    delivery_price real NOT NULL,
    amount real NOT NULL,
    type character varying(40) NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    total_cost real DEFAULT 0
);


ALTER TABLE public.order_payment_methods OWNER TO homekitchen;

--
-- Name: order_statuses; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.order_statuses (
    id integer NOT NULL,
    order_uuid uuid NOT NULL,
    status character varying(20) NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.order_statuses OWNER TO homekitchen;

--
-- Name: order_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: homekitchen
--

CREATE SEQUENCE public.order_statuses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_statuses_id_seq OWNER TO homekitchen;

--
-- Name: order_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homekitchen
--

ALTER SEQUENCE public.order_statuses_id_seq OWNED BY public.order_statuses.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.orders (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    status character varying(20) NOT NULL,
    "time" timestamp with time zone NOT NULL,
    executor_uuid uuid NOT NULL,
    customer_uuid uuid NOT NULL,
    store_uuid uuid NOT NULL,
    total_price real NOT NULL,
    address_uuid uuid,
    description character varying(560),
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone
);


ALTER TABLE public.orders OWNER TO homekitchen;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.roles OWNER TO homekitchen;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: homekitchen
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO homekitchen;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homekitchen
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO homekitchen;

--
-- Name: store_deliveries; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.store_deliveries (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    store_uuid uuid NOT NULL,
    user_uuid uuid NOT NULL,
    type character varying(25) DEFAULT 'hk_delivery_service'::character varying NOT NULL,
    price real,
    delivery_price_uuid character varying(36),
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.store_deliveries OWNER TO homekitchen;

--
-- Name: stories; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.stories (
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_uuid uuid NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(1000),
    image_source character varying(255) NOT NULL,
    phone_number character varying(255) NOT NULL,
    is_turn_on boolean NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    is_delivery boolean DEFAULT false,
    started_at integer,
    ended_at integer
);


ALTER TABLE public.stories OWNER TO homekitchen;

--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.user_roles (
    role_id integer NOT NULL,
    user_uuid uuid NOT NULL
);


ALTER TABLE public.user_roles OWNER TO homekitchen;

--
-- Name: users; Type: TABLE; Schema: public; Owner: homekitchen
--

CREATE TABLE public.users (
    uuid uuid NOT NULL,
    phone_number character varying(20) NOT NULL,
    email character varying(40) NOT NULL,
    password_hash character varying(255) NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    status character varying(35) NOT NULL
);


ALTER TABLE public.users OWNER TO homekitchen;

--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: order_statuses id; Type: DEFAULT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.order_statuses ALTER COLUMN id SET DEFAULT nextval('public.order_statuses_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Data for Name: addresses; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.addresses VALUES ('2db98c25-b49a-463c-8be9-2026df44ae62', 'c43177f3-3f71-4530-a082-389cdf02b1c1', 'Kazakhstan', 'Topar', '2', 'Понамарева', '15', '32', true, '2021-04-11 09:47:42.786771+00', '2021-04-11 09:47:42.786771+00', NULL);
INSERT INTO public.addresses VALUES ('ff5ffa3f-1d7c-47a7-8839-09697be07e5d', 'e928420e-a836-4514-bd59-548e5cfc6ba7', 'Kazakhstan', 'Karaganda', '2', 'Лободы ', '4', '45', false, '2021-04-11 09:47:42.793121+00', '2021-03-24 11:24:44.207596+00', NULL);


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.categories VALUES (1, 1, 'Pizzas', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (2, 2, 'Shawarmas', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (3, 3, 'Sushis', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (4, 4, 'Hamburgers', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (5, 5, 'Cakes', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (6, 6, 'Desert', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (7, 7, 'Drinks', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (8, 8, 'Snacks', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (9, 9, 'FirstMeals', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (10, 10, 'SecondMeals', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (11, 11, 'MainCourses', '2021-01-30 05:54:46.118709+00', '2021-01-30 05:54:46.118709+00');
INSERT INTO public.categories VALUES (12, 12, 'Vegetables', '2021-08-15 16:38:20.265056+00', '2021-08-15 16:38:20.265056+00');
INSERT INTO public.categories VALUES (13, 13, 'Fruits', '2021-08-15 16:38:20.265056+00', '2021-08-15 16:38:20.265056+00');
INSERT INTO public.categories VALUES (14, 14, 'Berries', '2021-08-15 16:38:20.265056+00', '2021-08-15 16:38:20.265056+00');
INSERT INTO public.categories VALUES (15, 15, 'Mushrooms', '2021-08-15 16:38:20.265056+00', '2021-08-15 16:38:20.265056+00');
INSERT INTO public.categories VALUES (16, 16, 'MilkProducts', '2021-08-15 16:38:20.265056+00', '2021-08-15 16:38:20.265056+00');
INSERT INTO public.categories VALUES (17, 17, 'Fishes', '2021-08-15 16:38:20.265056+00', '2021-08-15 16:38:20.265056+00');
INSERT INTO public.categories VALUES (18, 18, 'Meats', '2021-08-15 16:38:20.265056+00', '2021-08-15 16:38:20.265056+00');


--
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.clients VALUES ('c809825e-d185-46b8-b6d5-486b533f1455', '+77000884044', '790111', NULL, '2021-12-18 05:09:20.48519+00', '2021-12-18 05:09:20.48519+00');
INSERT INTO public.clients VALUES ('c43177f3-3f71-4530-a082-389cdf02b1c1', '+77056278085', '137510', 'elyUg6fOTE-k223Mq1Bjcl:APA91bE-482YV9K9lD8JsK0ov1XtVyzwy63MEWi6-nnK08Zc3SsL_Fvsh7EfA6SesfQyhc1FKfS0O93BAWw-a9xWJNJ8ImrnkaxhihW2ilVp0IHFcyglD7qz9EcN_xC8F4C5nt9VoBMq', '2022-01-10 12:38:24.446239+00', '2021-01-30 05:58:47.930616+00');
INSERT INTO public.clients VALUES ('20945b57-cfa6-4e16-a52c-74236a2af0b2', '+9073', '747366', NULL, '2022-01-10 12:50:15.672924+00', '2022-01-10 12:50:15.672924+00');
INSERT INTO public.clients VALUES ('af3228e2-ae9f-4c9b-8f29-69c62277a402', '+562', '968692', NULL, '2022-01-10 12:50:52.760284+00', '2022-01-10 12:50:52.760284+00');
INSERT INTO public.clients VALUES ('2caa9c0c-9a16-4d8c-8e6a-3aec7cf62130', '+9491067010', '522314', NULL, '2021-09-05 14:47:08.445518+00', '2021-09-05 14:47:08.445518+00');
INSERT INTO public.clients VALUES ('50fef679-c7ba-4626-9c19-979e687251a8', '87757140684', '643834', NULL, '2021-03-21 12:39:27.724563+00', '2021-03-21 12:39:27.724563+00');
INSERT INTO public.clients VALUES ('bea7a727-b331-45a6-a6a4-a04df6351f4c', '+77757140684', '914654', 'c79QRX-OQ8KvvTLqerKAnM:APA91bFHRAcskUoCYoHEAsxotp9gVEfR93f9xX4peOjwCWhmhymHTbqOB20h_uIFEejj4GbjCiTseV25mGGpZyxOIBgsxAspupuQmXlVpwaPuxIxntGYlJ24Wxc8pg8OtGaaWMEA5LwT', '2021-03-21 12:39:59.310041+00', '2021-03-21 12:39:45.766738+00');
INSERT INTO public.clients VALUES ('14afd6e3-d679-4996-9433-cce71d3d34fd', '87012141130', '508121', NULL, '2021-03-23 10:23:46.052673+00', '2021-03-23 10:23:46.052673+00');
INSERT INTO public.clients VALUES ('272aeab2-6582-46f5-8d42-b0848cec8596', '87012141138', '637011', NULL, '2021-03-23 10:23:48.883146+00', '2021-03-23 10:23:48.883146+00');
INSERT INTO public.clients VALUES ('196b5813-67ab-4577-bd46-c8749d5e47a6', '+919491067010', '694195', NULL, '2021-09-05 14:47:13.206255+00', '2021-09-05 14:47:13.206255+00');
INSERT INTO public.clients VALUES ('2de6f122-59ed-4e04-9fe6-aa8e3c21dfca', '+380961838288', '949979', NULL, '2021-10-04 14:32:31.308801+00', '2021-10-04 14:32:31.308801+00');
INSERT INTO public.clients VALUES ('e928420e-a836-4514-bd59-548e5cfc6ba7', '+77012141138', '667767', 'c8s543hKRcyEknkXs-41Gp:APA91bEWEfROKunm0VRm5lj5sQD5OOLQgpCac1-T0Y6JAZ2LH_QnbzijPdkbOC0OrXsej-wtHJwB3Td5A6yF8VeW3k29Klw5zuemHNMLTmP6uwwDb436oLMSDO8MiQ7aJ7O8dCTqkIsi', '2021-03-24 12:56:01.618208+00', '2021-03-24 11:02:23.904794+00');


--
-- Data for Name: delivery_prices; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.delivery_prices VALUES ('b690c9f6-b5b7-4f42-9bbc-06f9ca16ab9f', 'KZ', 'Kazachstan', 'Topar', 250, '2021-04-06 16:38:31.097582+00', '2021-04-06 16:38:31.097582+00');


--
-- Data for Name: executor_addresses; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.executor_addresses VALUES ('c43177f3-3f71-4530-a082-389cdf02b1c1', 'Topar', '+06(6:360)', 72.8294769, 49.5196265, 'KZ', '2021-01-30 05:59:14.09387+00', '2021-01-30 05:59:14.09387+00');
INSERT INTO public.executor_addresses VALUES ('e928420e-a836-4514-bd59-548e5cfc6ba7', 'Karaganda', '+06(6:360)', 73.0847706, 49.8121015, 'KZ', '2021-03-24 11:05:19.390188+00', '2021-03-24 11:05:19.390188+00');


--
-- Data for Name: executors; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.executors VALUES ('c43177f3-3f71-4530-a082-389cdf02b1c1', 'Виктор', 'дмитришен', '2021-01-30 05:59:14.09387+00', '2021-01-30 05:59:14.09387+00', '');
INSERT INTO public.executors VALUES ('e928420e-a836-4514-bd59-548e5cfc6ba7', 'islam', 'saadulaev', '2021-03-24 11:05:19.390188+00', '2021-03-24 11:05:19.390188+00', '');


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.items VALUES ('056ed9df-d56f-42ac-b745-a9c395d5e385', 'e928420e-a836-4514-bd59-548e5cfc6ba7', '08c6f4dd-c8bd-4d2e-a769-9e71f5fa4590', 'Дон карлелне ', 'вкусный бургери ', 1500, 500, 4, true, 'images/e928420e-a836-4514-bd59-548e5cfc6ba7/store/items/original_20210324_11_23_28_056ed9df-d56f-42ac-b745-a9c395d5e385.jpg', '2021-03-24 11:23:31.006902+00', '2021-03-24 11:22:27.453405+00', NULL);
INSERT INTO public.items VALUES ('6232625e-c3b4-43be-95e2-9dd7e3a5b97c', 'c43177f3-3f71-4530-a082-389cdf02b1c1', 'b08b9a59-de95-48af-9151-372dd8a02dc5', 'жареная картошка', '....', 1200, 1000, 10, true, 'images/c43177f3-3f71-4530-a082-389cdf02b1c1/store/items/original_20210411_09_08_26_6232625e-c3b4-43be-95e2-9dd7e3a5b97c.jpg', '2021-04-11 09:45:10.873003+00', '2021-04-11 09:08:01.346617+00', NULL);


--
-- Data for Name: order_items; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.order_items VALUES ('d61f7cff-a3d6-424d-a73c-ca3b98ea12aa', '9e852d8f-45ad-464f-8b11-be4e02af2d66', '056ed9df-d56f-42ac-b745-a9c395d5e385', 'Дон карлелне ', 1500, 2, '2021-03-24 11:24:50.6358+00', '2021-03-24 11:24:50.6358+00', 0);
INSERT INTO public.order_items VALUES ('80e00de3-38f6-406f-8742-76dcee5b277b', 'c7b95c77-b506-477c-9f0d-0b0b8ebf9257', '6232625e-c3b4-43be-95e2-9dd7e3a5b97c', 'жареная картошка', 1200, 3, '2021-04-11 09:47:51.857216+00', '2021-04-11 09:47:51.857216+00', 0);


--
-- Data for Name: order_payment_methods; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.order_payment_methods VALUES ('4344bd63-2f10-4113-9de3-5b805dea4b0a', 'c7b95c77-b506-477c-9f0d-0b0b8ebf9257', 3600, 0, 0, 3600, 'in_cash', '2021-04-11 09:47:51.857216+00', '2021-04-11 09:47:51.857216+00', 0);


--
-- Data for Name: order_statuses; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.order_statuses VALUES (1, '9e852d8f-45ad-464f-8b11-be4e02af2d66', 'received', '2021-03-24 11:24:50.6358+00', '2021-03-24 11:24:50.6358+00');
INSERT INTO public.order_statuses VALUES (2, '9e852d8f-45ad-464f-8b11-be4e02af2d66', 'accepted', '2021-03-24 11:26:18.254733+00', '2021-03-24 11:26:18.254733+00');
INSERT INTO public.order_statuses VALUES (3, '9e852d8f-45ad-464f-8b11-be4e02af2d66', 'accepted', '2021-03-24 11:26:19.382853+00', '2021-03-24 11:26:19.382853+00');
INSERT INTO public.order_statuses VALUES (4, '9e852d8f-45ad-464f-8b11-be4e02af2d66', 'processing', '2021-03-24 11:26:20.499229+00', '2021-03-24 11:26:20.499229+00');
INSERT INTO public.order_statuses VALUES (5, '9e852d8f-45ad-464f-8b11-be4e02af2d66', 'ready', '2021-03-24 11:26:27.228034+00', '2021-03-24 11:26:27.228034+00');
INSERT INTO public.order_statuses VALUES (6, '9e852d8f-45ad-464f-8b11-be4e02af2d66', 'ready', '2021-03-24 11:26:28.198238+00', '2021-03-24 11:26:28.198238+00');
INSERT INTO public.order_statuses VALUES (7, 'c7b95c77-b506-477c-9f0d-0b0b8ebf9257', 'received', '2021-04-11 09:47:51.857216+00', '2021-04-11 09:47:51.857216+00');


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.orders VALUES ('9e852d8f-45ad-464f-8b11-be4e02af2d66', 'ready', '2021-03-24 11:24:50.635718+00', 'e928420e-a836-4514-bd59-548e5cfc6ba7', 'e928420e-a836-4514-bd59-548e5cfc6ba7', '08c6f4dd-c8bd-4d2e-a769-9e71f5fa4590', 3000, 'ff5ffa3f-1d7c-47a7-8839-09697be07e5d', '', '2021-03-24 11:26:28.199389+00', '2021-03-24 11:24:50.6358+00', NULL);
INSERT INTO public.orders VALUES ('c7b95c77-b506-477c-9f0d-0b0b8ebf9257', 'received', '2021-04-11 09:47:51.856811+00', 'c43177f3-3f71-4530-a082-389cdf02b1c1', 'c43177f3-3f71-4530-a082-389cdf02b1c1', 'b08b9a59-de95-48af-9151-372dd8a02dc5', 3600, '2db98c25-b49a-463c-8be9-2026df44ae62', '', '2021-04-11 09:47:51.857216+00', '2021-04-11 09:47:51.857216+00', NULL);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.roles VALUES (1, 'Administation', '2021-01-30 05:54:45.990173+00', '2021-01-30 05:54:45.990173+00');
INSERT INTO public.roles VALUES (2, 'Executor', '2021-01-30 05:54:45.990173+00', '2021-01-30 05:54:45.990173+00');


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.schema_migrations VALUES (21, false);


--
-- Data for Name: store_deliveries; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.store_deliveries VALUES ('d3f1a489-6ccc-4e51-b5da-ff937630a41d', 'b08b9a59-de95-48af-9151-372dd8a02dc5', 'c43177f3-3f71-4530-a082-389cdf02b1c1', 'hk_delivery_service', NULL, 'b690c9f6-b5b7-4f42-9bbc-06f9ca16ab9f', '2021-04-06 16:38:31.097582+00', '2021-04-06 16:38:31.097582+00');
INSERT INTO public.store_deliveries VALUES ('e81867c4-7370-40af-ae55-d8fa40883b4d', '08c6f4dd-c8bd-4d2e-a769-9e71f5fa4590', 'e928420e-a836-4514-bd59-548e5cfc6ba7', 'hk_delivery_service', NULL, 'b690c9f6-b5b7-4f42-9bbc-06f9ca16ab9f', '2021-04-06 16:38:31.097582+00', '2021-04-06 16:38:31.097582+00');


--
-- Data for Name: stories; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.stories VALUES ('b08b9a59-de95-48af-9151-372dd8a02dc5', 'c43177f3-3f71-4530-a082-389cdf02b1c1', 'Small caffee.', NULL, 'images/c43177f3-3f71-4530-a082-389cdf02b1c1/store/original_20211029_10_56_23_b08b9a59-de95-48af-9151-372dd8a02dc5.jpg', '+7777777777777', false, '2022-01-10 12:38:44.352826+00', '2021-01-30 06:12:30.425494+00', false, NULL, NULL);
INSERT INTO public.stories VALUES ('08c6f4dd-c8bd-4d2e-a769-9e71f5fa4590', 'e928420e-a836-4514-bd59-548e5cfc6ba7', 'burger shop', 'дедаем вкусную , домашнюю еду . ', 'images/e928420e-a836-4514-bd59-548e5cfc6ba7/store/store.jpg', '+77012141138', true, '2021-03-24 11:21:54.347931+00', '2021-03-24 11:21:54.347931+00', false, NULL, NULL);


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.user_roles VALUES (2, 'c43177f3-3f71-4530-a082-389cdf02b1c1');
INSERT INTO public.user_roles VALUES (2, 'e928420e-a836-4514-bd59-548e5cfc6ba7');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: homekitchen
--

INSERT INTO public.users VALUES ('c43177f3-3f71-4530-a082-389cdf02b1c1', '+77056278085', 'vityadm199824@gmail.com', '$2a$14$iayjSdhT7vRSAFINiJ4Ydu3JpT7i5GFjJTpchb/bYzgMQSVmvppD.', '2021-01-30 05:59:14.09387+00', '2021-01-30 05:59:14.09387+00', ' verificated');
INSERT INTO public.users VALUES ('e928420e-a836-4514-bd59-548e5cfc6ba7', '+77012141138', 'vk-store@yandex.ru', '$2a$14$p6sgMOd7V36C2i.wFfz6pexlkfcyFaIAg4n19eEuJnYympjhCLdkO', '2021-03-24 11:05:19.390188+00', '2021-03-24 11:05:19.390188+00', ' verificated');


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homekitchen
--

SELECT pg_catalog.setval('public.categories_id_seq', 18, true);


--
-- Name: order_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homekitchen
--

SELECT pg_catalog.setval('public.order_statuses_id_seq', 7, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homekitchen
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (uuid);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: clients clients_phone_number_key; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_phone_number_key UNIQUE (phone_number);


--
-- Name: clients clients_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (uuid);


--
-- Name: delivery_prices delivery_prices_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.delivery_prices
    ADD CONSTRAINT delivery_prices_pkey PRIMARY KEY (uuid);


--
-- Name: executor_addresses executor_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.executor_addresses
    ADD CONSTRAINT executor_addresses_pkey PRIMARY KEY (uuid);


--
-- Name: executors executors_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.executors
    ADD CONSTRAINT executors_pkey PRIMARY KEY (uuid);


--
-- Name: items items_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (uuid);


--
-- Name: order_items order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.order_items
    ADD CONSTRAINT order_items_pkey PRIMARY KEY (uuid);


--
-- Name: order_payment_methods order_payment_methods_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.order_payment_methods
    ADD CONSTRAINT order_payment_methods_pkey PRIMARY KEY (uuid);


--
-- Name: order_statuses order_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.order_statuses
    ADD CONSTRAINT order_statuses_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (uuid);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: store_deliveries store_deliveries_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.store_deliveries
    ADD CONSTRAINT store_deliveries_pkey PRIMARY KEY (uuid);


--
-- Name: stories stories_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.stories
    ADD CONSTRAINT stories_pkey PRIMARY KEY (uuid);


--
-- Name: users users_phone_number_key; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_phone_number_key UNIQUE (phone_number);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: homekitchen
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (uuid);


--
-- PostgreSQL database dump complete
--

