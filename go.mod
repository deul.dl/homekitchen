module github.com/homekitchen/homekitchen

go 1.15

require (
	github.com/appleboy/go-fcm v0.1.5
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/cors v1.1.1
	github.com/google/uuid v1.1.5
	github.com/jackc/pgx/v4 v4.10.1
	github.com/joho/godotenv v1.3.0
	github.com/sfreiberg/gotwilio v0.0.0-20201211181435-c426a3710ab5
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
