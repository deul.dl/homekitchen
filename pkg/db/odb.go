package db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"
)

type ODB interface {
	Insert(query string, values []interface{}) (sql.Result, error)
	Update(query string, values []interface{}) (sql.Result, error)
	TruncateTables(tables []string) error
	GetDB() *sql.DB
}

type odbService struct {
	ODB

	db  *sql.DB
	ctx context.Context
}

func NewODB(ctx context.Context) ODB {
	return &odbService{db: Pool, ctx: ctx}
}

func (odb *odbService) Insert(query string, values []interface{}) (res sql.Result, err error) {
	inserted, err := odb.db.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("odb: unable to rollback: %v\n", err)
		return
	}

	res, err = inserted.Exec(values...)
	if err != nil {
		log.Printf("odb: create failed: %v\n", err)
		return
	}

	return res, nil
}

func (odb *odbService) Update(query string, values []interface{}) (res sql.Result, err error) {
	updated, err := odb.db.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("odb: unable to rollback: %v\n", err)
		return
	}

	res, err = updated.Exec(values...)
	if err != nil {
		log.Printf("odb: update failed: %v\n", err)
		return
	}
	return res, nil
}

func (odb *odbService) TruncateTables(tables []string) (err error) {
	if _, err = odb.db.Exec(fmt.Sprintf("TRUNCATE %s RESTART IDENTITY;", strings.Join(tables, ", "))); err != nil {
		log.Printf("odb: truncate failed: %v\n", err)
		return
	}
	return
}

func (odb *odbService) GetDB() *sql.DB {
	return odb.db
}
