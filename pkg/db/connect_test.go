package db

import (
	"testing"

	"github.com/joho/godotenv"
)

func TestConnectSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}
	if err = Connect(); err != nil {
		t.Errorf("Error Connect DataBase")
	}

}
