package db

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/jackc/pgx/v4/stdlib"
)

// "user=%s dbname=%s password=%s sslmode=disable port=5432"
// "postgres://%s:%s@database:5432/%s?sslmode=disable"
var Pool *sql.DB

func Connect() (err error) {
	dataSourceName := fmt.Sprintf(
		os.Getenv("SOURCE_DB"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
	)
	log.Print("connect... " + os.Getenv("POSTGRES_USER"))
	Pool, err = sql.Open("pgx", dataSourceName)
	if err != nil {
		panic(err)
	}

	err = Pool.Ping()
	if err != nil {
		panic(err)
	}

	Pool.SetMaxOpenConns(150)
	Pool.SetMaxIdleConns(150)
	Pool.SetConnMaxLifetime(10)

	return
}
