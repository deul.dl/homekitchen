package db

import (
	"context"
	"testing"

	"github.com/joho/godotenv"
)

func TestUpdateSuccess(t *testing.T) {

	err := godotenv.Load("../.test.env")

	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	_, err = NewODB(ctx).Update(
		"UPDATE clients SET phone_number = ? WHERE phone_number = ?",
		[]interface{}{"+77056278085", "+77056278085"},
	)

	if err != nil {
		t.Error(err)
	}
}

func TestUpdateFuilure(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	_, err = NewODB(ctx).Update(
		"UPDATE clients SET phone_numbe = ? WHERE phone_number = ?;",
		[]interface{}{"+77056278085", "+77056278085"},
	)

	if err != nil {
		t.Error(err)
	}
}
