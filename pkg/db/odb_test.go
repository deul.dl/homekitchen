package db

import (
	"context"
	"testing"

	"github.com/joho/godotenv"
)

func TestInsertSuccess(t *testing.T) {

	err := godotenv.Load("../.test.env")

	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	_, err = NewODB(ctx).Insert(
		"INSERT INTO test_records(name) VALUES(?)",
		[]interface{}{"Test"},
	)

	if err != nil {
		t.Error(err)
	}
}

func TestInsertFuilure(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	_, err = NewODB(ctx).Insert(
		"INSERT INTO(name) VALUES()",
		[]interface{}{"Test"},
	)

	if err != nil {
		t.Error(err)
	}
}

func TestTruncateSuccess(t *testing.T) {

	err := godotenv.Load("../.test.env")

	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = NewODB(ctx).TruncateTables([]string{"Test"})

	if err != nil {
		t.Error(err)
	}
}

func TestTruncateFuilure(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = NewODB(ctx).TruncateTables([]string{"Tests"})

	if err != nil {
		t.Error(err)
	}
}

// $ migrate -source ./migrations -database postgres://localhost:5432/home_kitchen up
