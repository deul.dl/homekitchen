package order_service

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/notification"
	"github.com/homekitchen/homekitchen/pkg/address_service"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/homekitchen/homekitchen/pkg/order_service/order_storage"
	"github.com/homekitchen/homekitchen/pkg/store_service"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type CustomerOrderService interface {
	CreateOrder(*entities.Order) error
	CancelOrder(*entities.Order) error
	CompleteOrder(*entities.Order) error
	GetCustomerOrders() (entities.Orders, error)
	PushNotification(userUUID, orderUUID, status string) error
	GetOrderStatuses(string) (entities.OrderStatuses, error)
}

type ExecutorOrderService interface {
	AcceptOrder(order *entities.Order) error
	StartCook(order *entities.Order) error
	PayOrder(order *entities.Order) error
	DoneOrder(order *entities.Order) error
	RejectOrder(order *entities.Order) error
	GetExecutorOrders() (entities.Orders, error)
}

type OrderService interface {
	CustomerOrderService
	ExecutorOrderService
	GetStorage() order_storage.Storage
	GetOrderPaymentMethod(string) (*entities.OrderPaymentMethod, error)
	GetOrderItemsByOrderUUID(string) (entities.OrderItems, error)
}

type orderService struct {
	OrderService

	ctx          context.Context
	orderStorage order_storage.Storage
}

func New(ctx context.Context) OrderService {
	return &orderService{
		ctx:          ctx,
		orderStorage: order_storage.NewOrders(ctx),
	}
}

func (service *orderService) CreateOrder(order *entities.Order) error {
	if order.AddressUUID.Valid {
		if _, err := address_service.New(service.ctx).
			GetAddressByUUID(order.AddressUUID.String); err != nil {
			return errors.New("Address is not found.")
		}
	}

	order.Status = entities.RECEIVED_STATUS

	for _, orderItem := range order.OrderItems {
		order.PaymentMethod.Amount += orderItem.Price * float64(orderItem.Count)
		order.PaymentMethod.TotalCost += orderItem.Cost * float64(orderItem.Count)

	}
	order.Time = time.Now()

	storeDelivery, err := store_service.New(service.ctx).
		GetStoreDeliveryByStoreUUID(order.StoreUUID)
	if err != nil {
		return err
	}
	order.TotalPrice = order.PaymentMethod.Amount + storeDelivery.Price.Float64
	order.PaymentMethod.TotalPrice = order.TotalPrice
	order.PaymentMethod.DeliveryPrice = storeDelivery.Price.Float64

	if err := service.GetStorage().CreateOrder(order); err != nil {
		return err
	}

	return service.PushNotification(order.ExecutorUUID, order.UUID, order.Status)
}

func (service *orderService) AcceptOrder(order *entities.Order) error {
	if err := service.GetStorage().UpdateStatus(&entities.OrderStatus{
		OrderUUID: order.UUID,
		Status:    entities.ACCEPTED_STATUS,
	}); err != nil {
		return err
	}

	return service.PushNotification(order.CustomerUUID, order.UUID, entities.ACCEPTED_STATUS)
}

func (service *orderService) RejectOrder(order *entities.Order) error {
	if err := service.GetStorage().UpdateStatus(&entities.OrderStatus{
		OrderUUID: order.UUID,
		Status:    entities.REJECTED_STATUS,
	}); err != nil {
		return err
	}

	return service.PushNotification(order.CustomerUUID, order.UUID, entities.REJECTED_STATUS)
}

func (service *orderService) CancelOrder(order *entities.Order) error {
	if err := service.GetStorage().UpdateStatus(&entities.OrderStatus{
		OrderUUID: order.UUID,
		Status:    entities.CANCELED_STATUS,
	}); err != nil {
		return err
	}

	return service.PushNotification(order.ExecutorUUID, order.UUID, entities.CANCELED_STATUS)
}

func (service *orderService) StartCook(order *entities.Order) error {
	if err := service.GetStorage().UpdateStatus(&entities.OrderStatus{
		OrderUUID: order.UUID,
		Status:    entities.PROCEESSED_STATUS,
	}); err != nil {
		return err
	}

	return service.PushNotification(order.CustomerUUID, order.UUID, entities.PROCEESSED_STATUS)
}

func (service *orderService) PayOrder(order *entities.Order) error {
	if err := service.GetStorage().UpdateStatus(&entities.OrderStatus{
		OrderUUID: order.UUID,
		Status:    entities.TRANSACTION_STATUS,
	}); err != nil {
		return err
	}

	return service.PushNotification(order.CustomerUUID, order.UUID, entities.TRANSACTION_STATUS)
}

func (service *orderService) DoneOrder(order *entities.Order) error {
	if err := service.GetStorage().UpdateStatus(&entities.OrderStatus{
		OrderUUID: order.UUID,
		Status:    entities.DONE_STATUS,
	}); err != nil {
		return err
	}

	return service.PushNotification(order.CustomerUUID, order.UUID, entities.DONE_STATUS)
}

func (service *orderService) CompleteOrder(order *entities.Order) error {
	if err := service.GetStorage().UpdateStatus(&entities.OrderStatus{
		OrderUUID: order.UUID,
		Status:    entities.COMPLETED_STATUS,
	}); err != nil {
		return err
	}

	return service.PushNotification(order.ExecutorUUID, order.UUID, entities.COMPLETED_STATUS)
}

func (service *orderService) GetOrderItemsByOrderUUID(orderUUID string) (orderItems entities.OrderItems, err error) {
	rows, err := service.GetStorage().GetOrderItems(orderUUID)
	if err != nil {
		return orderItems, err
	}
	defer rows.Close()

	for rows.Next() {
		orderItem := &entities.OrderItem{}
		if err = rows.Scan(
			&orderItem.OrderUUID, &orderItem.Name,
			&orderItem.Price, &orderItem.Count,
			&orderItem.ItemUUID,
		); err != nil {
			return
		}
		orderItems = append(orderItems, orderItem)
	}
	if err := rows.Err(); err != nil {
		return orderItems, err
	}
	return
}

func (service *orderService) GetOrderStatuses(orderUUID string) (orderStatuses entities.OrderStatuses, err error) {
	rows, err := service.GetStorage().GetOrderStatuses(orderUUID)
	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		orderStatus := &entities.OrderStatus{}
		if err = rows.Scan(
			&orderStatus.OrderUUID, &orderStatus.Status,
		); err != nil {
			return
		}
		orderStatuses = append(orderStatuses, orderStatus)
	}
	if rows.Err() != nil {
		return orderStatuses, rows.Err()
	}
	return
}

func (service *orderService) GetCustomerOrders() (orders entities.Orders, err error) {
	rows, err := service.orderStorage.GetCustomerOrders()
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		order, err := service.mapOrderWithAddress(rows)
		if err != nil {
			log.Printf("orders: %v", err)
			return nil, err
		}
		orders = append(orders, order)
	}
	if rows.Err() != nil {
		return orders, rows.Err()
	}
	return orders, err
}

func (service *orderService) GetOrderPaymentMethod(orderUUID string) (orderPaymentMethod *entities.OrderPaymentMethod, err error) {
	row := service.GetStorage().GetOrderPaymentMethodByOrderUUID(orderUUID)
	if row.Err() != nil {
		return orderPaymentMethod, row.Err()
	}

	orderPaymentMethod = &entities.OrderPaymentMethod{}
	if err = row.Scan(
		&orderPaymentMethod.UUID, &orderPaymentMethod.Type,
		&orderPaymentMethod.Amount, &orderPaymentMethod.TotalPrice,
		&orderPaymentMethod.DeliveryPrice, &orderPaymentMethod.OrderUUID,
	); err != nil {
		return
	}

	return
}

func (service *orderService) GetExecutorOrders() (entities.Orders, error) {
	return newExecutorFind(service.ctx, service).run()
}

func (*orderService) mapOrderWithAddress(rows *sql.Rows) (order *entities.Order, err error) {
	order = &entities.Order{}
	address := &entities.Address{}
	if err = rows.Scan(
		&order.UUID, &order.StoreUUID,
		&order.ExecutorUUID, &order.CustomerUUID,
		&order.AddressUUID, &order.Time,
		&order.Status, &order.TotalPrice,
		&address.UUID, &address.CustomerUUID,
		&address.Street, &address.House,
		&address.Apartment, &address.Porch,
		&address.City, &address.Country,
	); err != nil {
		log.Printf("orders: %v", err)
		return nil, err
	}

	order.Address = address

	if err != nil {
		log.Printf("orders: %v", err)
		return nil, err
	}

	return
}

func (service *orderService) PushNotification(userUUID, orderUUID, status string) error {
	client, err := users.NewClients(service.ctx).
		GetClientByField("uuid", userUUID)
	if err != nil {
		return err
	}

	if !client.NotificationToken.Valid {
		return nil
	}

	message := &notification.Notification{
		Title:     "notification." + status + ".title",
		Body:      "notification." + status + ".body",
		Status:    status,
		OrderUUID: orderUUID,
	}

	return notification.
		NewFirebaseMessaging(client.NotificationToken.String).
		Send(message)
}

func (service *orderService) GetStorage() order_storage.Storage {
	return service.orderStorage
}
