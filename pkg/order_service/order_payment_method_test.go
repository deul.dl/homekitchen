package order_service

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestOrderPaymentMethod(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	ctx = context.WithValue(ctx, entities.USER_UUID, "ad3d6ba8-0af2-47ee-b5e5-216288b792fc")

	order := &entities.Order{
		StoreUUID:    "aa063b26-9676-4a8a-8e5e-3d5c2ff32ec5",
		ExecutorUUID: "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
		CustomerUUID: "256864ed-c5cc-4f41-94fb-49c43900eca2",
		AddressUUID:  handles.NewNullString(uuid.New().String()),
		Description:  "Description",
		Time:         time.Now(),
		OrderItems: []*entities.OrderItem{
			&entities.OrderItem{
				ItemUUID: "d6c36b37-6287-45b3-b5de-a1e8a38193d9",
				Name:     "Item 1",
				Price:    500,
				Count:    1,
			},
			&entities.OrderItem{
				ItemUUID: "5762b21b-40a9-497c-ab14-9305bc0371ea",
				Name:     "Item 1",
				Price:    500,
				Count:    2,
			},
		},
		PaymentMethod: &entities.OrderPaymentMethod{
			Type: entities.TRANSLATE_TYPE,
		},
	}

	if err = New(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	_, err = New(ctx).GetOrderPaymentMethod(order.UUID)
	if err != nil {
		t.Error(err)
		return
	}
}
