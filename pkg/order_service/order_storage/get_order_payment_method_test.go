package order_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrderPaymentMethodSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables([]string{OrderTable, OrderStatusTable, OrderItemTable})

	order := &entities.Order{
		ExecutorUUID: "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
		CustomerUUID: "256864ed-c5cc-4f41-94fb-49c43900eca2",
		StoreUUID:    uuid.New().String(),
		AddressUUID:  handles.NewNullString(uuid.New().String()),
		Description:  "Description",
		Status:       entities.RECEIVED_STATUS,
		TotalPrice:   1500,
		PaymentMethod: &entities.OrderPaymentMethod{
			TotalPrice:    1500,
			DeliveryPrice: 122,
			Amount:        1500,
			Type:          entities.TRANSLATE_TYPE,
		},
	}

	order.OrderItems = []*entities.OrderItem{
		&entities.OrderItem{
			ItemUUID:  "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
			OrderUUID: order.UUID,
			Name:      "Item 1",
			Price:     500,
			Count:     1,
		},
		&entities.OrderItem{
			ItemUUID:  "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
			OrderUUID: order.UUID,
			Name:      "Item 1",
			Price:     500,
			Count:     1,
		},
		&entities.OrderItem{
			ItemUUID:  "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
			OrderUUID: order.UUID,
			Name:      "Item 1",
			Price:     500,
			Count:     1,
		},
	}

	if err = NewOrders(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	row := NewOrders(ctx).GetOrderPaymentMethodByOrderUUID(order.UUID)
	if row.Err() != nil {
		t.Error(row.Err())
		return
	}

	orderItem := &entities.OrderPaymentMethod{}
	if err = row.Scan(
		&orderItem.UUID, &orderItem.Type,
		&orderItem.Amount, &orderItem.TotalPrice,
		&orderItem.DeliveryPrice, &orderItem.OrderUUID,
	); err != nil {
		t.Error(err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{OrderTable, OrderStatusTable, OrderItemTable})
}
