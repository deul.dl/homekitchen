package order_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrderItemsSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables([]string{OrderTable, OrderStatusTable, OrderItemTable})

	order := &entities.Order{
		ExecutorUUID: "b5771632-9578-4f39-b6fc-66ad2efc4a3d	",
		CustomerUUID: "256864ed-c5cc-4f41-94fb-49c43900eca2	",
		StoreUUID:   uuid.New().String(),
		AddressUUID: handles.NewNullString(uuid.New().String()),
		Description: "Description",
		Status:      entities.RECEIVED_STATUS,
		TotalPrice:  1500,
	}

	var orderItems entities.OrderItems

	order.OrderItems = []*entities.OrderItem{
		&entities.OrderItem{
			Name:  "Item 1",
			Count: 1,
			Price: 500,
		},
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
			Count: 1,
		},
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
			Count: 1,
		},
	}

	if err = NewOrders(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	rows, err := NewOrders(ctx).GetOrderItems(order.UUID)
	if err != nil {
		t.Error(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		orderItem := &entities.OrderItem{}
		if err = rows.Scan(
			&orderItem.OrderUUID, &orderItem.Name,
			&orderItem.Price, &orderItem.Count,
			&orderItem.ItemUUID,
		); err != nil {
			t.Error(err)
			return
		}
		orderItems = append(orderItems, orderItem)
	}
	if rows.Err() != nil {
		t.Error(rows.Err())
	}

	if len(orderItems) != 3 {
		t.Error("Order Items have to math")
	}

	db.NewODB(ctx).TruncateTables([]string{OrderTable, OrderStatusTable, OrderItemTable})
}
