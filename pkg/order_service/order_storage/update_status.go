package order_storage

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type updateStatus struct {
	*orderStorage

	orderStatus *entities.OrderStatus
}

func newUpdateStatus(storage *orderStorage, orderStatus *entities.OrderStatus) *updateStatus {
	return &updateStatus{
		orderStorage: storage,
		orderStatus:  orderStatus}
}

func (action *updateStatus) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("orders: %v", err)
		return err
	}

	if err = action.saveOrderStatus(tx, action.orderStatus); err != nil {
		return err
	}

	if err = action.updateStatusOfOrder(tx); err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("orders: %v", err)
	}
	return nil
}

func (action *updateStatus) updateStatusOfOrder(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			UPDATE %s
			SET status = $1,
					updated_at = $2
			WHERE
					uuid = $3;
		`,
		OrderTable,
	)

	update, err := action.database.Prepare(query)
	defer update.Close()
	if err != nil {
		log.Printf("orders: unable to rollback: %v\n", err)
		return nil
	}

	if _, err := tx.StmtContext(action.ctx, update).Exec(
		handles.NewNullString(action.orderStatus.Status),
		time.Now(),
		action.orderStatus.OrderUUID,
	); err != nil {
		log.Printf("orders: add item failed: %v", err)
		tx.Rollback()
		return err
	}
	return nil
}
