package order_storage

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

const OrderTable = "orders"
const OrderStatusTable = "order_statuses"
const OrderItemTable = "order_items"
const OrderPaymentMethodTable = "order_payment_methods"

const AddressTable = "addresses"
const ClientTable = "clients"

type Storage interface {
	CreateOrder(order *entities.Order) error
	UpdateStatus(updateStatus *entities.OrderStatus) error
	GetOrderByUUID(orderUUID string) (*entities.Order, error)
	GetCustomerOrders() (*sql.Rows, error)
	GetExecutorOrders() (*sql.Rows, error)
	GetOrderPaymentMethodByOrderUUID(string) *sql.Row
	GetAddressByCustomerUUID(orderUUID string) (*entities.Address, error)
	GetOrderItems(orderUUID string) (*sql.Rows, error)
	GetOrderStatuses(orderUUID string) (*sql.Rows, error)
}

func NewOrders(ctx context.Context) Storage {

	return &orderStorage{
		ctx:      ctx,
		database: db.Pool,
	}
}

type orderStorage struct {
	Storage

	ctx      context.Context
	database *sql.DB
}

func (storage *orderStorage) CreateOrder(order *entities.Order) error {
	return newCreateOrder(storage, order).run()
}

func (storage *orderStorage) UpdateStatus(orderStatus *entities.OrderStatus) error {
	return newUpdateStatus(storage, orderStatus).run()
}

func (storage *orderStorage) GetCustomerOrders() (*sql.Rows, error) {
	query := fmt.Sprintf(`
		SELECT
			"orders"."uuid", "orders"."store_uuid",
			"orders"."executor_uuid", "orders"."customer_uuid",
			"orders"."address_uuid", "orders"."time",
			"orders"."status", "orders"."total_price",
			"addresses"."uuid", "addresses"."customer_uuid",
			"addresses"."street", "addresses"."house",
			"addresses"."apartment", "addresses"."porch",
			"addresses"."city", "addresses"."country"
		FROM %s
		LEFT JOIN %s
			ON addresses.uuid = orders.address_uuid
		WHERE
			orders.customer_uuid = $1
		ORDER BY orders.time DESC
	`,
		OrderTable, AddressTable)

	userUUID := storage.ctx.Value(entities.USER_UUID).(string)

	return storage.database.QueryContext(storage.ctx, query, userUUID)
}

func (storage *orderStorage) GetExecutorOrders() (rows *sql.Rows, err error) {
	query := fmt.Sprintf(`
		SELECT
			orders.uuid, orders.store_uuid,
			orders.executor_uuid, orders.customer_uuid,
			orders.address_uuid, orders.time,
			orders.status, orders.total_price,
			addresses.uuid, addresses.customer_uuid,
			addresses.street, addresses.house,
			addresses.apartment, addresses.porch,
			addresses.city, addresses.country,
			clients.phone_number
		FROM %s
		LEFT JOIN %s
		ON addresses.uuid = orders.address_uuid
		INNER JOIN %s
		ON orders.customer_uuid = clients.uuid
		WHERE
			orders.executor_uuid = $1
		GROUP BY orders.uuid, clients.phone_number, orders.address_uuid, addresses.uuid
		ORDER BY orders.time DESC
	`,
		OrderTable, AddressTable, ClientTable)

	userUUID := storage.ctx.Value(entities.USER_UUID).(string)

	log.Println(query)
	return storage.database.QueryContext(storage.ctx, query, userUUID)

}

func (storage *orderStorage) GetOrderItems(orderUUID string) (rows *sql.Rows, err error) {
	query := fmt.Sprintf(`
			SELECT
				order_uuid, name, price, count, item_uuid
			FROM %s
			WHERE order_uuid = $1
		`,
		OrderItemTable)
	return storage.database.QueryContext(storage.ctx, query, orderUUID)
}

func (storage *orderStorage) GetOrderStatuses(orderUUID string) (rows *sql.Rows, err error) {
	query := fmt.Sprintf(`
			SELECT
				order_uuid, status
			FROM %s
			WHERE order_uuid = $1
		`,
		OrderStatusTable)
	return storage.database.QueryContext(storage.ctx, query, orderUUID)
}

func (storage *orderStorage) GetOrderByUUID(orderUUID string) (*entities.Order, error) {
	query := fmt.Sprintf(`
			SELECT
				uuid, store_uuid,
				executor_uuid, customer_uuid,
				address_uuid, time,
				status, total_price
			FROM %s
			WHERE
				uuid = $1
			LIMIT 1
		`,
		OrderTable)

	row := storage.database.
		QueryRowContext(storage.ctx, query, orderUUID)
	if row.Err() != nil {
		return nil, row.Err()
	}

	order := &entities.Order{}
	err := row.Scan(
		&order.UUID,
		&order.StoreUUID,
		&order.ExecutorUUID,
		&order.CustomerUUID,
		&order.AddressUUID,
		&order.Time,
		&order.Status,
		&order.TotalPrice,
	)
	if err == sql.ErrNoRows {
		return nil, errors.New("Order is not exits")
	} else if err != nil {
		log.Printf("store: %v", err)
		return nil, err
	}
	return order, nil
}

func (storage *orderStorage) GetOrderPaymentMethodByOrderUUID(orderUUID string) *sql.Row {
	query := fmt.Sprintf(`
			SELECT
				uuid, type, amount, 
				total_price, delivery_price, order_uuid
			FROM %s
			WHERE
				order_uuid = $1
			LIMIT 1
		`,
		OrderPaymentMethodTable)

	return storage.database.
		QueryRowContext(storage.ctx, query, orderUUID)
}

func (storage *orderStorage) saveOrderStatus(tx *sql.Tx, orderStatus *entities.OrderStatus) error {
	query := fmt.Sprintf(
		`
			INSERT INTO %s 
				(order_uuid, status)
			VALUES ($1, $2)
		`,
		OrderStatusTable,
	)

	inserted, err := storage.database.Prepare(query)
	defer inserted.Close()

	if err != nil {
		log.Printf("order_statuses: unable to rollback: %v\n", err)
		return nil
	}

	if _, err := tx.StmtContext(storage.ctx, inserted).Exec(
		handles.NewNullString(orderStatus.OrderUUID),
		handles.NewNullString(orderStatus.Status),
	); err != nil {
		log.Printf("order_statuses: add status failed: %v", err)
		tx.Rollback()
		return err
	}
	return nil
}
