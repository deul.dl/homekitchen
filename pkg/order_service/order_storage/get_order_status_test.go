package order_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrderStatusesSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables([]string{OrderTable, OrderStatusTable, OrderItemTable})

	order := &entities.Order{
		ExecutorUUID: uuid.New().String(),
		CustomerUUID: uuid.New().String(),
		StoreUUID:    uuid.New().String(),
		AddressUUID:  handles.NewNullString(uuid.New().String()),
		Description:  "Description",
		Status:       entities.RECEIVED_STATUS,
		TotalPrice:   1500,
	}

	order.OrderItems = []*entities.OrderItem{
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
			Count: 1,
		},
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
			Count: 1,
		},
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
			Count: 1,
		},
	}

	if err = NewOrders(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	if _, err = NewOrders(ctx).GetOrderStatuses(order.UUID); err != nil {
		t.Error(err)
		return
	}

	//db.NewODB(ctx).TruncateTables([]string{OrderTable, OrderStatusTable, OrderItemTable})
}
