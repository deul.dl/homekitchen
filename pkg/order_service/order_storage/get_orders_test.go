package order_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrdersSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	userUUID := "b5771632-9578-4f39-b6fc-66ad2efc4a3d"
	order := &entities.Order{
		ExecutorUUID: userUUID,
		CustomerUUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
		StoreUUID:    uuid.New().String(),
		AddressUUID:  handles.NewNullString("d2278709-c0da-4533-bebf-ee0a03c23df4"),
		Description:  "Description",
		Status:       entities.RECEIVED_STATUS,
		TotalPrice:   1500,
		PaymentMethod: &entities.OrderPaymentMethod{
			Type: entities.TRANSLATE_TYPE,
		},
	}

	order.OrderItems = []*entities.OrderItem{
		&entities.OrderItem{
			ItemUUID:  "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
			OrderUUID: order.UUID,
			Name:      "Item 1",
			Price:     500,
			Count:     1,
		},
		&entities.OrderItem{
			ItemUUID:  "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
			OrderUUID: order.UUID,
			Name:      "Item 1",
			Price:     500,
			Count:     1,
		},
		&entities.OrderItem{
			ItemUUID:  "b5771632-9578-4f39-b6fc-66ad2efc4a3d",
			OrderUUID: order.UUID,
			Name:      "Item 1",
			Price:     500,
			Count:     1,
		},
	}

	if err = NewOrders(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	order.UUID = uuid.New().String()

	if err = NewOrders(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	ctx = context.WithValue(ctx, entities.USER_UUID, userUUID)

	rows, err := NewOrders(ctx).GetExecutorOrders()

	if err != nil {
		t.Error(err)
		return
	}
	defer rows.Close()
	var orders entities.Orders
	for rows.Next() {
		order := &entities.Order{}
		address := &entities.Address{}
		customer := &entities.Customer{}
		if err = rows.Scan(
			&order.UUID, &order.StoreUUID,
			&order.ExecutorUUID, &order.CustomerUUID,
			&order.AddressUUID, &order.Time,
			&order.Status, &order.TotalPrice,
			&address.UUID, &address.CustomerUUID,
			&address.Street, &address.House,
			&address.Apartment, &address.Porch,
			&address.City, &address.Country,
			&customer.PhoneNumber,
		); err != nil {
			t.Error(err)
			return
		}

		order.Address = address
		order.Customer = customer

		orders = append(orders, order)
	}

	if rows.Err() != nil {
		t.Error(rows.Err())
	}

	if len(orders) == 0 {
		t.Error("Orders doesn't have to be zero!")
	}

	db.NewODB(ctx).TruncateTables([]string{OrderTable, OrderStatusTable, OrderItemTable})
}
