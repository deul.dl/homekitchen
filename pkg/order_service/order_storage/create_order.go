package order_storage

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
)

type createOrder struct {
	*orderStorage

	order *entities.Order
}

func newCreateOrder(storage *orderStorage, order *entities.Order) *createOrder {
	return &createOrder{orderStorage: storage, order: order}
}

func (action *createOrder) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("orders: %v", err)
		return err
	}

	if err = action.saveOrder(tx); err != nil {
		return err
	}

	if err = action.saveOrderStatus(tx, &entities.OrderStatus{
		OrderUUID: action.order.UUID,
		Status:    action.order.Status,
	}); err != nil {
		return err
	}

	for _, item := range action.order.OrderItems {
		if err = action.saveOrderItem(tx, item); err != nil {
			return err
		}
	}

	if err = action.saveOrderPaymentMethod(tx); err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("orders: %v", err)
	}
	return nil
}

func (action *createOrder) saveOrder(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			INSERT INTO 
				%s 
				(
					store_uuid, status, time,
					executor_uuid, customer_uuid,
					description, total_price, address_uuid
				)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
			RETURNING "uuid";
		`,
		OrderTable,
	)

	inserted, err := action.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("add orders are failed: %v\n", err)
		return nil
	}

	if err := tx.StmtContext(action.ctx, inserted).QueryRow(
		handles.NewNullString(action.order.StoreUUID),
		handles.NewNullString(action.order.Status),
		action.order.Time,
		handles.NewNullString(action.order.ExecutorUUID),
		handles.NewNullString(action.order.CustomerUUID),
		handles.NewNullString(action.order.Description),
		handles.NewNullFloat64(action.order.TotalPrice),
		action.order.AddressUUID,
	).Scan(&action.order.UUID); err != nil {
		log.Printf("add orders are failed: %v", err)
		tx.Rollback()
		return err
	}

	return nil
}

func (action *createOrder) saveOrderItem(tx *sql.Tx, orderItem *entities.OrderItem) error {
	query := fmt.Sprintf(
		`
			INSERT INTO 
				%s (order_uuid, name, price, count, item_uuid, cost)
			VALUES ($1, $2, $3, $4, $5, $6)
		`,
		OrderItemTable,
	)

	inserted, err := action.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("add item are failed: unable to rollback: %v\n", err)
		return nil
	}
	orderItem.OrderUUID = action.order.UUID
	if _, err := tx.StmtContext(action.ctx, inserted).Exec(
		handles.NewNullString(action.order.UUID),
		handles.NewNullString(orderItem.Name),
		handles.NewNullFloat64(orderItem.Price),
		handles.NewNullInt64(orderItem.Count),
		handles.NewNullString(orderItem.ItemUUID),
		handles.NewNullFloat64(orderItem.Cost),
	); err != nil {
		log.Printf("add item are failed: %v\n", err)
		tx.Rollback()
		return err
	}
	return nil
}

func (action *createOrder) saveOrderPaymentMethod(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			INSERT INTO 
				%s (order_uuid, total_price, amount, discount, delivery_price, type, total_cost)
			VALUES ($1, $2, $3, $4, $5, $6, $7)
		`,
		OrderPaymentMethodTable,
	)

	inserted, err := action.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("add order payment method are failed: unable to rollback: %v\n", err)
		return nil
	}
	action.order.PaymentMethod.UUID = action.order.UUID
	if _, err := tx.StmtContext(action.ctx, inserted).Exec(
		handles.NewNullString(action.order.UUID),
		handles.NewNullFloat64(action.order.PaymentMethod.TotalPrice),
		handles.NewNullFloat64(action.order.PaymentMethod.Amount),
		handles.NewNullFloat64(action.order.PaymentMethod.Discount),
		handles.NewNullFloat64(action.order.PaymentMethod.DeliveryPrice),
		handles.NewNullString(action.order.PaymentMethod.Type),
		handles.NewNullFloat64(action.order.PaymentMethod.TotalCost),
	); err != nil {
		log.Printf("add order payment method are failed: %v\n", err)
		tx.Rollback()
		return err
	}
	return nil
}
