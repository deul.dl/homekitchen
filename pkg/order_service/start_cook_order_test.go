package order_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestStartOrder(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	order := &entities.Order{
		ExecutorUUID: uuid.New().String(),
		CustomerUUID: uuid.New().String(),
		StoreUUID:    uuid.New().String(),
		AddressUUID:  handles.NewNullString(uuid.New().String()),
		Description:  "Description",
		Status:       entities.RECEIVED_STATUS,
		TotalPrice:   1500,
	}

	order.OrderItems = []*entities.OrderItem{
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
		},
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
		},
		&entities.OrderItem{
			Name:  "Item 1",
			Price: 500,
		},
	}

	if err = New(ctx).StartCook(order); err != nil {
		t.Error(err)
	}
}
