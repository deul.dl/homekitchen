package entities

import "time"

type OrderStatus struct {
	OrderUUID string
	Status    string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type OrderStatuses []*OrderStatus
