package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type Order struct {
	UUID          string
	StoreUUID     string
	Status        string
	Time          time.Time
	ExecutorUUID  string
	CustomerUUID  string
	TotalPrice    float64
	AddressUUID   handles.NullString
	Description   string
	CreatedAt     time.Time
	UpdatedAt     time.Time
	OrderItems    []*OrderItem
	OrderStatuses []*OrderStatus
	Address       *Address
	Customer      *Customer
	PaymentMethod *OrderPaymentMethod
}

const (
	RECEIVED_STATUS    = "received"
	ACCEPTED_STATUS    = "accepted"
	PROCEESSED_STATUS  = "processing"
	DONE_STATUS        = "ready"
	TRANSACTION_STATUS = "paid"
	COMPLETED_STATUS   = "completed"
	CANCELED_STATUS    = "cancelled"
	REJECTED_STATUS    = "rejected"
)

type Orders []*Order
