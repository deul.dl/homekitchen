package entities

import (
	"time"
)

type OrderItem struct {
	OrderUUID string
	Name      string
	Price     float64
	Cost      float64
	Count     int64
	ItemUUID  string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type OrderItems []*OrderItem
