package entities

type key string

const USER_UUID key = "userUUID"
const ROLE key = "role"
const CUSTOMER = "Customer"
const EXECUTOR = "Executor"

type Customer struct {
	PhoneNumber string
}
