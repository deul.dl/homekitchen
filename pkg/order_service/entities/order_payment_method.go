package entities

import "time"

type OrderPaymentMethod struct {
	UUID          string
	OrderUUID     string
	Type          string
	Discount      float64
	DeliveryPrice float64
	TotalPrice    float64
	TotalCost     float64
	Amount        float64
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

const (
	IN_CASH_TYPE   = "in_cash"
	TRANSLATE_TYPE = "translate"
)
