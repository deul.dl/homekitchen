package entities

import "github.com/homekitchen/homekitchen/internal/handles"

type Address struct {
	UUID         handles.NullString
	CustomerUUID handles.NullString
	Street       handles.NullString
	House        handles.NullString
	Apartment    handles.NullString
	Porch        handles.NullString
	City         handles.NullString
	Country      handles.NullString
	Primary      handles.NullBool
}
