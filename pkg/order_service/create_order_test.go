package order_service

import (
	"context"
	"math"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func Equal(a, b float64) bool {
	ba := math.Float64bits(a)
	bb := math.Float64bits(b)
	diff := ba - bb
	if diff < 0 {
		diff = -diff
	}
	// accept one bit difference
	return diff < 2
}

func TestCreateOrderSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	order := &entities.Order{
		StoreUUID:    "e1be9dc6-7bdd-4795-a399-84f81b36c564",
		ExecutorUUID: "1e0225f1-fb11-4839-b378-7daf28351b1c",
		CustomerUUID: "1e0225f1-fb11-4839-b378-7daf28351b1c",
		AddressUUID:  handles.NewNullString(uuid.New().String()),
		Description:  "Description",
		OrderItems: []*entities.OrderItem{
			&entities.OrderItem{
				ItemUUID: "1d79bcdc-d45a-4f63-b02c-4c2abe7086fe",
				Name:     "Item 1",
				Price:    123,
				Count:    1,
				Cost:     123,
			},
		},
		PaymentMethod: &entities.OrderPaymentMethod{
			Type: entities.TRANSLATE_TYPE,
		},
	}

	if err = New(ctx).CreateOrder(order); err != nil {
		t.Error(err)
	}

	if order.TotalPrice == 1000 {
		t.Error("Total is not right!")
	}
}

func TestCreateOrderWithoutAddressSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	order := &entities.Order{
		StoreUUID:    "e1be9dc6-7bdd-4795-a399-84f81b36c564",
		ExecutorUUID: "1e0225f1-fb11-4839-b378-7daf28351b1c",
		CustomerUUID: "1e0225f1-fb11-4839-b378-7daf28351b1c",
		Description:  "Description",
		Time:         time.Now(),
		OrderItems: []*entities.OrderItem{
			&entities.OrderItem{
				ItemUUID: "cc95b2e0-251d-49e0-bc12-ea217317d59f",
				Name:     "Item 1",
				Price:    123,
				Cost:     123,
				Count:    2,
			},
		},
		PaymentMethod: &entities.OrderPaymentMethod{
			Type: entities.TRANSLATE_TYPE,
		},
	}

	if err = New(ctx).CreateOrder(order); err != nil {
		t.Error(err)
	}

	if Equal(order.TotalPrice, float64(123*2)) {
		t.Errorf("Total: %f == %f", order.TotalPrice, float64(123*2))
	}

	if Equal(order.TotalPrice, float64(123*2)) {
		t.Errorf("Cost: %f == %f", order.PaymentMethod.TotalCost, float64(123*2))
	}
}
