package order_service

import (
	"context"
	"testing"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrders(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx = context.WithValue(ctx, entities.USER_UUID, "aa063b26-9676-4a8a-8e5e-3d5c2ff32ec5")
	order := &entities.Order{
		StoreUUID:    "0c7931ca-d82a-400a-8521-248c4445d901",
		ExecutorUUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
		CustomerUUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
		AddressUUID:  handles.NewNullString("f0e5cfef-3d1d-489e-8df4-046c1aa684ca"),
		Description:  "Description",
		Time:         time.Now(),
		OrderItems: []*entities.OrderItem{
			&entities.OrderItem{
				ItemUUID: "d6c36b37-6287-45b3-b5de-a1e8a38193d9",
				Name:     "Item 1",
				Price:    500,
				Count:    1,
			},
			&entities.OrderItem{
				ItemUUID: "5762b21b-40a9-497c-ab14-9305bc0371ea",
				Name:     "Item 1",
				Price:    500,
				Count:    2,
			},
		},
		PaymentMethod: &entities.OrderPaymentMethod{
			Type: entities.TRANSLATE_TYPE,
		},
	}

	if err = New(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	orders, err := New(ctx).GetExecutorOrders()
	if err != nil {
		t.Error(err)
		return
	}

	if len(orders) > 0 {
		t.Error("Order Items doesn't have to be zero")
	}
}

func TestGetCustomerOrders(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx = context.WithValue(ctx, entities.USER_UUID, "d9739d52-2fcd-452b-a004-bcbfd0dbb41d")

	order := &entities.Order{
		StoreUUID:    "0c7931ca-d82a-400a-8521-248c4445d901",
		ExecutorUUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
		CustomerUUID: "d9739d52-2fcd-452b-a004-bcbfd0dbb41d",
		AddressUUID:  handles.NewNullString("f0e5cfef-3d1d-489e-8df4-046c1aa684ca"),
		Description:  "Description",
		Time:         time.Now(),
		OrderItems: []*entities.OrderItem{
			&entities.OrderItem{
				ItemUUID: "cc95b2e0-251d-49e0-bc12-ea217317d59f",
				Name:     "Item 1",
				Price:    123,
				Count:    1,
			},
		},
		PaymentMethod: &entities.OrderPaymentMethod{
			Type: entities.TRANSLATE_TYPE,
		},
	}

	if err = New(ctx).CreateOrder(order); err != nil {
		t.Error(err)
		return
	}

	orders, err := New(ctx).GetCustomerOrders()
	if err != nil {
		t.Error(err)
		return
	}

	if len(orders) == 0 {
		t.Errorf("Order Items doesn't have to be zero %d", len(orders))
	}
}
