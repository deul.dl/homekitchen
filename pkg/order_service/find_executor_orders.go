package order_service

import (
	"context"
	"database/sql"
	"log"

	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type FindExecutorOrders struct {
	ctx     context.Context
	service *orderService
}

func newExecutorFind(ctx context.Context, service *orderService) *FindExecutorOrders {
	return &FindExecutorOrders{ctx, service}
}

func (find *FindExecutorOrders) run() (orders entities.Orders, err error) {
	rows, err := find.service.orderStorage.GetExecutorOrders()
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		order, err := find.mapOrderWithAddressAndClient(rows)
		if err != nil {
			log.Printf("orders: %v", err)
			return nil, err
		}

		client, err := users.NewClients(find.ctx).
			GetClientByField("uuid", order.CustomerUUID)
		if err != nil {
			log.Printf("orders: %v", err)
			return nil, err
		}

		order.Customer = &entities.Customer{
			PhoneNumber: client.PhoneNumber.String,
		}

		orders = append(orders, order)
	}
	if rows.Err() != nil {
		return orders, rows.Err()
	}

	return orders, err
}

func (*FindExecutorOrders) mapOrderWithAddressAndClient(rows *sql.Rows) (order *entities.Order, err error) {
	order = &entities.Order{}
	address := &entities.Address{}
	customer := &entities.Customer{}
	if err = rows.Scan(
		&order.UUID, &order.StoreUUID,
		&order.ExecutorUUID, &order.CustomerUUID,
		&order.AddressUUID, &order.Time,
		&order.Status, &order.TotalPrice,
		&address.UUID, &address.CustomerUUID,
		&address.Street, &address.House,
		&address.Apartment, &address.Porch,
		&address.City, &address.Country,
		&customer.PhoneNumber,
	); err != nil {
		log.Printf("orders: %v", err)
		return nil, err
	}

	order.Address = address
	order.Customer = customer

	if err != nil {
		log.Printf("orders: %v", err)
		return nil, err
	}

	return
}
