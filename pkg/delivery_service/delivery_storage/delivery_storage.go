package delivery_storage

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/homekitchen/homekitchen/pkg/db"
)

const DeliveryPriceTable = "delivery_prices"

type DeliveryStorage interface {
	GetDeliveryPriceByCity(string) *sql.Row
}

type develiveryStorage struct {
	DeliveryStorage

	ctx      context.Context
	database *sql.DB
}

func New(ctx context.Context) DeliveryStorage {

	return &develiveryStorage{
		ctx:      ctx,
		database: db.Pool,
	}
}

func (storage *develiveryStorage) GetDeliveryPriceByCity(city string) *sql.Row {
	query := fmt.Sprintf(`
			SELECT
				uuid, country_code, country, 
				city, price
			FROM %s
			WHERE
				city = $1
			LIMIT 1
		`,
		DeliveryPriceTable)

	return storage.database.
		QueryRowContext(storage.ctx, query, city)
}
