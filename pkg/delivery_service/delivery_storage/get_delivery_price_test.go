package delivery_storage

import (
	"context"
	"database/sql"
	"log"
	"testing"

	"github.com/homekitchen/homekitchen/pkg/delivery_service/entities"

	"github.com/joho/godotenv"
)

func TestGetDeliveryPriceSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	row := New(ctx).GetDeliveryPriceByCity("Topar")
	develiveryPrice := &entities.DeliveryPrice{}
	err = row.Scan(
		&develiveryPrice.UUID,
		&develiveryPrice.CountryCode,
		&develiveryPrice.Country,
		&develiveryPrice.City,
		&develiveryPrice.Price,
	)

	if err == sql.ErrNoRows {
		t.Error(err)
		return
	} else if err != nil {
		log.Printf("store: %v", err)
		t.Error(err)
		return
	}
}
