package delivery_service

import (
	"context"
	"testing"

	"github.com/joho/godotenv"
)

func TestGetDeliveryPrice(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	deliveryPrice, err := New(ctx).GetDeliveryPriceByCity("Topar")
	if err != nil {
		t.Error(err)
		return
	}
	t.Error(deliveryPrice)
}
