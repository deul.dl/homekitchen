package delivery_service

import (
	"context"

	"github.com/homekitchen/homekitchen/pkg/delivery_service/delivery_storage"
	"github.com/homekitchen/homekitchen/pkg/delivery_service/entities"
)

type DeliveryService interface {
	GetDeliveryPriceByCity(string) (*entities.DeliveryPrice, error)
}

type deliveryPrice struct {
	DeliveryService

	ctx     context.Context
	storage delivery_storage.DeliveryStorage
}

func New(ctx context.Context) DeliveryService {
	return &deliveryPrice{
		ctx:     ctx,
		storage: delivery_storage.New(ctx),
	}
}

func (service *deliveryPrice) GetDeliveryPriceByCity(city string) (*entities.DeliveryPrice, error) {
	row := service.storage.GetDeliveryPriceByCity(city)
	deliveryPrice := &entities.DeliveryPrice{}
	if err := row.Scan(
		&deliveryPrice.UUID,
		&deliveryPrice.CountryCode,
		&deliveryPrice.Country,
		&deliveryPrice.City,
		&deliveryPrice.Price,
	); err != nil {
		return nil, err
	}
	return deliveryPrice, nil
}
