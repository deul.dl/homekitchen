package entities

import "time"

type DeliveryPrice struct {
	UUID        string
	CountryCode string
	Country     string
	City        string
	Price       float64
	UpdatedAt   time.Time
	CreatedAt   time.Time
}
