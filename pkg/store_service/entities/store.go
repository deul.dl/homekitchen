package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type key string

const USER_UUID key = "userUUID"

const CUSTOMER = "Customer"
const EXECUTOR = "Executor"

type Store struct {
	UUID        string
	UserUUID    string
	Name        string
	Description handles.NullString
	ImageSource string
	IsTurnOn    bool
	PhoneNumber string
	IsDelivery  bool
	StartedAt   handles.NullInt64
	EndedAt     handles.NullInt64
	CreatedAt   time.Time
	UpdatedAt   time.Time

	StoreDelivery *StoreDelivery
}

type StoreWithItems struct {
	UUID        string
	Name        string
	Description handles.NullString
	ImageSource string
	UserUUID    string
	StartedAt   int64
	EndedAt     int64
	Items       Items
}

type Stories []*Store
type StoriesWithItems []*StoreWithItems
