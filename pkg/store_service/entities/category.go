package entities

import (
	"time"
)

type Category struct {
	ID        int64
	Position  int64
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Categories []*Category
