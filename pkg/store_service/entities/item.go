package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type Item struct {
	UUID        string
	UserUUID    string
	CategoryID  int64
	Name        string
	Description string
	Price       float64
	Cost        float64
	ImageSource handles.NullString
	IsTurnOn    bool
	StoreUUID   string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   time.Time
	Category
}

type Items []*Item
