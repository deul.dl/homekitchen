package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type DeliveryPrice struct {
	UUID        handles.NullString
	CountryCode string
	Country     string
	City        string
	Price       handles.NullFloat64
	UpdatedAt   time.Time
	CreatedAt   time.Time
}

type StoreDelivery struct {
	UUID              string
	StoreUUID         string
	UserUUID          string
	Type              string
	Price             handles.NullFloat64
	DeliveryPriceUUID handles.NullString
	CreatedAt         time.Time
	UpdatedAt         time.Time
	DeliveryPrice     *DeliveryPrice
}

const (
	HOME_KITCHEN_DELIVERY = "hk_delivery_service"
	COOKER_DELIVERY       = "ck_delivery_service"
)
