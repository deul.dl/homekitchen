package store_storage

import (
	"context"
	"database/sql"
	"log"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestGetStoreByField(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	userUUID := uuid.New().String()
	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    userUUID,
		ImageSource: "./asd/aa.jpg",
		StartedAt:   handles.NewNullInt64(1),
		EndedAt:     handles.NewNullInt64(3),
		StoreDelivery: &entities.StoreDelivery{
			Type:     entities.COOKER_DELIVERY,
			UserUUID: uuid.New().String(),
		}}
	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
		return
	}

	row := New(ctx).GetStoreByField("uuid", store.UUID)
	if row.Err() != nil {
		t.Error(row.Err())
		return
	}

	store = &entities.Store{}

	err = row.Scan(
		&store.UUID,
		&store.Name,
		&store.Description,
		&store.UserUUID,
		&store.ImageSource,
		&store.StartedAt,
		&store.EndedAt,
	)

	if err == sql.ErrNoRows {
		t.Error(err)
		return
	} else if err != nil {
		log.Printf("store: %v", err)
		t.Error(err)
		return
	}

}
