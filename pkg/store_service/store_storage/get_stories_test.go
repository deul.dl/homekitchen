package store_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/homekitchen/homekitchen/pkg/store_service/items"
	"github.com/joho/godotenv"
)

func TestStoriesOrders(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    uuid.New().String(),
		ImageSource: "./asd/aa.jpg",
		StartedAt:   handles.NewNullInt64(1),
		EndedAt:     handles.NewNullInt64(3),
		StoreDelivery: &entities.StoreDelivery{
			Type:     entities.COOKER_DELIVERY,
			UserUUID: uuid.New().String(),
		},
	}

	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
		return
	}

	store.UserUUID = uuid.New().String()

	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
		return
	}

	item1 := &entities.Item{
		Name:        "Store 1",
		StoreUUID:   store.UUID,
		Description: "Test create store",
		Price:       123,
		Cost:        123,
		UserUUID:    store.UserUUID,
		IsTurnOn:    true,
		CategoryID:  1,
	}

	item2 := &entities.Item{
		Name:        "Store 2",
		StoreUUID:   store.UUID,
		Description: "Test create store",
		Price:       123,
		Cost:        123,
		UserUUID:    store.UserUUID,
		IsTurnOn:    true,
		CategoryID:  1,
	}

	if err = items.New(ctx).CreateItem(item1); err != nil {
		t.Error(err)
		return
	}

	if err = items.New(ctx).CreateItem(item2); err != nil {
		t.Error(err)
		return
	}

	filter := &Filter{
		City: "Mountain View",
	}

	rows, err := New(ctx).FindStories(filter)

	if err != nil {
		t.Error(err)
		return
	}
	defer rows.Close()
	var stories entities.Stories
	for rows.Next() {
		if err = rows.Scan(
			&store.UUID,
			&store.Name,
			&store.Description,
			&store.UserUUID,
			&store.ImageSource,
			&store.StartedAt,
			&store.EndedAt,
		); err != nil {
			t.Error(err)
			return
		}

		stories = append(stories, store)
	}
	if rows.Err(); err != nil {
		t.Error(err)
		return
	}

	if len(stories) == 0 {
		t.Error("Stories is not be!!!")
	}

	db.NewODB(ctx).TruncateTables([]string{StoreTable, ItemTable})
}
