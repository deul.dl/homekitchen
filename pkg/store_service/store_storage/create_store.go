package store_storage

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type createStore struct {
	*storeStorage

	store *entities.Store
}

func newCreateStore(storage *storeStorage, store *entities.Store) *createStore {
	return &createStore{storeStorage: storage, store: store}
}

func (action *createStore) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("stories: %v", err)
		return err
	}

	if err := action.save(tx); err != nil {
		log.Printf("stories: %v", err)
		return err
	}

	if err := action.saveStoreDelivery(tx, action.store.StoreDelivery); err != nil {
		log.Printf("store delivery: %v", err)
		return err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("stories: %v", err)
	}
	return nil
}

func (action *createStore) save(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			INSERT INTO %s
				(user_uuid, name, description, image_source, phone_number, is_turn_on, started_at, ended_at)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
			RETURNING "uuid";
		`,
		StoreTable,
	)

	inserted, err := action.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("stories: unable to rollback: %v\n", err)
		return nil
	}

	if err := tx.StmtContext(action.ctx, inserted).QueryRow(
		handles.NewNullString(action.store.UserUUID),
		handles.NewNullString(action.store.Name),
		action.store.Description,
		handles.NewNullString(action.store.ImageSource),
		handles.NewNullString(action.store.PhoneNumber),
		handles.NewNullBool(action.store.IsTurnOn),
		action.store.StartedAt,
		action.store.EndedAt,
	).Scan(&action.store.UUID); err != nil {
		log.Printf("stories: add store failed: %v", err)
		tx.Rollback()
		return err
	}
	return nil
}

func (action *createStore) saveStoreDelivery(tx *sql.Tx, storeDelivery *entities.StoreDelivery) error {
	query := fmt.Sprintf(
		`
			INSERT INTO %s
				(user_uuid, store_uuid, type, delivery_price_uuid)
			VALUES ($1, $2, $3, $4)
		`,
		StoreDeliveryTable,
	)

	inserted, err := action.database.Prepare(query)
	defer inserted.Close()

	if err != nil {
		log.Printf("stories: unable to rollback: %v\n", err)
		return nil
	}
	storeDelivery.StoreUUID = action.store.UUID
	if _, err := tx.StmtContext(action.ctx, inserted).Exec(
		handles.NewNullString(storeDelivery.UserUUID),
		handles.NewNullString(storeDelivery.StoreUUID),
		handles.NewNullString(storeDelivery.Type),
		storeDelivery.DeliveryPriceUUID,
	); err != nil {
		log.Printf("stories: add store delivery failed: %v", err)
		tx.Rollback()
		return err
	}
	return nil
}
