package store_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestChangeTypeOfDelivery(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables([]string{StoreTable})
	userUUID := uuid.New().String()
	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    userUUID,
		ImageSource: "./asd/aa.jpg",
		StoreDelivery: &entities.StoreDelivery{
			Type:              entities.HOME_KITCHEN_DELIVERY,
			UserUUID:          userUUID,
			DeliveryPriceUUID: handles.NewNullString(userUUID),
		},
	}

	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
		return
	}

	if err = New(ctx).UpdateStoreDelivery(&entities.StoreDelivery{}); err != nil {
		t.Error(err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{StoreTable})
}
