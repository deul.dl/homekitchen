package store_storage

import (
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type updateDeliveryStore struct {
	*storeStorage

	storeDelivery *entities.StoreDelivery
}

func newUpdateDeliveryStore(storage *storeStorage, storeDelivery *entities.StoreDelivery) *updateDeliveryStore {
	return &updateDeliveryStore{storeStorage: storage, storeDelivery: storeDelivery}
}

func (action *updateDeliveryStore) run() error {
	query := fmt.Sprintf(
		`
			UPDATE %s
			SET
				type=$1,
				price=$2,
				delivery_price_uuid=$3,
				updated_at=$4
			WHERE store_uuid=$5
		`,
		StoreDeliveryTable,
	)

	updated, err := action.database.PrepareContext(action.ctx, query)
	defer updated.Close()
	if err != nil {
		log.Printf("stories: unable to rollback: %v\n", err)
		return err
	}

	if _, err := updated.Exec(
		handles.NewNullString(action.storeDelivery.Type),
		action.storeDelivery.Price,
		action.storeDelivery.DeliveryPriceUUID,
		action.storeDelivery.UpdatedAt,
		handles.NewNullString(action.storeDelivery.StoreUUID),
	); err != nil {
		log.Printf("stories: update store failed: %v\n", err)
		return err
	}
	return nil
}
