package store_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateStoreSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	userUUID := uuid.New().String()
	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    userUUID,
		ImageSource: "./asd/aa.jpg",
		StartedAt:   handles.NewNullInt64(1),
		EndedAt:     handles.NewNullInt64(3),
		StoreDelivery: &entities.StoreDelivery{
			Type:     entities.COOKER_DELIVERY,
			UserUUID: userUUID,
		},
	}

	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
		return
	}

	store.Name = "Store 2"
	if err = New(ctx).UpdateStore(store); err != nil {
		t.Error(err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{StoreTable})
}
