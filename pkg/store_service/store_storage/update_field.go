package store_storage

import (
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type updateFieldStore struct {
	modal *storeRecord

	field string
	value interface{}
}

func newUpdateFiled(modal *storeRecord, field string, value interface{}) *updateFieldStore {
	return &updateFieldStore{modal, field, value}
}

func (action *updateFieldStore) run() error {

	action.beforeUpdate()

	if err := action.save(); err != nil {
		log.Printf("store: %v\n", err)
		return err
	}

	return nil
}

func (action *updateFieldStore) beforeUpdate() {
	action.modal.UpdatedAt = time.Now()
}

func (action *updateFieldStore) save() error {
	query := fmt.Sprintf(
		`
			UPDATE %s
			SET
				%s =$1,
				updated_at =$2
			WHERE "uuid" = $3
		`,
		StoreTable,
		action.field,
	)

	updated, err := action.modal.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("store: unable to rollback: %v\n", err)
		return err
	}

	if _, err := updated.ExecContext(
		action.modal.ctx,
		action.value,
		action.modal.UpdatedAt,
		handles.NewNullString(action.modal.UUID),
	); err != nil {
		log.Printf("store: update store failed: %v\n", err)
		return err
	}
	return nil
}
