package store_storage

import (
	"context"
	"database/sql"

	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type Record interface {
	Update(field string, value interface{}) error
}

type storeRecord struct {
	Record
	entities.Store

	ctx      context.Context
	database *sql.DB
}

func NewStoreRecord(uuid string, database *sql.DB, ctx context.Context) Record {
	return &storeRecord{
		Store:    entities.Store{UUID: uuid},
		ctx:      ctx,
		database: database,
	}
}

func (modal *storeRecord) Update(field string, value interface{}) error {
	return newUpdateFiled(modal, field, value).run()
}
