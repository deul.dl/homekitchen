package store_storage

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateOneStoreSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    uuid.New().String(),
		ImageSource: "./asd/aa.jpg",
		StartedAt:   handles.NewNullInt64(1),
		EndedAt:     handles.NewNullInt64(3),
		StoreDelivery: &entities.StoreDelivery{
			Type:     entities.COOKER_DELIVERY,
			UserUUID: uuid.New().String(),
		},
	}

	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
		return
	}

	if err = New(ctx).ByUUID(store.UUID).Update("name", "Store 3"); err != nil {
		t.Error(err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{StoreTable})
}
