package store_storage

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestStoriesOrdersByCategoryID(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	filter := &Filter{
		City:       "Mountain View",
		CategoryID: 1,
	}
	rows, err := New(ctx).FindStories(filter)

	if err != nil {
		t.Error(err)
		return
	}
	defer rows.Close()
	var stories entities.Stories
	for rows.Next() {
		store := &entities.Store{}
		if err = rows.Scan(
			&store.UUID,
			&store.Name,
			&store.Description,
			&store.UserUUID,
			&store.ImageSource,
		); err != nil {
			t.Error(err)
			return
		}

		stories = append(stories, store)
	}

	if rows.Err() != nil {
		t.Error(rows.Err())
	}

	if len(stories) == 0 {
		t.Error("Stories is not be")
	}

	//db.NewODB(ctx).TruncateTables([]string{StoreTable})
}
