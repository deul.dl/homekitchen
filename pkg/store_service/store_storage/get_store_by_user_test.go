package store_storage

import (
	"context"
	"database/sql"
	"errors"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestGetStoreByUserUUIDSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	db.NewODB(ctx).TruncateTables([]string{StoreTable})
	userUUID := uuid.New().String()
	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    userUUID,
		ImageSource: "./asd/aa.jpg",
		StoreDelivery: &entities.StoreDelivery{
			Type:     entities.COOKER_DELIVERY,
			UserUUID: userUUID,
		},
	}

	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
		return
	}

	row := New(ctx).GetStoreByUserUUID(store.UserUUID)
	if row.Err() != nil {
		t.Error(row.Err())
		return
	}

	store = &entities.Store{}
	err = row.Scan(
		&store.UUID,
		&store.Name,
		&store.Description,
		&store.UserUUID,
		&store.PhoneNumber,
		&store.ImageSource,
		&store.IsTurnOn,
	)
	if err == sql.ErrNoRows {
		t.Error(errors.New("Store is not exits"))
	} else if err != nil {
		t.Error(err)
	}

	db.NewODB(ctx).TruncateTables([]string{StoreTable})
}
