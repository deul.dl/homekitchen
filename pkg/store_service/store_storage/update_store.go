package store_storage

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type updateStore struct {
	*storeStorage

	store *entities.Store
}

func newUpdateStore(storage *storeStorage, store *entities.Store) *updateStore {
	return &updateStore{storeStorage: storage, store: store}
}

func (action *updateStore) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("stories: %v\n", err)
		return err
	}

	action.beforeUpdate(tx)

	if err := action.save(tx); err != nil {
		log.Printf("stories: %v\n", err)
		return err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("stories: %v\n", err)
	}
	return nil
}

func (action *updateStore) beforeUpdate(tx *sql.Tx) {
	action.store.UpdatedAt = time.Now()
}

func (action *updateStore) save(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			UPDATE %s
			SET
				name =$1,
				description=$2,
				phone_number=$3,
				updated_at =$4,
				started_at = $5,
				ended_at = $6
			WHERE "uuid" = $7
		`,
		StoreTable,
	)

	updated, err := action.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("stories: unable to rollback: %v\n", err)
		return err
	}

	if _, err := tx.StmtContext(action.ctx, updated).Exec(
		handles.NewNullString(action.store.Name),
		action.store.Description,
		handles.NewNullString(action.store.PhoneNumber),
		action.store.UpdatedAt,
		action.store.StartedAt,
		action.store.EndedAt,
		handles.NewNullString(action.store.UUID),
	); err != nil {
		log.Printf("stories: update store failed: %v\n", err)
		tx.Rollback()
		return err
	}
	return nil
}
