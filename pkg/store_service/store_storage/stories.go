package store_storage

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

const StoreTable = "stories"
const ItemTable = "items"
const StoreDeliveryTable = "store_deliveries"

type Filter struct {
	City       string
	CategoryID int64
}

type Storage interface {
	GetStoreByUserUUID(string) *sql.Row
	CreateStore(*entities.Store) error
	UpdateStore(*entities.Store) error
	FindStories(*Filter) (*sql.Rows, error)
	GetStoreByField(string, interface{}) *sql.Row
	ByUUID(string) Record
	UpdateStoreDelivery(*entities.StoreDelivery) error
	GetStoreDeliveryByStoreUUID(string) *sql.Row
}

type storeStorage struct {
	Storage

	ctx      context.Context
	database *sql.DB
}

func New(ctx context.Context) Storage {

	return &storeStorage{ctx: ctx, database: db.Pool}
}

func (storage *storeStorage) GetStoreByUserUUID(userUUID string) *sql.Row {
	query := fmt.Sprintf(`
			SELECT
				"uuid", name,
				description, user_uuid,
				phone_number, image_source,
				is_turn_on, started_at, ended_at
			FROM %s
			WHERE user_uuid=$1
		`,
		StoreTable)

	return storage.database.
		QueryRowContext(storage.ctx, query, userUUID)
}

func (storage *storeStorage) UpdateStoreDelivery(storeDelivery *entities.StoreDelivery) error {
	return newUpdateDeliveryStore(storage, storeDelivery).run()
}

func (storage *storeStorage) CreateStore(store *entities.Store) error {
	return newCreateStore(storage, store).run()
}

func (storage *storeStorage) UpdateStore(store *entities.Store) error {
	return newUpdateStore(storage, store).run()
}

func (storage *storeStorage) FindStories(filter *Filter) (*sql.Rows, error) {
	args := make([]interface{}, 0, 1)
	args = append(args, filter.City)

	query := fmt.Sprintf(`
		SELECT
			stories.uuid, stories.name,
			stories.description, stories.user_uuid,
			stories.image_source, stories.started_at,
			stories.ended_at
		FROM
			%s
		INNER JOIN
			executor_addresses
		ON
			executor_addresses.city = $1
		AND
			executor_addresses.uuid = stories.user_uuid
	`, StoreTable)

	if filter.CategoryID != 0 {
		args = append(args, filter.CategoryID)
		query += fmt.Sprintf(`
			INNER JOIN %s
			ON
				%s.uuid = items.store_uuid	
			AND
				items.category_id = $2`, ItemTable, StoreTable)
	}
	query += " WHERE stories.is_turn_on=true GROUP BY stories.uuid"

	log.Println(query)
	return storage.database.
		QueryContext(storage.ctx, query, args...)
}

func (storage *storeStorage) GetStoreByField(field string, value interface{}) *sql.Row {
	query := fmt.Sprintf(`
			SELECT
				"uuid", name,
				description, user_uuid,
				image_source, started_at,
				ended_at
			FROM %s
			WHERE
				%s = $1
		`,
		StoreTable, field)

	return storage.database.QueryRow(query, value)
}

func (storage *storeStorage) GetStoreDeliveryByStoreUUID(uuid string) *sql.Row {
	query := fmt.Sprintf(`
			SELECT
				uuid, price, user_uuid
			FROM %s
			WHERE
				store_uuid = $1
		`,
		StoreDeliveryTable)

	return storage.database.QueryRowContext(storage.ctx, query, uuid)
}

func (storage *storeStorage) ByUUID(uuid string) Record {
	return NewStoreRecord(uuid, storage.database, storage.ctx)
}
