package store_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateImageOfStoreSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	item := &entities.Item{
		Name:        "Item 1",
		StoreUUID:   uuid.New().String(),
		Description: "Test create item",
		Price:       123,
		UserUUID:    uuid.New().String(),
		IsTurnOn:    true,
		ImageSource: handles.NewNullString("./asd/aa.jpg"),
		CategoryID:  1,
	}
	if err = GetItemService(ctx).AddItem(item); err != nil {
		t.Error(err)
	}

	if err = GetItemService(ctx).UpdateItemImage(item.UUID, "https://image.com"); err != nil {
		t.Error(err)
	}
}
