package store_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func CreateItemSuccess(t *testing.T, store *entities.Store) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	item := &entities.Item{
		Name:        "Store 1",
		StoreUUID:   store.UUID,
		Description: "Test create store",
		Price:       123,
		Cost:        123,
		UserUUID:    "1e0225f1-fb11-4839-b378-7daf28351b1c",
		IsTurnOn:    true,
		ImageSource: handles.NewNullString("./asd/aa.jpg"),
		CategoryID:  1,
	}

	if err = GetItemService(ctx).AddItem(item); err != nil {
		t.Error(err)
	}
}

func TestCreateItem(t *testing.T) {
	CreateItemSuccess(t, &entities.Store{
		UUID: "e1be9dc6-7bdd-4795-a399-84f81b36c564",
	})
}
