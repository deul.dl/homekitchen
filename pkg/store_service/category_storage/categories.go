package categoryStorage

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/homekitchen/homekitchen/pkg/db"
)

const CategoryTable = "categories"

type Storage interface {
	GetCategories() (*sql.Rows, error)
	GetIsNotCategories() (*sql.Rows, error)
}

type categoryStorage struct {
	Storage

	ctx context.Context
	db  *sql.DB
}

func New(ctx context.Context) Storage {

	return &categoryStorage{ctx: ctx, db: db.Pool}
}

func (storage *categoryStorage) GetCategories() (*sql.Rows, error) {
	query := fmt.Sprintf(`
			SELECT
				id,
				name,
				position
			FROM %s
		`,
		CategoryTable)
	return storage.db.
		QueryContext(storage.ctx, query)
}

func (storage *categoryStorage) GetIsNotCategories() (*sql.Rows, error) {
	query := fmt.Sprintf(`
			SELECT
			categories.id,
			categories.name,
			categories.position
			FROM %s
				INNER JOIN items
				ON id = items.category_id
			GROUP BY categories.id
		`,
		CategoryTable)
	return storage.db.QueryContext(storage.ctx, query)
}
