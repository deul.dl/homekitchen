package categoryStorage

import (
	"context"
	"log"
	"testing"

	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestGetCategoriesSuccess(t *testing.T) {

	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	rows, err := New(ctx).GetCategories()
	if err != nil {
		log.Printf("categories: %v", err)
		return
	}

	var categories entities.Categories
	for rows.Next() {
		category := &entities.Category{}
		if err = rows.Scan(
			&category.ID,
			&category.Name,
			&category.Position,
		); err != nil {
			t.Errorf("categories: %v", err)
			return
		}
		categories = append(categories, category)
	}

	t.Log(categories)

	if len(categories) < 1 {
		t.Error("no categories")
		return
	}
}

func TestGetCategoriesWithItemsSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	rows, err := New(ctx).GetIsNotCategories()
	var categories entities.Categories

	for rows.Next() {
		category := &entities.Category{}
		if err := rows.Scan(&category.ID, &category.Name, &category.Position); err != nil {
			t.Error(err)
			return
		}

		categories = append(categories, category)
	}

	if len(categories) < 1 {
		t.Error("no categories")
		return
	}
}
