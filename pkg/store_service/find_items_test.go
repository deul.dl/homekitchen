package store_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/homekitchen/homekitchen/pkg/store_service/items"
	"github.com/joho/godotenv"
)

func TestFindItems(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	userUUID := "d9739d52-2fcd-452b-a004-bcbfd0dbb41d"

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx = context.WithValue(ctx, "user_uuid", userUUID)

	store := &entities.Store{
		UUID: "508b3430-87ab-41e2-a2de-bdb981bb229c",
	}

	item := &entities.Item{
		Name:        "Store 1",
		StoreUUID:   store.UUID,
		Description: "Test create store",
		Price:       123,
		Cost:        123,
		UserUUID:    userUUID,
		IsTurnOn:    true,
		ImageSource: handles.NewNullString("./asd/aa.jpg"),
		CategoryID:  1,
	}

	if err = GetItemService(ctx).AddItem(item); err != nil {
		t.Error(err)
	}

	if err = GetItemService(ctx).AddItem(item); err != nil {
		t.Error(err)
	}

	if err = GetItemService(ctx).AddItem(item); err != nil {
		t.Error(err)
	}

	items, err := GetItemService(ctx).FindItems(&items.Filter{
		UserUUID: item.UserUUID,
	})
	if err != nil {
		t.Error(err)
	}

	if len(items) == 0 {
		t.Error("items is empty")
	}
}
