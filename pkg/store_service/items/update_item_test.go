package items

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateTestItemSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables([]string{ItemTable})

	item := &entities.Item{
		Name:        "Item 1",
		StoreUUID:   uuid.New().String(),
		Description: "Test create item",
		Price:       123,
		Cost:        123,
		UserUUID:    uuid.New().String(),
		IsTurnOn:    true,
		CategoryID:  1,
	}

	if err = New(ctx).CreateItem(item); err != nil {
		t.Error(err)
		return
	}

	item.Name = "Item 2"

	if err = New(ctx).UpdateItem(item); err != nil {
		t.Error(err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{ItemTable})
}
