package items

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type createItem struct {
	*itemStorage

	item *entities.Item
}

func newCreateItem(storage *itemStorage, item *entities.Item) *createItem {
	return &createItem{itemStorage: storage, item: item}
}

func (action *createItem) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("items: %v", err)
		return err
	}

	if err := action.save(tx); err != nil {
		log.Printf("items: %v", err)
		return err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("items: %v", err)
	}
	return nil
}

func (action *createItem) save(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			INSERT INTO %s 
				(
					user_uuid, store_uuid,
					name, description, price,
					is_turn_on, category_id,
					cost
				)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
			RETURNING "uuid";
		`,
		ItemTable,
	)

	inserted, err := action.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("items: unable to rollback: %v\n", err)
		return nil
	}

	if err := tx.StmtContext(action.ctx, inserted).QueryRow(
		handles.NewNullString(action.item.UserUUID),
		handles.NewNullString(action.item.StoreUUID),
		handles.NewNullString(action.item.Name),
		handles.NewNullString(action.item.Description),
		handles.NewNullFloat64(action.item.Price),
		handles.NewNullBool(action.item.IsTurnOn),
		handles.NewNullInt64(action.item.CategoryID),
		handles.NewNullFloat64(action.item.Cost),
	).Scan(&action.item.UUID); err != nil {
		log.Printf("items: add store failed: %v", err)
		tx.Rollback()
		return err
	}
	return nil
}
