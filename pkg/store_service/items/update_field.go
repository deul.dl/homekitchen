package items

import (
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type updateFieldOfItem struct {
	modal *itemRecord

	field string
	value interface{}
}

func newUpdateFiledOfItem(modal *itemRecord, field string, value interface{}) *updateFieldOfItem {
	return &updateFieldOfItem{modal, field, value}
}

func (action *updateFieldOfItem) run() error {

	action.beforeUpdate()

	if err := action.save(); err != nil {
		log.Printf("item: %v", err)
		return err
	}

	return nil
}

func (action *updateFieldOfItem) beforeUpdate() {
	action.modal.UpdatedAt = time.Now()
}

func (action *updateFieldOfItem) save() error {
	query := fmt.Sprintf(
		`
			UPDATE %s
			SET
				%s = $1,
				updated_at = $2
			WHERE "uuid" = $3
		`,
		ItemTable,
		action.field,
	)

	updated, err := action.modal.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("item: unable to rollback: %v\n", err)
		return nil
	}

	if _, err := updated.ExecContext(
		action.modal.ctx,
		action.value,
		action.modal.UpdatedAt,
		handles.NewNullString(action.modal.UUID),
	); err != nil {
		log.Printf("item: update item failed: %v", err)
		return err
	}
	return nil
}
