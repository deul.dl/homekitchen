package items

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type updateItem struct {
	*itemStorage

	item *entities.Item
}

func newUpdateItem(storage *itemStorage, item *entities.Item) *updateItem {
	return &updateItem{itemStorage: storage, item: item}
}

func (action *updateItem) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("items: %v", err)
		return err
	}

	action.beforeUpdate(tx)

	if err := action.save(tx); err != nil {
		log.Printf("items: %v", err)
		return err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("items: %v", err)
	}
	return nil
}

func (action *updateItem) beforeUpdate(tx *sql.Tx) {
	action.item.UpdatedAt = time.Now()
}

func (action *updateItem) save(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			UPDATE
				%s
			SET
				name =$1,
				description =$2,
				price =$3,
				cost =$4,
				is_turn_on =$5,
				category_id =$6,
				updated_at =$7
			WHERE "uuid" = $8
		`,
		ItemTable,
	)

	updated, err := action.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("items: unable to rollback: %v\n", err)
		return nil
	}

	if _, err := tx.StmtContext(action.ctx, updated).Exec(
		handles.NewNullString(action.item.Name),
		handles.NewNullString(action.item.Description),
		handles.NewNullFloat64(action.item.Price),
		handles.NewNullFloat64(action.item.Cost),
		handles.NewNullBool(action.item.IsTurnOn),
		handles.NewNullInt64(action.item.CategoryID),
		action.item.UpdatedAt,
		handles.NewNullString(action.item.UUID),
	); err != nil {
		log.Printf("items: add store failed: %v", err)
		tx.Rollback()
		return err
	}
	return nil
}
