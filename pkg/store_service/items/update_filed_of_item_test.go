package items

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateFieldOfStoreSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	item := &entities.Item{
		Name:        "Store 1",
		StoreUUID:   uuid.New().String(),
		Description: "Test create store",
		Price:       123,
		UserUUID:    uuid.New().String(),
		IsTurnOn:    true,
		CategoryID:  1,
	}
	if err = New(ctx).CreateItem(item); err != nil {
		t.Error(err)
		return
	}

	if err = New(ctx).ByUUID(item.UUID).Update("name", "Item 3"); err != nil {
		t.Error(err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{ItemTable})
}
