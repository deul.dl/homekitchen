package items

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/homekitchen/homekitchen/pkg/store_service/store_storage"

	"github.com/joho/godotenv"
)

func TestFindItems(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables([]string{ItemTable, store_storage.StoreTable})
	userUUID := uuid.New().String()
	store := &entities.Store{
		Name:        "Item",
		ImageSource: "https://",
		Description: handles.NewNullString("description"),
		UserUUID:    uuid.New().String(),
		PhoneNumber: "+77056278085",

		StoreDelivery: &entities.StoreDelivery{
			Type:     entities.COOKER_DELIVERY,
			UserUUID: userUUID,
		},
	}

	if err := store_storage.New(ctx).CreateStore(store); err != nil {
		t.Error(err)
	}

	item := &entities.Item{
		Name:        "Item 1",
		StoreUUID:   store.UUID,
		Description: "Test create item",
		Price:       123,
		UserUUID:    store.UserUUID,
		IsTurnOn:    true,
		CategoryID:  1,
	}
	itemsStorage := New(ctx)
	if err = itemsStorage.CreateItem(item); err != nil {
		t.Error(err)
		return
	}

	if err = itemsStorage.CreateItem(item); err != nil {
		t.Error(err)
		return
	}
	if err = itemsStorage.CreateItem(item); err != nil {
		t.Error(err)
		return
	}
	if err = itemsStorage.CreateItem(item); err != nil {
		t.Error(err)
		return
	}

	rows, err := itemsStorage.FindItems(&Filter{
		UserUUID: item.UserUUID,
	})

	if err != nil {
		t.Errorf("items: %v", err)

	}
	defer rows.Close()
	var items entities.Items

	for rows.Next() {
		item := &entities.Item{}
		if err = rows.Scan(
			&item.UUID,
			&item.Name,
			&item.Description,
			&item.ImageSource,
			&item.Price,
			&item.Cost,
			&item.IsTurnOn,
			&item.CategoryID,
			&item.StoreUUID,
			&item.UserUUID,
			&item.UpdatedAt,
		); err != nil {
			t.Errorf("items: %v", err)

		}
		items = append(items, item)
	}

	if rows.Err() != nil {
		t.Error(rows.Err())
		return
	}

	if len(items) == 0 {
		t.Error("Items is empty")
		return
	}

	db.NewODB(ctx).TruncateTables([]string{ItemTable})
}

func TestItemByUUIDSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	ctx = context.WithValue(ctx, "MC_KEY", "0949f368-a51a-47ae-bf21-655552b5a37b")

	db.NewODB(ctx).TruncateTables([]string{ItemTable, store_storage.StoreTable})

	item := &entities.Item{
		Name:        "Item 1",
		StoreUUID:   uuid.New().String(),
		Description: "Test create irem",
		Price:       123,
		UserUUID:    uuid.New().String(),
		IsTurnOn:    true,
		CategoryID:  1,
	}

	if err = New(ctx).CreateItem(item); err != nil {
		t.Error(err)
		return
	}

	row := New(ctx).GetItemByUUID(item.UUID)

	if row.Err() != nil {
		t.Error(row.Err())
	}

	item = &entities.Item{}
	err = row.Scan(
		&item.UUID,
		&item.UserUUID,
		&item.Name,
		&item.Description,
		&item.ImageSource,
		&item.Price,
		&item.Cost,
		&item.IsTurnOn,
		&item.CategoryID,
		&item.StoreUUID,
		&item.UpdatedAt,
	)
	if err == sql.ErrNoRows {
		t.Error("Item is not exits")
	} else if err != nil {
		t.Errorf("store: %v", err)

	}

	//db.NewODB(ctx).TruncateTables([]string{ItemTable})
}

func TestFindItemsWithoutCreating(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	_, err = New(ctx).FindItems(&Filter{
		UserUUID: "0949f368-a51a-47ae-bf21-655552b5a37b",
	})
	if err != nil {
		t.Error(err)
		return
	}
}

func TestGetItemsField(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	item := &entities.Item{
		Name:        "Item 1",
		StoreUUID:   "0c7931ca-d82a-400a-8521-248c4445d901",
		Description: "Test create item",
		Price:       123,
		Cost:        123,
		UserUUID:    uuid.New().String(),
		IsTurnOn:    true,
		CategoryID:  1,
	}

	if err = New(ctx).CreateItem(item); err != nil {
		t.Error(err)
		return
	}
	fmt.Println(item.UUID)

	rows, err := New(ctx).GetItemsByField("uuid", item.UUID)
	if err != nil {
		t.Error(err)
		return
	}
	defer rows.Close()
	var items entities.Items
	for rows.Next() {
		item := &entities.Item{}
		log.Println(item)
		if err = rows.Scan(
			&item.UUID,
			&item.Name,
			&item.Description,
			&item.ImageSource,
			&item.Price,
			&item.Cost,
			&item.CategoryID,
			&item.StoreUUID,
			&item.UpdatedAt); err != nil {
			return
		}
		items = append(items, item)
	}

	if len(items) == 0 {
		t.Error("Items is empty")
		return
	}

	// db.NewODB(ctx).TruncateTables([]string{ItemTable})
}
