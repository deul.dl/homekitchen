package items

import (
	"context"
	"database/sql"

	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

type Record interface {
	Update(field string, value interface{}) error
}

type itemRecord struct {
	Record
	entities.Item

	ctx      context.Context
	database *sql.DB
}

func NewItemRecord(uuid string, database *sql.DB, ctx context.Context) Record {
	return &itemRecord{
		Item:     entities.Item{UUID: uuid},
		ctx:      ctx,
		database: database,
	}
}

func (modal *itemRecord) Update(field string, value interface{}) error {
	return newUpdateFiledOfItem(modal, field, value).run()
}
