package items

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
)

const ItemTable = "items"

type Items []entities.Item

type Storage interface {
	GetItemByUUID(uuid string) *sql.Row
	CreateItem(*entities.Item) error
	UpdateItem(*entities.Item) error
	ByUUID(uuid string) Record
	FindItems(filter *Filter) (*sql.Rows, error)
	GetItemsByField(field string, value interface{}) (*sql.Rows, error)
}

type Filter struct {
	UserUUID string
}

type itemStorage struct {
	Storage

	ctx      context.Context
	database *sql.DB
}

func New(ctx context.Context) Storage {

	return &itemStorage{ctx: ctx, database: db.Pool}
}

func (storage *itemStorage) CreateItem(item *entities.Item) error {
	return newCreateItem(storage, item).run()
}

func (storage *itemStorage) UpdateItem(item *entities.Item) error {
	return newUpdateItem(storage, item).run()
}

func (storage *itemStorage) ByUUID(uuid string) Record {
	return NewItemRecord(uuid, storage.database, storage.ctx)
}

func (storage *itemStorage) FindItems(filter *Filter) (rows *sql.Rows, err error) {
	query := fmt.Sprintf(`
			SELECT
				"items"."uuid", "items"."name",
				"items"."description",
				"items"."image_source",
				"items"."price", "items"."cost",
				"items"."is_turn_on", "items"."category_id",
				"items"."store_uuid", "items"."user_uuid",
				"items"."updated_at"
			FROM %s
			INNER JOIN
				stories
			ON
				stories.user_uuid = items.user_uuid
			AND
				stories.uuid = items.store_uuid
			WHERE
				items.user_uuid = $1
		`,
		ItemTable)
	return storage.database.
		QueryContext(storage.ctx, query, filter.UserUUID)

}

func (storage *itemStorage) GetItemsByField(field string, value interface{}) (rows *sql.Rows, err error) {
	query := fmt.Sprintf(`
		SELECT items.uuid,
			items.name,
			items.description,
			items.image_source,
			items.price,
			items.cost,
			items.category_id,
			items.store_uuid,
			items.user_uuid,
			items.updated_at
		FROM %s
		INNER JOIN stories
		ON stories.uuid = items.store_uuid
		AND stories.is_turn_on = true
		WHERE items.%s = $1
		AND items.is_turn_on = true
		GROUP BY items.uuid
		`,
		ItemTable, field)
	log.Println(query)
	return storage.database.
		QueryContext(storage.ctx, query, value)
}

func (storage *itemStorage) GetItemByUUID(uuid string) *sql.Row {
	query := fmt.Sprintf(`
			SELECT
				"uuid", user_uuid, name, description,
				image_source, price, cost,
				is_turn_on, category_id,
				store_uuid, updated_at
			FROM
				%s
			WHERE
				user_uuid = $1
				AND
				"uuid" = $2
		`,
		ItemTable)

	userUUID := storage.ctx.Value("user_uuid").(string)

	return storage.database.
		QueryRowContext(storage.ctx, query, userUUID, uuid)
}
