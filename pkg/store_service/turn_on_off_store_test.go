package store_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestTurnOnStoreSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    uuid.New().String(),
		ImageSource: "./asd/aa.jpg",
	}

	CreateStoreSuccess(t, store)

	if err = New(ctx).TurnOn(store.UUID); err != nil {
		t.Error(err)
	}
}

func TestTurnOffStoreSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    uuid.New().String(),
		ImageSource: "./asd/aa.jpg",
		IsTurnOn:    true,
	}

	CreateStoreSuccess(t, store)

	if err = New(ctx).TurnOff(store.UUID); err != nil {
		t.Error(err)
	}
}
