package store_service

import (
	"context"
	"log"
	"testing"

	"github.com/joho/godotenv"
)

func TestGetCategoriesSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	categories, err := New(ctx).GetCategories()
	if err != nil {
		log.Println(err)
		return
	}
	t.Log(categories)
	t.Log(len(categories))

	if len(categories) <= 0 {
		t.Error("no categories")
		return
	}
}

func TestGetIsNotCategoriesSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	categories, err := New(ctx).GetIsNotCategories()
	if err != nil {
		log.Println(err)
		return
	}
	t.Log(categories)

	if len(categories) <= 0 {
		t.Error("no categories")
		return
	}
}
