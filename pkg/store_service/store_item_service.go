package store_service

import (
	"context"
	"fmt"

	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/homekitchen/homekitchen/pkg/store_service/store_storage"
)

type StoreItemService interface {
	GetStoriesWithItemsByCity(string) (entities.StoriesWithItems, error)
}

type storeItemService struct {
	StoreItemService

	ctx         context.Context
	itemService ItemService
	stories     store_storage.Storage
}

func NewStoreItemService(ctx context.Context) StoreItemService {
	return &storeItemService{
		ctx:         ctx,
		itemService: GetItemService(ctx),
		stories:     store_storage.New(ctx),
	}
}

func (service *storeItemService) GetStoriesWithItemsByCity(city string) (storiesWithItems entities.StoriesWithItems, err error) {
	rows, err := service.stories.FindStories(&store_storage.Filter{
		City: city,
	})

	defer rows.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	for rows.Next() {
		storeWithItems := &entities.StoreWithItems{}

		if err = rows.Scan(
			&storeWithItems.UUID,
			&storeWithItems.Name,
			&storeWithItems.Description,
			&storeWithItems.UserUUID,
			&storeWithItems.ImageSource,
			&storeWithItems.StartedAt,
			&storeWithItems.EndedAt); err != nil {
			return
		}

		items, err := service.itemService.
			GetItemsByField("store_uuid", storeWithItems.UUID)
		if err != nil {
			return nil, err
		}

		storeWithItems.Items = items

		storiesWithItems = append(storiesWithItems, storeWithItems)

	}
	return
}
