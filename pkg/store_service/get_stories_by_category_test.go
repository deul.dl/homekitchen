package store_service

import (
	"context"
	"testing"

	"github.com/joho/godotenv"
)

func TestGetStoriesByCategoryID(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	stories, err := New(ctx).FindStories(&StoreFilter{
		City:       "Mountain View",
		CategoryID: 1,
	})
	if err != nil {
		t.Error(err)
		return
	}

	if len(stories) == 0 {
		t.Error("Order Items doesn't have to be zero")
	}
}
