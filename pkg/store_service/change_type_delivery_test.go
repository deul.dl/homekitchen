package store_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestChangeTypeOfDelivery(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    uuid.New().String(),
		ImageSource: "./asd/aa.jpg",
		IsTurnOn:    true,
	}

	CreateStoreSuccess(t, store)

	if err = New(ctx).ChangeServiceOfDelivery(&entities.StoreDelivery{
		StoreUUID: store.UUID,
		UserUUID:  store.UserUUID,
		Type:      entities.HOME_KITCHEN_DELIVERY,
	}); err != nil {
		t.Error(err)
		return
	}

}
