package store_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestGetItemByUUID(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	userUUID := "d9739d52-2fcd-452b-a004-bcbfd0dbb41d"

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx = context.WithValue(ctx, "user_uuid", userUUID)

	store := &entities.Store{
		UUID: "0c7931ca-d82a-400a-8521-248c4445d901",
	}

	item := &entities.Item{
		Name:        "Store 1",
		StoreUUID:   store.UUID,
		Description: "Test create store",
		Price:       123,
		Cost:        123,
		UserUUID:    userUUID,
		IsTurnOn:    true,
		ImageSource: handles.NewNullString("./asd/aa.jpg"),
		CategoryID:  1,
	}

	if err = GetItemService(ctx).AddItem(item); err != nil {
		t.Error(err)
	}

	item, err = GetItemService(ctx).GetItemByUUID(item.UUID)
	if err != nil {
		t.Error(err)
	}

	if item == nil {
		t.Error("item is empty")
	}
}
