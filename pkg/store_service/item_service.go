package store_service

import (
	"context"
	"database/sql"
	"errors"
	"log"

	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/homekitchen/homekitchen/pkg/store_service/items"
)

type ItemService interface {
	AddItem(item *entities.Item) error
	UpdateItem(item *entities.Item) error
	UpdateItemImage(uuid, pathfile string) error
	TurnOn(uuid string) error
	TurnOff(uuid string) error

	GetItemsByField(field string, value interface{}) (entities.Items, error)
	GetItemByUUID(uuid string) (*entities.Item, error)
	FindItems(filter *items.Filter) (entities.Items, error)
}

type itemService struct {
	ItemService

	ctx   context.Context
	items items.Storage
}

func GetItemService(ctx context.Context) ItemService {
	return &itemService{
		ctx:   ctx,
		items: items.New(ctx),
	}
}

func (service *itemService) AddItem(item *entities.Item) error {
	if err := service.Verify(item); err != nil {
		return err
	}

	return service.items.CreateItem(item)
}

func (service *itemService) UpdateItem(item *entities.Item) error {
	if err := service.Verify(item); err != nil {
		return err
	}

	return service.items.UpdateItem(item)
}

func (service *itemService) Verify(item *entities.Item) error {
	if len(item.Name) > 255 {
		return errors.New("Name is very long.")
	}
	if len(item.Name) > 1000 {
		return errors.New("Description is very long.")
	}
	return nil
}

func (service *itemService) TurnOn(uuid string) error {
	return service.items.ByUUID(uuid).Update("is_turn_on", true)
}

func (service *itemService) TurnOff(uuid string) error {
	return service.items.ByUUID(uuid).Update("is_turn_on", false)
}

func (service *itemService) UpdateItemImage(uuid, pathfile string) error {
	return service.items.ByUUID(uuid).Update("image_source", pathfile)
}

func (service *itemService) GetItemsByField(field string, value interface{}) (items entities.Items, err error) {
	rows, err := service.items.GetItemsByField(field, value)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		item := &entities.Item{}

		if err = rows.Scan(
			&item.UUID,
			&item.Name,
			&item.Description,
			&item.ImageSource,
			&item.Price,
			&item.Cost,
			&item.CategoryID,
			&item.StoreUUID,
			&item.UserUUID,
			&item.UpdatedAt); err != nil {
			return
		}
		items = append(items, item)
	}
	if rows.Err() != nil {
		return items, rows.Err()
	}
	return
}

func (service *itemService) GetItemByUUID(uuid string) (*entities.Item, error) {
	row := service.items.GetItemByUUID(uuid)
	if row.Err() != nil {
		return nil, row.Err()
	}

	item := &entities.Item{}
	err := row.Scan(
		&item.UUID,
		&item.UserUUID,
		&item.Name,
		&item.Description,
		&item.ImageSource,
		&item.Price,
		&item.Cost,
		&item.IsTurnOn,
		&item.CategoryID,
		&item.StoreUUID,
		&item.UpdatedAt,
	)
	if err == sql.ErrNoRows {
		return nil, errors.New("Item is not exits")
	} else if err != nil {
		log.Printf("item: %v", err)
		return nil, err
	}
	return item, nil
}

func (service *itemService) FindItems(filter *items.Filter) (itemss entities.Items, err error) {
	rows, err := service.items.FindItems(filter)
	if err != nil {
		log.Printf("items: %v", err)
		return nil, rows.Err()
	}
	defer rows.Close()
	for rows.Next() {
		item := &entities.Item{}
		if err = rows.Scan(
			&item.UUID,
			&item.Name,
			&item.Description,
			&item.ImageSource,
			&item.Price,
			&item.Cost,
			&item.IsTurnOn,
			&item.CategoryID,
			&item.StoreUUID,
			&item.UserUUID,
			&item.UpdatedAt,
		); err != nil {
			log.Printf("items: %v", err)
			return
		}
		itemss = append(itemss, item)
	}
	if rows.Err() != nil {
		log.Printf("items: %v", rows.Err())
		return nil, rows.Err()
	}
	return
}
