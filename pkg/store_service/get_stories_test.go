package store_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func TestGetStoriesWithItems(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	ctx = context.WithValue(ctx, entities.USER_UUID, "b5771632-9578-4f39-b6fc-66ad2efc4a3d")

	stories, err := NewStoreItemService(ctx).GetStoriesWithItemsByCity("Mountain View")
	if err != nil {
		t.Error(err)
		return
	}

	if len(stories) != 0 {
		t.Error("Order Items doesn't have to be zero")
	}
}

func TestGetStoreByUUID(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    uuid.New().String(),
		ImageSource: "./asd/aa.jpg",
	}

	CreateStoreSuccess(t, store)

	store, err = New(ctx).GetStoreByField("uuid", store.UUID)
	if err != nil {
		t.Error(err)
		return
	}
}

func TestGetStoreByUserUUID(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	newStore := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    uuid.New().String(),
		ImageSource: "./asd/aa.jpg",
	}

	CreateStoreSuccess(t, newStore)

	store, err := New(ctx).GetStoreByUserUUID(newStore.UserUUID)
	if err != nil {
		t.Error(err)
		return
	}

	if store == nil {
		t.Error("Store is empty")
	}
}
