package store_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/joho/godotenv"
)

func CreateStoreSuccess(t *testing.T, store *entities.Store) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	if err = New(ctx).CreateStore(store); err != nil {
		t.Error(err)
	}

}

func TestCreateStoreSuccess(t *testing.T) {
	userUUID := "1e0225f1-fb11-4839-b378-7daf28351b1c"
	store := &entities.Store{
		Name:        "Store 1",
		Description: handles.NewNullString("Test create store"),
		PhoneNumber: "+770562780850",
		UserUUID:    userUUID,
		ImageSource: "./asd/aa.jpg",
	}

	CreateStoreSuccess(t, store)
}
