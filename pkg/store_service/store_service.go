package store_service

import (
	"context"
	"database/sql"
	"errors"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/delivery_service"
	categoryStorage "github.com/homekitchen/homekitchen/pkg/store_service/category_storage"
	"github.com/homekitchen/homekitchen/pkg/store_service/entities"
	"github.com/homekitchen/homekitchen/pkg/store_service/store_storage"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type StoreFilter struct {
	CategoryID int64
	City       string
}

type StoreService interface {
	CreateStore(store *entities.Store) error
	UpdateStore(store *entities.Store) error
	UpdateStoreImage(uuid, pathfile string) error
	TurnOn(uuid string) error
	TurnOff(uuid string) error
	GetCategories() (entities.Categories, error)
	GetStoreByField(field string, value interface{}) (*entities.Store, error)
	FindStories(*StoreFilter) (entities.Stories, error)
	UpdatePriceOfDelivery(string, float64) error
	ChangeServiceOfDelivery(*entities.StoreDelivery) error
	GetStoreDeliveryByStoreUUID(string) (*entities.StoreDelivery, error)
	GetStoreByUserUUID(string) (store *entities.Store, err error)
	GetIsNotCategories() (categories entities.Categories, err error)
}

type storeService struct {
	StoreService

	ctx        context.Context
	stories    store_storage.Storage
	categories categoryStorage.Storage
}

func New(ctx context.Context) StoreService {
	return &storeService{
		ctx:        ctx,
		stories:    store_storage.New(ctx),
		categories: categoryStorage.New(ctx),
	}
}

func (service *storeService) CreateStore(store *entities.Store) error {
	if err := service.Verify(store); err != nil {
		return err
	}

	store.StoreDelivery = &entities.StoreDelivery{
		Price:    handles.NewNullFloat64(0),
		Type:     entities.COOKER_DELIVERY,
		UserUUID: store.UserUUID,
	}
	return service.stories.CreateStore(store)
}

func (service *storeService) UpdateStore(store *entities.Store) error {
	if err := service.Verify(store); err != nil {
		return err
	}

	return service.stories.UpdateStore(store)
}

func (service *storeService) Verify(store *entities.Store) error {
	if len(store.Name) >= 255 {
		return errors.New("Name is very long")
	}
	if len(store.Description.String) > 1000 {
		return errors.New("Description is very long.")
	}
	return handles.VerifyMobile(store.PhoneNumber)
}

func (service *storeService) TurnOn(uuid string) error {
	return service.stories.ByUUID(uuid).Update("is_turn_on", true)
}

func (service *storeService) TurnOff(uuid string) error {
	return service.stories.ByUUID(uuid).Update("is_turn_on", false)
}

func (service *storeService) UpdateStoreImage(uuid, pathfile string) error {
	return service.stories.ByUUID(uuid).Update("image_source", pathfile)
}

func (service *storeService) GetCategories() (categories entities.Categories, err error) {
	rows, err := service.categories.GetCategories()
	if rows.Err() != nil || err != nil {
		if rows.Err() != nil {
			return categories, rows.Err()
		}
		log.Println(err)

		return
	}

	for rows.Next() {
		category := &entities.Category{}
		if err = rows.Scan(
			&category.ID,
			&category.Name,
			&category.Position,
		); err != nil {
			log.Printf("categories: %v", err)
			return nil, err
		}
		categories = append(categories, category)
	}

	return
}

func (service *storeService) GetIsNotCategories() (categories entities.Categories, err error) {
	rows, err := service.categories.GetIsNotCategories()
	if rows.Err() != nil || err != nil {
		if rows.Err() != nil {
			return categories, rows.Err()
		}
		log.Println(err)

		return
	}

	for rows.Next() {
		category := &entities.Category{}
		if err = rows.Scan(&category.ID, &category.Name, &category.Position); err != nil {
			log.Println(err)
			return
		}
		categories = append(categories, category)
	}
	return
}

func (service *storeService) FindStories(filter *StoreFilter) (stories entities.Stories, err error) {
	rows, err := service.stories.FindStories(&store_storage.Filter{
		CategoryID: filter.CategoryID,
		City:       filter.City,
	})
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		store := &entities.Store{}

		if err = rows.Scan(
			&store.UUID,
			&store.Name,
			&store.Description,
			&store.UserUUID,
			&store.ImageSource,
			&store.StartedAt,
			&store.EndedAt,
		); err != nil {
			return
		}
		stories = append(stories, store)
	}

	if rows.Err() != nil {
		return stories, rows.Err()
	}
	return
}

func (service *storeService) GetStoreByField(field string, value interface{}) (store *entities.Store, err error) {
	row := service.stories.GetStoreByField(field, value)
	if row.Err() != nil {
		return
	}

	store = &entities.Store{}
	err = row.Scan(
		&store.UUID,
		&store.Name,
		&store.Description,
		&store.UserUUID,
		&store.ImageSource,
		&store.StartedAt,
		&store.EndedAt,
	)
	if err == sql.ErrNoRows {
		return store, errors.New("Store is not exits")
	} else if err != nil {
		log.Printf("store: %v", err)
		return nil, err
	}
	return
}

func (service *storeService) UpdatePriceOfDelivery(storeUUID string, price float64) error {
	return service.stories.UpdateStoreDelivery(&entities.StoreDelivery{
		StoreUUID: storeUUID,
		Price:     handles.NewNullFloat64(price),
		Type:      entities.COOKER_DELIVERY,
	})
}

func (service *storeService) ChangeServiceOfDelivery(storeDelivery *entities.StoreDelivery) error {

	if storeDelivery.Type == entities.COOKER_DELIVERY {
		return service.stories.UpdateStoreDelivery(storeDelivery)
	}
	executorAddress, err := users.New(service.ctx).
		GetExecutors().
		GetExecutorAddressByExecutorUUID(storeDelivery.UserUUID)
	if err != nil {
		return err
	}

	deliveryPrice, err := delivery_service.New(service.ctx).
		GetDeliveryPriceByCity(executorAddress.City.String)
	if err != nil {
		return err
	}

	storeDelivery.Price = handles.NewNullFloat64(0)
	storeDelivery.DeliveryPriceUUID = handles.NewNullString(deliveryPrice.UUID)

	return service.stories.UpdateStoreDelivery(storeDelivery)
}

func (service *storeService) GetStoreDeliveryByStoreUUID(uuid string) (storeDelivery *entities.StoreDelivery, err error) {
	storeDelivery = &entities.StoreDelivery{}

	row := service.stories.GetStoreDeliveryByStoreUUID(uuid)
	if row.Err() != nil {
		return nil, row.Err()
	}

	if err = row.Scan(
		&storeDelivery.UUID,
		&storeDelivery.Price,
		&storeDelivery.UserUUID,
	); err != nil {
		return
	}
	return
}

func (service *storeService) GetStoreByUserUUID(uuid string) (store *entities.Store, err error) {
	row := service.stories.GetStoreByUserUUID(uuid)
	if row.Err() != nil {
		return
	}

	store = &entities.Store{}
	err = row.Scan(
		&store.UUID,
		&store.Name,
		&store.Description,
		&store.UserUUID,
		&store.PhoneNumber,
		&store.ImageSource,
		&store.IsTurnOn,
		&store.StartedAt,
		&store.EndedAt,
	)
	if err == sql.ErrNoRows {
		return nil, errors.New("Store is not exits")
	}

	return
}
