package addresses

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

type setPrimary struct {
	*addressStorage

	uuid string
}

func newSetPrimary(addressStorage *addressStorage, uuid string) *setPrimary {
	return &setPrimary{addressStorage: addressStorage, uuid: uuid}
}

func (action *setPrimary) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("addresses: %v\n", err)
		return err
	}

	if err := action.save(tx); err != nil {
		log.Printf("addresses: %v\ns", err)
		return err
	}

	if err := action.unsetPrimaryAddresses(tx, action.uuid); err != nil {
		log.Printf("addresses: %v\n", err)
		return err
	}

	if err = tx.Commit(); err != nil {
		log.Printf("addresses: %v\n", err)
	}
	return nil
}

func (action *setPrimary) save(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`UPDATE %s SET is_primary =$1, updated_at =$2 WHERE "uuid" = $3`,
		AddressTable,
	)

	update, err := action.database.Prepare(query)
	defer update.Close()
	if err != nil {
		log.Printf("addresses: unable to rollback: %v\n", err)
		return nil
	}

	if _, err := tx.StmtContext(action.ctx, update).Exec(
		true,
		time.Now(),
		action.uuid,
	); err != nil {
		log.Printf("addresses: add executor failed: %v\n", err)
		tx.Rollback()
		return err
	}
	return nil
}
