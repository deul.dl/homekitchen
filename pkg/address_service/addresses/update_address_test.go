package addresses

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/joho/godotenv"
)

func TestUpdateAddressSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()
	addreess := &entities.Address{
		CustomerUUID: uuidWithHyphen,
		Street:       "Ponamareva",
		House:        "15",
		Apartment:    "32",
		Porch:        "2",
		City:         "Topar",
		Country:      "Kazachstan",
		Primary:      true,
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables(
		[]string{
			AddressTable,
		})

	if err := New(ctx).CreateAddress(addreess); err != nil {
		t.Error(err)
	}

	addreess_1 := &entities.Address{
		UUID:         addreess.UUID,
		CustomerUUID: addreess.UUID,
		Street:       "Ponamarevassss",
		House:        "15",
		Apartment:    "32",
		Porch:        "2",
		City:         "Toparssss",
		Country:      "Kazachstan",
		Primary:      false,
	}

	if err := New(ctx).UpdateAddress(addreess_1); err != nil {
		t.Error(err)
	}

	if addreess.Primary == addreess_1.Primary {
		t.Error("Error: primary of address doesn't have to be similar")
	}
	db.NewODB(ctx).TruncateTables(
		[]string{
			AddressTable,
		})
}
