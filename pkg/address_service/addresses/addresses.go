package addresses

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
	"github.com/homekitchen/homekitchen/pkg/db"
)

const AddressTable = "addresses"

type Storage interface {
	CreateAddress(address *entities.Address) error
	UpdateAddress(address *entities.Address) error
	SetPrimary(uuid string) error
	RemoveAddress(uuid string) error
	GetAddressesByCustomerUUID(customerUUID string) (*sql.Rows, error)
	GetAddressByUUID(uuid string) *sql.Row
}

type addressStorage struct {
	Storage

	ctx      context.Context
	database *sql.DB
}

func New(ctx context.Context) Storage {

	return &addressStorage{ctx: ctx, database: db.Pool}
}

func (storage *addressStorage) CreateAddress(address *entities.Address) error {
	return newCreateAddress(storage, address).run()
}

func (storage *addressStorage) UpdateAddress(address *entities.Address) error {
	return newUpdateAddress(storage, address).run()
}

func (storage *addressStorage) SetPrimary(uuid string) error {
	return newSetPrimary(storage, uuid).run()
}

func (storage *addressStorage) RemoveAddress(uuid string) (err error) {
	query := fmt.Sprintf(
		`UPDATE %s SET deleted_at = $1 WHERE "uuid" = $2`,
		AddressTable,
	)

	updated, err := storage.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("addresses: unable to rollback: %v\n", err)
		return
	}

	if _, err = updated.Exec(time.Now(), uuid); err != nil {
		log.Printf("addresses: update executor failed: %v\n", err)
		return err
	}
	return
}

func (storage *addressStorage) GetAddressByUUID(uuid string) *sql.Row {
	query := fmt.Sprintf(`
			SELECT
				uuid, customer_uuid,
				street, house,
				apartment, porch,
				city, country,
				is_primary
			FROM %s
			WHERE "uuid" = $1
			AND deleted_at IS NULL
		`,
		AddressTable)
	return storage.database.
		QueryRowContext(storage.ctx, query, uuid)
}

func (storage *addressStorage) GetAddressesByCustomerUUID(customerUUID string) (*sql.Rows, error) {

	query := fmt.Sprintf(`
			SELECT
				uuid,
				customer_uuid,
				street,
				house,
				apartment,
				porch,
				city,
				country,
				is_primary
			FROM %s
			WHERE customer_uuid = $1
			AND deleted_at IS NULL
		`,
		AddressTable)
	return storage.database.
		QueryContext(storage.ctx, query, customerUUID)
}

func (storage *addressStorage) unsetPrimaryAddresses(tx *sql.Tx, uuid string) (err error) {
	query := fmt.Sprintf(
		`UPDATE %s SET is_primary = $1, updated_at =$2 WHERE "uuid" != $3`,
		AddressTable,
	)

	updated, err := storage.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("addresses: primary unable to rollback: %v\n", err)
		tx.Rollback()
		return
	}

	if _, err := tx.StmtContext(storage.ctx, updated).Exec(
		false, time.Now(), uuid,
	); err != nil {
		log.Printf("addresses: update executor failed: %v\n", err)
		tx.Rollback()
		return err
	}
	return
}
