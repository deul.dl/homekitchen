package addresses

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
)

type updateAddress struct {
	*addressStorage

	address *entities.Address
}

func newUpdateAddress(storage *addressStorage, address *entities.Address) *updateAddress {
	return &updateAddress{addressStorage: storage, address: address}
}

func (action *updateAddress) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("addresses: %v\n", err)
		return err
	}

	if err := action.save(tx); err != nil {
		log.Printf("addresses: %v\n", err)
		return err
	}

	if action.address.Primary {
		if err = action.afterUpdate(tx); err != nil {
			return err
		}
	}

	if err = tx.Commit(); err != nil {
		log.Printf("addresses: %v\n", err)
	}
	return nil
}

func (action *updateAddress) save(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			UPDATE
				%s
			SET street =$1,
			house =$2,
			apartment =$3,
			porch =$4,
			is_primary =$5,
			updated_at =$6
			WHERE "uuid" = $7
		`,
		AddressTable,
	)

	updated, err := action.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("addresses: unable to rollback: %v\n", err)
		return nil
	}

	if _, err := tx.StmtContext(action.ctx, updated).Exec(
		handles.NewNullString(action.address.Street),
		handles.NewNullString(action.address.House),
		action.address.Apartment,
		action.address.Porch,
		handles.NewNullBool(action.address.Primary),
		time.Now(),
		action.address.UUID,
	); err != nil {
		log.Printf("addresses: update executor failed: %v\n", err)
		tx.Rollback()
		return err
	}
	return nil
}

func (action *updateAddress) afterUpdate(tx *sql.Tx) error {
	if err := action.unsetPrimaryAddresses(tx, action.address.UUID); err != nil {
		log.Printf("addresses: %v\n", err)
		return err
	}
	return nil
}
