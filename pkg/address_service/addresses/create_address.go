package addresses

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
)

type createAddress struct {
	*addressStorage

	address *entities.Address
}

func newCreateAddress(storage *addressStorage, address *entities.Address) *createAddress {
	return &createAddress{addressStorage: storage, address: address}
}

func (action *createAddress) run() error {
	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("addresses: %v\n", err)
		return err
	}

	if err := action.save(tx); err != nil {
		log.Printf("addresses: %v\n", err)
		return err
	}

	if action.address.Primary {
		if err = action.afterCreate(tx); err != nil {
			return err
		}
	}

	if err = tx.Commit(); err != nil {
		log.Printf("addresses: %v\n", err)
	}
	return nil
}

func (action *createAddress) save(tx *sql.Tx) error {
	query := fmt.Sprintf(
		`
			INSERT INTO %s (customer_uuid, street, house, apartment, porch, city, country, is_primary)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING "uuid";
		`,
		AddressTable,
	)

	inserted, err := action.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("addresses: unable to rollback: %v\n", err)
		return err
	}

	if err := tx.StmtContext(action.ctx, inserted).
		QueryRow(
			handles.NewNullString(action.address.CustomerUUID),
			handles.NewNullString(action.address.Street),
			handles.NewNullString(action.address.House),
			action.address.Apartment,
			action.address.Porch,
			handles.NewNullString(action.address.City),
			action.address.Country,
			handles.NewNullBool(action.address.Primary),
		).Scan(&action.address.UUID); err != nil {
		log.Printf("addresses: add executor failed: %v\n", err)
		tx.Rollback()
		return err
	}
	return nil
}

func (action *createAddress) afterCreate(tx *sql.Tx) error {
	if err := action.unsetPrimaryAddresses(tx, action.address.UUID); err != nil {
		log.Printf("addresses: %v", err)
		return err
	}
	return nil
}
