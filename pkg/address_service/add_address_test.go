package address_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
	"github.com/joho/godotenv"
)

func TestCreateAddressSuccess(t *testing.T) {

	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := "d9739d52-2fcd-452b-a004-bcbfd0dbb41d"

	address := &entities.Address{
		CustomerUUID: uuidWithHyphen,
		Street:       "Ponamareva",
		House:        "15",
		Apartment:    "32",
		Porch:        "2",
		City:         "Topar",
		Country:      "Kazachstan",
		Primary:      true,
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = New(ctx).AddAddress(address)

	if err != nil {
		t.Errorf("err %v", err)
	}
}
