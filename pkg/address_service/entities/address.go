package entities

import (
	"time"
)

type Address struct {
	UUID         string
	CustomerUUID string
	Street       string
	House        string
	Apartment    string
	Porch        string
	City         string
	Country      string
	Primary      bool
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    time.Time
}
type Addresses []*Address
