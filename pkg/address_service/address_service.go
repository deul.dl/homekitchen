package address_service

import (
	"context"

	"github.com/homekitchen/homekitchen/pkg/address_service/addresses"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
)

type AddreesService interface {
	AddAddress(address *entities.Address) error
	UpdateAddress(address *entities.Address) error
	SetPrimary(uuid string) error
	RemoveAddress(uuid string) error
	GetAddresses(uuid string) (entities.Addresses, error)
	GetAddressByUUID(uuid string) (*entities.Address, error)
}

type addressService struct {
	AddreesService

	ctx       context.Context
	addresses addresses.Storage
}

func New(ctx context.Context) AddreesService {
	return &addressService{
		ctx:       ctx,
		addresses: addresses.New(ctx),
	}
}

func (service *addressService) AddAddress(address *entities.Address) error {
	return service.addresses.CreateAddress(address)
}

func (service *addressService) UpdateAddress(address *entities.Address) error {
	return service.addresses.UpdateAddress(address)
}

func (service *addressService) SetPrimary(uuid string) error {
	return service.addresses.SetPrimary(uuid)
}

func (service *addressService) RemoveAddress(uuid string) error {
	return service.addresses.RemoveAddress(uuid)
}

func (service *addressService) GetAddresses(uuid string) (addresses entities.Addresses, err error) {
	rows, err := service.addresses.GetAddressesByCustomerUUID(uuid)
	if err != nil {
		return
	}
	defer rows.Close()

	var address *entities.Address
	for rows.Next() {
		address = &entities.Address{}
		if err = rows.Scan(
			&address.UUID,
			&address.CustomerUUID,
			&address.Street,
			&address.House,
			&address.Apartment,
			&address.Porch,
			&address.City,
			&address.Country,
			&address.Primary,
		); err != nil {
			return nil, err
		}
		addresses = append(addresses, address)
	}

	if rows.Err() != nil {
		return addresses, rows.Err()
	}

	return addresses, nil
}

func (service *addressService) GetAddressByUUID(uuid string) (address *entities.Address, err error) {
	row := service.addresses.GetAddressByUUID(uuid)
	if row.Err() != nil {
		return nil, row.Err()
	}

	address = &entities.Address{}
	if err := row.Scan(
		&address.UUID, &address.CustomerUUID,
		&address.Street, &address.House,
		&address.Apartment, &address.Porch,
		&address.City, &address.Country,
		&address.Primary,
	); err != nil {
		return nil, err
	}

	return
}
