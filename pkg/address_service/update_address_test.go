package address_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateAddressSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	address := &entities.Address{
		CustomerUUID: uuidWithHyphen,
		Street:       "Ponamareva",
		House:        "15",
		Apartment:    "32",
		Porch:        "2",
		City:         "Topar",
		Country:      "Kazachstan",
		Primary:      true,
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = New(ctx).AddAddress(address)
	if err != nil {
		t.Errorf("err %v", err)
	}

	err = New(ctx).UpdateAddress(address)
	if err != nil {
		t.Errorf("err %v", err)
	}
}
