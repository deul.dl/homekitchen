package address_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/pkg/address_service/entities"
	"github.com/joho/godotenv"
)

func TestGetAddressesSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	address := &entities.Address{
		CustomerUUID: uuidWithHyphen,
		Street:       "Ponamareva",
		House:        "15",
		Apartment:    "32",
		Porch:        "2",
		City:         "Topar",
		Country:      "Kazachstan",
		Primary:      true,
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = New(ctx).AddAddress(address)
	if err != nil {
		t.Errorf("err %v", err)
	}

	err = New(ctx).AddAddress(address)
	if err != nil {
		t.Errorf("err %v", err)
	}

	addresses, err := New(ctx).GetAddresses(address.CustomerUUID)
	if err != nil {
		t.Errorf("err %v", err)
	}

	if len(addresses) == 0 {
		t.Error("addresses = 0!")
	}
}

func TestGetAddressSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}
	uuidWithHyphen := uuid.New().String()

	address := &entities.Address{
		CustomerUUID: uuidWithHyphen,
		Street:       "Ponamareva",
		House:        "15",
		Apartment:    "32",
		Porch:        "2",
		City:         "Topar",
		Country:      "Kazachstan",
		Primary:      true,
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = New(ctx).AddAddress(address)
	if err != nil {
		t.Errorf("err %v", err)
	}

	_, err = New(ctx).GetAddressByUUID(address.UUID)
	if err != nil {
		t.Errorf("err %v", err)
	}
}
