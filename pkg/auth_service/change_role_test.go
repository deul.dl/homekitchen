package auth_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/joho/godotenv"
)

func TestChangeCustomerSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	authSuccess, err := New(ctx).Login(&LoginInput{
		PhoneNumber: "+77056278085",
		Key:         "123456a",
	})
	if err != nil {
		t.Error(err)
		return
	}

	authSuccess, err = New(ctx).ChangeRole(authSuccess.Token, entities.EXECUTOR)
	if err != nil {
		t.Error(err)
		return
	}

	if authSuccess == nil {
		t.Error("Error")
		return
	}
}

func TestChangeExecutorSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	authSuccess, err := New(ctx).Login(&LoginInput{
		PhoneNumber: "+77056278085",
		Key:         "123456a",
	})
	if err != nil {
		t.Error(err)
		return
	}

	authSuccess, err = New(ctx).ChangeRole(authSuccess.Token, entities.EXECUTOR)
	if err != nil {
		t.Error(err)
		return
	}

	if authSuccess == nil {
		t.Error("Error")
		return
	}
}
