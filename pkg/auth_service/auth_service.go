package auth_service

import (
	"context"
	"errors"
	"log"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

const (
	SMS_CODE_TYPE = "sms_code"
	PASSWORD_TYPE = "password"
)

type Auth interface {
	SendCode(phoneNumber string) (string, error)
	SendCodeByUUID(uuid string) (string, error)
	SendCodeForSignup(phoneNumber string) (string, error)
	VerificateCode(phoneNumber string, code string) error
	SignUp(signupInsert *SignUpInput) (*AuthSuccess, error)
	Login(login *LoginInput) (*AuthSuccess, error)
	ChangeRole(token string, role string) (*AuthSuccess, error)
	GetClients() users.Clients
}

type AuthSuccess struct {
	Token string `json:"token"`
}

type authService struct {
	Auth

	ctx     context.Context
	clients users.Clients
}

func New(ctx context.Context) Auth {
	return &authService{ctx: ctx, clients: users.NewClients(ctx)}
}

func (auth *authService) validPhoneNumber(phoneNumber string) error {
	if phoneNumber == "" {
		return errors.New("Phone number is not empty")
	}

	return handles.VerifyMobile(phoneNumber)
}

func (auth *authService) validCode(code string) error {
	if code == "" {
		return errors.New("Code is not empty")
	}
	if len(code) < 6 {
		return errors.New("Code is very short")
	}

	if len(code) > 6 {
		return errors.New("Code is very long")
	}

	return nil
}

func (auth *authService) validPassword(password string) error {
	if len(password) < 6 {
		return errors.New("Password is very short")
	}

	return nil
}

func (auth *authService) SendCode(phoneNumber string) (string, error) {
	return newSendCode(auth, phoneNumber, "phone_number").run()
}

func (auth *authService) SendCodeByUUID(uuid string) (string, error) {
	return newSendCode(auth, uuid, "uuid").run()
}

func (auth *authService) SendCodeForSignup(phoneNumber string) (string, error) {
	return newSendCodeForSignup(auth, phoneNumber).run()
}

func (auth *authService) VerificateCode(phoneNumber string, code string) (err error) {
	if err = auth.validPhoneNumber(phoneNumber); err != nil {
		return
	}

	client, err := auth.clients.GetClientByField("phone_number", phoneNumber)
	if err != nil {
		return err
	}

	if code != client.Code.String {
		return errors.New("Code is not match")
	}

	return nil
}

func (auth *authService) SignUp(signupInput *SignUpInput) (*AuthSuccess, error) {
	return newSignup(auth, signupInput).run()
}

func (auth *authService) Login(loginInput *LoginInput) (*AuthSuccess, error) {
	if loginInput.Type == SMS_CODE_TYPE {
		return auth.loginWithVeritificateCode(loginInput)
	}
	return newLogin(auth, loginInput).run()
}

func (auth *authService) loginWithVeritificateCode(loginInput *LoginInput) (success *AuthSuccess, err error) {
	if err = auth.VerificateCode(
		loginInput.PhoneNumber,
		loginInput.Key); err != nil {
		return
	}

	client, err := auth.clients.GetClientByField("phone_number", loginInput.PhoneNumber)
	if err != nil {
		return nil, err
	}

	token, err := ojwt.CreateToken(&ojwt.TokenInput{
		UUID: client.UUID.String,
		Role: entities.CUSTOMER,
	})
	if err != nil {
		log.Println("auth: problem is token")
		return
	}

	success = &AuthSuccess{
		Token: token,
	}
	return
}

func (auth *authService) ChangeRole(inToken string, role string) (*AuthSuccess, error) {
	_, mc, err := ojwt.FromToken(inToken)
	if err != nil {
		return nil, err
	}

	inToken, err = ojwt.CreateToken(&ojwt.TokenInput{
		UUID: mc["uuid"].(string),
		Role: role,
	})

	if err != nil {
		return nil, err
	}
	return &AuthSuccess{
		Token: inToken,
	}, nil
}

func (auth *authService) GetClients() users.Clients {
	return auth.clients
}
