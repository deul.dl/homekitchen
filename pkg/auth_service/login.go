package auth_service

import (
	"errors"

	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/auth_service/password"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type LoginInput struct {
	PhoneNumber string `json:"phoneNumber"`
	Key         string `json:"key"`
	Type        string `json:"type"`
	FmcToken    string `json:"fcmToken"`
}

type loginWithPassword struct {
	auth *authService
	data *LoginInput
}

func newLogin(auth *authService, data *LoginInput) *loginWithPassword {
	return &loginWithPassword{auth, data}
}

func (loginWithPassword *loginWithPassword) run() (*AuthSuccess, error) {

	users := users.New(loginWithPassword.auth.ctx)
	user, err := users.GetUserByPhoneNumber(loginWithPassword.data.PhoneNumber)
	if err != nil {
		return nil, err
	}

	if !password.CheckPasswordHash(loginWithPassword.data.Key, user.PasswordHash.String) {
		return nil, errors.New("Password is wrong")
	}

	role, err := users.GetRoleForUser(user)
	if err != nil {
		return nil, err
	}

	token, err := ojwt.CreateToken(&ojwt.TokenInput{
		UUID: user.UUID,
		Role: role.Name,
	})

	if err != nil {
		return nil, err
	}

	return &AuthSuccess{
		Token: token,
	}, nil

}
