package auth_service

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/joho/godotenv"
)

func TestSendCodeSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	phoneNumber := "+77056278085"
	_, err = New(ctx).SendCode(phoneNumber)
	if err != nil {

		fmt.Println(len(phoneNumber))
		t.Error(err)
		return
	}

	client, err := New(ctx).
		GetClients().
		GetClientByField("phone_number", phoneNumber)
	if err != nil {
		t.Error(err)
		return
	}
	if client.Code.String == "" {
		t.Error(err)
		return
	}
}

func TestSendCodeByUUIDSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	uuid := uuid.New().String()
	_, err = New(ctx).SendCodeByUUID(uuid)
	if err != nil {
		fmt.Println(uuid)
		t.Error(err)
		return
	}

	client, err := New(ctx).
		GetClients().
		GetClientByField("uuid", uuid)
	if err != nil {
		t.Error(err)
		return
	}

	if client.Code.String == "" {
		t.Error(err)
		return
	}
}

func TestSendCodeForSignupSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	uuid := uuid.New().String()
	_, err = New(ctx).SendCodeForSignup("+77056278085")
	if err != nil {
		fmt.Println(uuid)
		t.Error(err)
		return
	}

	client, err := New(ctx).
		GetClients().
		GetClientByField("uuid", uuid)
	if err != nil {
		t.Error(err)
		return
	}

	if client.Code.String == "" {
		t.Error(err)
		return
	}
}
