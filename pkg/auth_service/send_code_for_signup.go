package auth_service

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/internal/notification"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type sendCodeForSignup struct {
	sendCode

	auth        *authService
	phoneNumber string
}

func newSendCodeForSignup(auth *authService, phoneNumber string) sendCode {
	return &sendCodeForSignup{auth: auth, phoneNumber: phoneNumber}
}

func (data *sendCodeForSignup) run() (string, error) {
	var client *entities.Client
	if err := data.auth.validPhoneNumber(data.phoneNumber); err != nil {
		return "", err
	}

	client, _ = data.auth.clients.GetClientByField("phone_number", data.phoneNumber)

	rand.Seed(time.Now().Unix())
	randomCode := rand.Int63n(999999-100000) + 100000

	if client != nil {
		if _, err := users.New(data.auth.ctx).GetUserByPhoneNumber(client.PhoneNumber.String); err != nil {
			return "", err
		}

		client.Code = handles.NewNullString(randomCode)
		if err := data.auth.clients.UpdateClient("phone_number", "=", client); err != nil {
			return "", err
		}
		return notification.SendSMS(client.PhoneNumber.String, client.Code.String)
	}

	client, err := data.addClient(fmt.Sprint(randomCode))
	if err != nil {
		return "", err
	}
	return notification.SendSMS(client.PhoneNumber.String, client.Code.String)
}

func (data *sendCodeForSignup) addClient(code string) (*entities.Client, error) {
	client := &entities.Client{
		PhoneNumber: handles.NewNullString(data.phoneNumber),
		Code:        handles.NewNullString(code),
	}
	if err := data.auth.GetClients().
		CreateClient(client); err != nil {
		return nil, err
	}
	return client, nil
}
