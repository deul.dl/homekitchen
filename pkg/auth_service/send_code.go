package auth_service

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/internal/notification"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type sendCode interface {
	run() (string, error)
}

type sendCodeByPhoneNumber struct {
	sendCode

	auth  *authService
	value string
}

func (data *sendCodeByPhoneNumber) run() (string, error) {
	if err := data.auth.validPhoneNumber(data.value); err != nil {
		return "", err
	}

	rand.Seed(time.Now().Unix())
	randomCode := rand.Int63n(999999-100000) + 100000
	var client *entities.Client

	client, _ = data.auth.GetClients().
		GetClientByField("phone_number", data.value)

	if client == nil {
		client, err := data.addClient(fmt.Sprint(randomCode))
		if err != nil {
			return "", err
		}

		return notification.SendSMS(client.PhoneNumber.String, client.Code.String)
	} else {
		client.Code = handles.NewNullString(fmt.Sprint(randomCode))
		if err := data.auth.GetClients().UpdateClient("code", "uuid", client); err != nil {
			return "", err
		}

		return notification.SendSMS(client.PhoneNumber.String, client.Code.String)
	}
}

func (data *sendCodeByPhoneNumber) addClient(code string) (*entities.Client, error) {
	client := &entities.Client{
		PhoneNumber: handles.NewNullString(data.value),
		Code:        handles.NewNullString(code),
	}
	if err := data.auth.GetClients().
		CreateClient(client); err != nil {
		return nil, err
	}
	return client, nil
}

type sendCodeByUUID struct {
	sendCode

	auth  *authService
	value string
}

func (data *sendCodeByUUID) run() (string, error) {
	if data.value == "" {
		return "", errors.New("UUID has to be empty!")
	}

	rand.Seed(time.Now().Unix())
	randomCode := rand.Int63n(999999-100000) + 100000

	client, err := data.auth.GetClients().
		GetClientByField("uuid", data.value)
	if err != nil {
		return "", err
	}
	client.Code = handles.NewNullString(fmt.Sprint(randomCode))
	if err := data.auth.GetClients().UpdateClient("code", "uuid", client); err != nil {
		return "", err
	}

	sid, err := notification.SendSMS(client.PhoneNumber.String, client.Code.String)
	if err != nil {
		return "", err
	}

	return sid, nil
}

func newSendCode(auth *authService, value, name string) sendCode {
	if name == "uuid" {
		return &sendCodeByUUID{auth: auth, value: value}
	}
	return &sendCodeByPhoneNumber{auth: auth, value: value}
}
