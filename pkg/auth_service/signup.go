package auth_service

import (
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/internal/ojwt"
	"github.com/homekitchen/homekitchen/pkg/auth_service/password"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type SignUpInput struct {
	User     *entities.User
	Password string
}

type signup struct {
	auth *authService
	data *SignUpInput
}

func newSignup(auth *authService, data *SignUpInput) *signup {
	return &signup{auth, data}
}

func (signup *signup) run() (*AuthSuccess, error) {
	if err := signup.auth.validPassword(signup.data.Password); err != nil {
		return nil, err
	}

	passwordHash, err := password.HashPassword(signup.data.Password)
	if err != nil {
		return nil, err
	}
	user := signup.data.User
	user.PasswordHash = handles.NewNullString(passwordHash)

	client, err := signup.auth.GetClients().
		GetClientByField("phone_number", user.PhoneNumber.String)
	if err != nil {
		return nil, err
	}

	userUUID := client.UUID
	user.UUID = userUUID.String
	user.Executor.UUID = userUUID.String
	user.Status = handles.NewNullString(entities.VERIFICATED)
	user.Executor.ExecutorAddress.UUID = userUUID.String
	user.PasswordHash = handles.NewNullString(passwordHash)

	usersStorage := users.New(signup.auth.ctx)
	if err = usersStorage.CreateUser(user); err != nil {
		return nil, err
	}

	userWithRole, err := usersStorage.PutRole(user, entities.EXECUTOR)
	if err != nil {
		return nil, err
	}

	token, err := ojwt.CreateToken(&ojwt.TokenInput{
		UUID: userWithRole.UUID,
		Role: entities.EXECUTOR},
	)
	if err != nil {
		return nil, err
	}

	return &AuthSuccess{
		Token: token,
	}, nil
}
