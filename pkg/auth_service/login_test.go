package auth_service

import (
	"context"
	"testing"

	"github.com/joho/godotenv"
)

func TestLoginUsingVeritificationCode(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	phoneNumber := "+77056278085"

	_, err = New(ctx).SendCode(phoneNumber)
	if err != nil {
		t.Error(err)
		return
	}

	client, err := New(ctx).
		GetClients().
		GetClientByField("phone_number", phoneNumber)
	if err != nil {
		t.Error(err)
		return
	}

	authSuccess, err := New(ctx).Login(&LoginInput{
		PhoneNumber: "87056278085",
		Key:         client.Code.String,
		Type:        SMS_CODE_TYPE,
	})
	if err != nil {
		t.Error(err)
	}

	if authSuccess == nil {
		t.Error("Error")
		return
	}

}

func TestLoginUsingPassword(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	authSuccess, err := New(ctx).Login(&LoginInput{
		PhoneNumber: "+77056278888",
		Key:         "13579bosS@",
	})
	if err != nil {
		t.Error(err)
		return
	}

	if authSuccess == nil {
		t.Error("Error")
		return
	}
}
