package auth_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestSignUpSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	address := &entities.ExecutorAddress{
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("+6(UTC)"),
	}

	executor := &entities.Executor{
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		PhoneNumber: handles.NewNullString("+77056278085"),
		Email:       "vityadm199824@gmail.com",
		Executor:    executor,
	}

	if _, err = New(ctx).SignUp(&SignUpInput{
		User:     user,
		Password: "123456a",
	}); err != nil {
		t.Error(err)
		return
	}
}
