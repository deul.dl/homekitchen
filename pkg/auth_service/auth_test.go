package auth_service

import (
	"context"
	"testing"

	"github.com/joho/godotenv"
)

func TestVerificateCodeSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	code := "148370"
	phoneNumber := "+77056278085"
	err = New(ctx).VerificateCode(phoneNumber, code)

	if err != nil {
		t.Error(err)
		return
	}
}
