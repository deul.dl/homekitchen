package statistic_service

import (
	"context"
	"fmt"
	"time"

	"github.com/homekitchen/homekitchen/pkg/statistic_service/entities"
	"github.com/homekitchen/homekitchen/pkg/statistic_service/storage"
)

type StatisticService interface {
	GetDataByPeriod(from, to time.Time) ([]*entities.StatsData, error)
}

type statisticService struct {
	StatisticService

	ctx     context.Context
	storage storage.Storage
}

func New(ctx context.Context) StatisticService {
	return &statisticService{
		ctx:     ctx,
		storage: storage.New(ctx),
	}
}

func (statistic *statisticService) GetDataByPeriod(from, to time.Time) (data []*entities.StatsData, err error) {
	rows, err := storage.New(statistic.ctx).GetOrderByPeriod(from, to)
	if err != nil {
		return
	}
	for rows.Next() {
		stats := &entities.StatsData{}
		fmt.Println(stats)
		if err = rows.Scan(
			&stats.Profit,
			&stats.Revenue,
			&stats.OrderCounts,
			&stats.Day,
		); err != nil {
			return nil, err
		}

		data = append(data, stats)
	}

	rows.Close()
	if rows.Err() != nil {
		return data, rows.Err()
	}
	return
}
