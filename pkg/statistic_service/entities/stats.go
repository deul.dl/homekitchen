package entities

type StatsData struct {
	OrderCounts int64
	Profit      float64
	Revenue     float64
	Day         string
}

type key string

const USER_UUID key = "userUUID"
