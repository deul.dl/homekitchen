package storage

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/pkg/db"
	orderEntities "github.com/homekitchen/homekitchen/pkg/order_service/entities"
	"github.com/homekitchen/homekitchen/pkg/statistic_service/entities"
)

const OrderItemTable = "order_items"
const OrderPaymentMethodTable = "order_payment_methods"

type Storage interface {
	GetOrderByPeriod(from, to time.Time) (*sql.Rows, error)
}

type storage struct {
	Storage

	ctx      context.Context
	database *sql.DB
}

func New(ctx context.Context) Storage {

	return &storage{
		ctx:      ctx,
		database: db.Pool,
	}
}

func (storage *storage) GetOrderByPeriod(from, to time.Time) (rows *sql.Rows, err error) {
	query := fmt.Sprintf(`SELECT
				SUM(order_payment_methods.amount - order_payment_methods.total_cost) AS profit,
				SUM(order_payment_methods.amount) AS revenue,
				COUNT(order_payment_methods.*) AS order_counts,
				date_trunc('day', order_payment_methods.created_at) AS day
			FROM %s
			INNER JOIN orders
			ON orders.uuid = order_payment_methods.order_uuid
			AND orders.executor_uuid = $1
			AND orders.status = $2
			AND orders.time BETWEEN $3 AND $4
			GROUP BY day;
		`,
		OrderPaymentMethodTable)
	log.Println(query)
	userUUID := storage.ctx.Value(entities.USER_UUID).(string)
	return storage.database.
		QueryContext(storage.ctx,
			query,
			userUUID,
			orderEntities.COMPLETED_STATUS,
			from, to)
}
