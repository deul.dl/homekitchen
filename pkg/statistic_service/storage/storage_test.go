package storage

import (
	"context"
	"database/sql"
	"log"
	"testing"
	"time"

	"github.com/homekitchen/homekitchen/pkg/statistic_service/entities"
	"github.com/joho/godotenv"
)

func TestGetOrderByToday(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	ctx = context.WithValue(ctx, entities.USER_UUID, "aa063b26-9676-4a8a-8e5e-3d5c2ff32ec5")
	defer stop()
	now := time.Now()
	from := time.Date(now.Year(), now.Month(), 23, 0, 0, 0, 0, now.Location())
	to := from.Add(24 * time.Hour)

	rows, err := New(ctx).GetOrderByPeriod(from, to)
	if err != nil {
		t.Error(err)
	}
	defer rows.Close()
	stats := &entities.StatsData{}
	for rows.Next() {
		err = rows.Scan(
			&stats.OrderCounts,
			&stats.Profit,
			&stats.Revenue,
			&stats.Day,
		)
		if err != nil {
			t.Error(err)
		}
	}
	if err == sql.ErrNoRows {
		t.Error(err)
		return
	} else if err != nil {
		log.Printf("store: %v", err)
		t.Error(err)
		return
	}
}
