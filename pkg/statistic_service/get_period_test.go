package statistic_service

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/homekitchen/homekitchen/pkg/statistic_service/entities"
	"github.com/joho/godotenv"
)

func TestGetDataByPeriod(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	ctx = context.WithValue(ctx, entities.USER_UUID, "1e0225f1-fb11-4839-b378-7daf28351b1c")
	defer stop()
	now := time.Now()
	from := time.Date(now.Year(), time.July, 23, 0, 0, 0, 0, now.Location())
	to := from.Add(30 * time.Hour)

	listOfStats, err := New(ctx).GetDataByPeriod(from, to)
	if err != nil {
		t.Error(err)
	}

	var totalRevenue float64
	log.Println(listOfStats)
	for _, stats := range listOfStats {
		fmt.Println(stats.Revenue)
		totalRevenue += stats.Revenue
	}

	if totalRevenue != 246*9 {
		t.Errorf("Total Revenue: %f", totalRevenue)
	}
}
