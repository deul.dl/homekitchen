package user_service

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestCreateUserSuccess(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	CreateUserSuccess(t)
}

func CreateUserSuccess(t *testing.T) (*entities.User, error) {
	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	uuid := uuid.New().String()

	address := &entities.ExecutorAddress{
		UUID:     uuid,
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("+6(UTC)"),
	}

	executor := &entities.Executor{
		UUID:            uuid,
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuid,
		PhoneNumber:  handles.NewNullString("+77056278085"),
		Email:        "vityadm199824@gmail.com",
		Executor:     executor,
		PasswordHash: handles.NewNullString("123456a"),
		Status:       handles.NewNullString(entities.IN_MODERATION),
	}

	if err := New(ctx).GetStorage().
		CreateUser(user); err != nil {
		t.Error(err)
		return nil, err
	}
	return user, nil
}
