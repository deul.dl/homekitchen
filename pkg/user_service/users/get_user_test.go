package users

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestGetUserByUUIDSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()
	phoneNumber := "87777777771"

	address := &entities.ExecutorAddress{
		UUID:     uuidWithHyphen,
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("UTC(+6)"),
	}

	executor := &entities.Executor{
		UUID:            uuidWithHyphen,
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuidWithHyphen,
		PhoneNumber:  handles.NewNullString(phoneNumber),
		Email:        "vityadm199824@gmail.com",
		PasswordHash: handles.NewNullString("12das34343@#!25.6"),
		Executor:     executor,
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})

	if err = New(ctx).CreateUser(user); err != nil {
		t.Error(err)
		return
	}

	_, err = New(ctx).PutRole(user, EXECUTOR)
	if err != nil {
		t.Errorf("err %v", err)
		return
	}

	if user, err = New(ctx).GetUserByUUID(user.UUID); err != nil {
		t.Error(err)
		return
	}

	if user.UUID == "" {
		t.Error("Error is get user by phone Number")
		return
	}

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})
}

func TestGetRolesByUserSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()
	phoneNumber := "87777777771"

	address := &entities.ExecutorAddress{
		UUID:     uuidWithHyphen,
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("UTC(+6)"),
	}

	executor := &entities.Executor{
		UUID:            uuidWithHyphen,
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuidWithHyphen,
		PhoneNumber:  handles.NewNullString(phoneNumber),
		Email:        "vityadm199824@gmail.com",
		PasswordHash: handles.NewNullString("12das34343@#!25.6"),
		Executor:     executor,
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	// db.NewODB(ctx).TruncateTables(
	// 	[]string{
	// 		UserTable,
	// 		ExecutorTable,
	// 		ExecutorAddressTable,
	// 		UserRolesTable,
	// 	})

	users := New(ctx)
	if err = users.CreateUser(user); err != nil {
		t.Error(err)
	}

	_, err = users.PutRole(user, EXECUTOR)
	if err != nil {
		t.Errorf("err %v", err)
		return
	}

	if user, err = users.GetUserByPhoneNumber(phoneNumber); err != nil {
		t.Error(err)
		return
	}

	if user.UUID == "" {
		t.Error("Error is get user by phone Number")
		return
	}

	role, err := users.GetRoleForUser(user)
	if err != nil {
		t.Error(err)
		return
	}

	if role.Name == "" {
		t.Error("User has to have role")
	}

	if role.Name != entities.EXECUTOR {
		t.Error("role is not executor")
	}
}
