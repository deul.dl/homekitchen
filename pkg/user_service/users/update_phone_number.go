package users

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type updatePhoneNumber struct {
	ctx      context.Context
	database *sql.DB

	client *entities.Client
}

func newUpdatePhoneNumber(ctx context.Context, database *sql.DB, client *entities.Client) *updatePhoneNumber {
	return &updatePhoneNumber{ctx: ctx, database: database, client: client}
}

func (action *updatePhoneNumber) run() error {
	action.beforeUpdate()

	return action.save()
}

func (action *updatePhoneNumber) beforeUpdate() {
	action.client.UpdatedAt = time.Now()
}

func (action *updatePhoneNumber) save() (err error) {

	tx, err := action.database.BeginTx(action.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("users: %v\n", err)
		return
	}

	if err = action.saveRecord(tx, ClientTable); err != nil {
		return
	}

	if err = action.saveRecord(tx, UserTable); err != nil {
		return
	}

	if err = tx.Commit(); err != nil {
		log.Printf("users: %v\n", err)
		return
	}
	return
}

func (action *updatePhoneNumber) saveRecord(tx *sql.Tx, table string) (err error) {
	query := fmt.Sprintf(
		`UPDATE %s SET phone_number = $1, updated_at = $2 WHERE "uuid" = $3`,
		table,
	)
	updated, err := action.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		tx.Rollback()
		log.Printf("users: unable to rollback: %v\n", err)
		return
	}

	_, err = tx.
		StmtContext(action.ctx, updated).
		Exec(
			action.client.PhoneNumber,
			action.client.UpdatedAt,
			action.client.UUID)
	if err != nil {
		tx.Rollback()
		log.Printf("users: update executor failed: %v\n", err)
	}
	return
}
