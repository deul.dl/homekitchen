package users

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestCreateUserSuccess(t *testing.T) {

	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	address := &entities.ExecutorAddress{
		UUID:     uuidWithHyphen,
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("+6(UTC)"),
	}

	executor := &entities.Executor{
		UUID:            uuidWithHyphen,
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuidWithHyphen,
		PhoneNumber:  handles.NewNullString("87056278085"),
		Email:        "vityadm199824@gmail.com",
		PasswordHash: handles.NewNullString("12das34343@#!25.6"),
		Executor:     executor,
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})

	err = New(ctx).CreateUser(user)

	if err != nil {
		t.Errorf("err %v", err)
	}

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})
}
