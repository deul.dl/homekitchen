package users

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestPutRoleSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	address := &entities.ExecutorAddress{
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("UTC(+6)"),
	}

	executor := &entities.Executor{
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuidWithHyphen,
		PhoneNumber:  handles.NewNullString("87056278085"),
		Email:        "vityadm199824@gmail.com",
		PasswordHash: handles.NewNullString("12das34343@#!25.6"),
		Executor:     executor,
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	users := New(ctx)

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})

	err = users.CreateUser(user)
	if err != nil {
		t.Errorf("err %v", err)

	}

	ur, err := users.PutRole(user, ADMIN)
	if err != nil {
		t.Errorf("err %v", err)
	}

	if ur.Roles[0].Name != ADMIN {
		t.Error("role has to be admin")
	}

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})
}

func TestPutRoleFailed(t *testing.T) {
	err := godotenv.Load("../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	uuidWithHyphen := uuid.New().String()

	address := &entities.ExecutorAddress{
		City: handles.NewNullString("Topar"),
	}

	executor := &entities.Executor{
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuidWithHyphen,
		PhoneNumber:  handles.NewNullString("87056278085"),
		Email:        "vityadm199824@gmail.com",
		PasswordHash: handles.NewNullString("12das34343@#!25.6"),
		Executor:     executor,
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	users := New(ctx)

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})

	err = users.CreateUser(user)
	if err != nil {
		t.Errorf("err %v", err)

	}

	ur, err := users.PutRole(user, EXECUTOR)
	if err != nil {
		t.Errorf("err %v", err)
	}

	if ur.Roles[0].Name != ADMIN {
		t.Error("role has to be admin")
	}

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})
}
