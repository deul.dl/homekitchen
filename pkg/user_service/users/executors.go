package users

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type Executors interface {
	Update(*entities.Executor) error
	GetExecutorAddressByExecutorUUID(string) (*entities.ExecutorAddress, error)
}

type executors struct {
	Executors

	ctx      context.Context
	database *sql.DB
}

func NewExecutors(ctx context.Context, database *sql.DB) Executors {
	return &executors{
		ctx:      ctx,
		database: database,
	}
}

func (storage *executors) Update(executor *entities.Executor) error {
	return newUpdateExecutor(executor, storage.database, storage.ctx).run()
}

func (storage *executors) GetExecutorAddressByExecutorUUID(userUUID string) (*entities.ExecutorAddress, error) {
	query := fmt.Sprintf(`
				SELECT
					uuid, city
				FROM %s
				WHERE uuid = $1
				LIMIT 1
			`,
		ExecutorAddressTable)
	row := storage.database.
		QueryRowContext(storage.ctx, query, userUUID)
	if row.Err() != nil {
		return nil, row.Err()
	}

	executorAddress := &entities.ExecutorAddress{}
	err := row.Scan(
		&executorAddress.UUID,
		&executorAddress.City)

	if err == sql.ErrNoRows {
		return nil, errors.New("Executor address is not exits")
	} else if err != nil {
		return nil, err
	}

	return executorAddress, err
}
