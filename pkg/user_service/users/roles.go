package users

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

const (
	EXECUTOR = "Executor"
	ADMIN    = "Administation"
	CLIENT   = "Customer"
)

const (
	RolesTable     = "roles"
	UserRolesTable = "user_roles"
)

type UserRoles interface {
	PutRole(name string) (*entities.UserWithRole, error)
	GetRoleForUser() (role *entities.Role, err error)
}

type userRoles struct {
	UserRoles
	entities.Role

	ctx      context.Context
	database *sql.DB
	user     *entities.User
}

func NewRoles(user *entities.User, database *sql.DB, ctx context.Context) UserRoles {
	return &userRoles{
		user:     user,
		database: database,
		ctx:      ctx,
		Role:     entities.Role{Name: CLIENT},
	}
}

func (storage *userRoles) PutRole(name string) (userWithRole *entities.UserWithRole, err error) {

	tx, err := storage.database.BeginTx(storage.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("roles: %v", err)
		return
	}

	query := fmt.Sprintf("SELECT id FROM %s WHERE name=$1 LIMIT 1", RolesTable)
	role := &entities.Role{}
	if err = storage.database.
		QueryRowContext(storage.ctx, query, name).
		Scan(&role.ID); err != nil {
		log.Printf("roles: select failed: %v", err)
	}
	if err != nil {
		return
	}

	query = fmt.Sprintf("INSERT INTO user_roles (user_uuid, role_id) VALUES($1, $2)")
	inserted, err := storage.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("roles: unable to rollback: %v\n", err)
		return
	}

	_, err = tx.StmtContext(storage.ctx, inserted).Exec(storage.user.UUID, role.ID)
	if err != nil {
		log.Printf("roles: create failed: %v\n", err)
		return
	}

	if err = tx.Commit(); err != nil {
		log.Println(err)
		return
	}
	userWithRole = &entities.UserWithRole{
		User:  *storage.user,
		Roles: []*entities.Role{role},
	}
	return
}

func (storage *userRoles) GetRoleForUser() (role *entities.Role, err error) {
	query := fmt.Sprintf(`
		SELECT
			id, name
		FROM %s
		INNER JOIN
			%s
		ON
			roles.id=user_roles.role_id
		AND user_roles.user_uuid = $1
	`, RolesTable, UserRolesTable)

	row := storage.database.
		QueryRowContext(storage.ctx, query, storage.user.UUID)
	if row.Err() != nil {
		return nil, row.Err()
	}

	role = &entities.Role{}
	err = row.Scan(&role.ID, &role.Name)
	if err == sql.ErrNoRows {
		return nil, errors.New("User doesn't have this role")
	} else if err != nil {
		log.Printf("users: %v\n", err)
		return nil, err
	}

	return
}
