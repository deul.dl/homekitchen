package users

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdatePhoneNumberSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	address := &entities.ExecutorAddress{
		UUID:     uuidWithHyphen,
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("UTC(+6)"),
	}
	executor := &entities.Executor{
		UUID:            uuidWithHyphen,
		FirstName:       handles.NewNullString("Victor"),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuidWithHyphen,
		PhoneNumber:  handles.NewNullString("+77056278081"),
		Email:        "vityadm199824@gmail.com",
		PasswordHash: handles.NewNullString("12das34343@#!25.6"),
		Executor:     executor,
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
			ClientTable,
		})

	users := New(ctx)

	err = users.CreateUser(user)
	if err != nil {
		t.Errorf("err %v", err)
	}

	CreateClientSuccess(t, &entities.Client{
		UUID:        handles.NewNullString(uuidWithHyphen),
		PhoneNumber: handles.NewNullString("+77056278081"),
	})

	if err = users.UpdatePhoneNumber(&entities.Client{
		UUID:        handles.NewNullString(uuidWithHyphen),
		PhoneNumber: handles.NewNullString("+77056278085"),
	}); err != nil {
		t.Errorf("err %v", err)
	}

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
			ClientTable,
		})
}
