package users

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestSavePutRoleSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	client := &entities.Client{
		UUID:        handles.NewNullString(uuidWithHyphen),
		PhoneNumber: handles.NewNullString("+77156278085"),
		Code:        handles.NewNullString("123456"),
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = NewClients(ctx).CreateClient(client)
	if err != nil {
		t.Error(err)
		return
	}

	if err = NewClients(ctx).SaveNotificationToken(client.UUID.String, "Token Test"); err != nil {
		t.Error(err)
		return
	}
}
