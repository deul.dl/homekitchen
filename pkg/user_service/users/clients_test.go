package users

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func CreateClientSuccess(t *testing.T, client *entities.Client) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	client.Code = handles.NewNullString("123456")
	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	clients := NewClients(ctx)
	if err = clients.CreateClient(client); err != nil {
		t.Errorf("err %v", err)
		return
	}

	if client.Code.String == "" {
		t.Errorf("Code is Requied")
		return
	}
}

func TestCreateClientSuccess(t *testing.T) {
	CreateClientSuccess(t, &entities.Client{
		PhoneNumber: handles.NewNullString("+77056278085"),
		UUID:        handles.NewNullString(uuid.New().String()),
	})

	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	db.NewODB(ctx).TruncateTables([]string{ClientTable})
}

func TestUpdateClientSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	client := &entities.Client{
		UUID:        handles.NewNullString(uuidWithHyphen),
		PhoneNumber: handles.NewNullString("87056278087"),
		Code:        handles.NewNullString("123465"),
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	err = NewClients(ctx).CreateClient(client)

	if err != nil {
		t.Errorf("err %v", err)
	}

	if client.Code.String == "" {
		t.Errorf("Code is Requied")
	}
	code := "111111"
	err = NewClients(ctx).UpdateClient("code", "phone_number", &entities.Client{
		UUID:        handles.NewNullString(uuidWithHyphen),
		PhoneNumber: handles.NewNullString("87056278087"),
		Code:        handles.NewNullString(code),
	})

	if client.Code.String == code {
		t.Errorf("Code has to not math")
	}

	db.NewODB(ctx).TruncateTables([]string{ClientTable})
}

func TestGetClientByPhoneNumberSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	UUIDWithHyphen := uuid.New().String()

	phoneNumber := "87056278088"
	client := &entities.Client{
		UUID:        handles.NewNullString(UUIDWithHyphen),
		PhoneNumber: handles.NewNullString(phoneNumber),
		Code:        handles.NewNullString("148370"),
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	db.NewODB(ctx).TruncateTables([]string{ClientTable})

	clients := NewClients(ctx)

	err = clients.CreateClient(client)

	_, err = clients.GetClientByField("phone_number", client.PhoneNumber.String)
	if err != nil {
		t.Errorf("err %v", err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{ClientTable})
}

func TestGetClientByPhoneNumberFailure(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Errorf("Error loading .env file")
		return
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	client, err := NewClients(ctx).
		GetClientByField("phone_number", "+7777777777")
	fmt.Println(client)
	if err != nil {
		t.Errorf("err %v", err)
		return
	}

	db.NewODB(ctx).TruncateTables([]string{ClientTable})
}
