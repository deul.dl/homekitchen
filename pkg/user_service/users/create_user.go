package users

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type creatorUser struct {
	data     *entities.User
	database *sql.DB
	ctx      context.Context
}

func (creater *creatorUser) run() (err error) {
	tx, err := creater.database.BeginTx(creater.ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Printf("users: %v", err)
		return
	}

	if err = creater.addUser(tx); err != nil {
		return
	}

	if err = creater.addExecutor(tx); err != nil {
		return
	}

	if err = creater.addAddress(tx); err != nil {
		return
	}

	if err = tx.Commit(); err != nil {
		log.Printf("users: %v\n", err)
	}
	return
}

func (creater *creatorUser) addUser(tx *sql.Tx) (err error) {
	query := fmt.Sprintf(
		`INSERT INTO %s ("uuid", status, phone_number, email, password_hash) VALUES($1, $2, $3, $4, $5)`,
		UserTable,
	)
	inserted, err := creater.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("users: unable to rollback: %v\n", err)
		return
	}

	_, err = tx.
		StmtContext(creater.ctx, inserted).
		Exec(
			creater.data.UUID,
			creater.data.Status,
			creater.data.PhoneNumber,
			creater.data.Email,
			creater.data.PasswordHash)
	if err != nil {
		tx.Rollback()
		log.Printf("users: add user failed: %v\n", err)
	}

	return
}

func (creater *creatorUser) addExecutor(tx *sql.Tx) (err error) {
	executor := creater.data.Executor
	query := fmt.Sprintf(
		`INSERT INTO %s ("uuid", iin, first_name, last_name) VALUES($1, $2, $3, $4)`,
		ExecutorTable,
	)
	inserted, err := creater.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		tx.Rollback()
		log.Printf("users: unable to rollback: %v\n", err)
		return
	}

	_, err = tx.
		StmtContext(creater.ctx, inserted).
		Exec(executor.UUID, executor.IIN,
			executor.FirstName, executor.LastName)
	if err != nil {
		tx.Rollback()
		log.Printf("users: add executor failed: %v\n", err)
	}
	return
}

func (creater *creatorUser) addAddress(tx *sql.Tx) (err error) {
	address := creater.data.Executor.ExecutorAddress
	query := fmt.Sprintf(`
		INSERT INTO %s ("uuid", city, timezone, longitude, latitude, location_code)
		VALUES($1, $2, $3, $4, $5, $6)
	`, ExecutorAddressTable)

	inserted, err := creater.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("users: unable to rollback: %v\n", err)
		return
	}

	_, err = tx.
		StmtContext(creater.ctx, inserted).
		Exec(
			address.UUID,
			address.City,
			address.Timezone,
			address.Longitude,
			address.Latitude,
			address.LocationCode,
		)
	if err != nil {
		tx.Rollback()
		log.Printf("users: add address failed: %v\n", err)
	}
	return
}
