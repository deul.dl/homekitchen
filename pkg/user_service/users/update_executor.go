package users

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

type updateExecutor struct {
	*entities.Executor

	ctx      context.Context
	database *sql.DB
}

func newUpdateExecutor(executor *entities.Executor, database *sql.DB, ctx context.Context) *updateExecutor {
	return &updateExecutor{
		Executor: executor,
		database: database,
		ctx:      ctx,
	}
}

func (storage *updateExecutor) run() error {
	storage.beforeUpdate()
	return storage.save()
}

func (storage *updateExecutor) beforeUpdate() {
	storage.Executor.UpdatedAt = time.Now()
}

func (storage *updateExecutor) save() error {
	query := fmt.Sprintf(
		`UPDATE %s SET first_name = $1, last_name = $2, iin = $3, updated_at = $4 WHERE "uuid" = $5`,
		ExecutorTable,
	)
	updated, err := storage.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("executor: unable to rollback: %v\n", err)
		return err
	}

	_, err = updated.Exec(
		storage.Executor.FirstName,
		storage.Executor.LastName,
		storage.Executor.IIN,
		storage.Executor.UpdatedAt,
		storage.Executor.UUID)
	if err != nil {
		log.Printf("executor: add user failed: %v\n", err)
		return err
	}
	return nil
}
