package users

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

const ClientTable = "clients"

// Clients : interface of StorageClient.
type Clients interface {
	CreateClient(client *entities.Client) error
	UpdateClient(field string, where string, client *entities.Client) error
	GetClientByField(field, value string) (*entities.Client, error)
	SaveNotificationToken(userUUID, notificationToken string) error
}

type clientsStorage struct {
	Clients

	ctx      context.Context
	database *sql.DB
}

// NewClients : create storoge clients.
func NewClients(ctx context.Context) Clients {

	return &clientsStorage{ctx: ctx, database: db.Pool}
}

func (cl *clientsStorage) UpdateClient(field string, by string, client *entities.Client) (err error) {
	query := fmt.Sprintf(
		"UPDATE %s SET %s = $1 WHERE %s = $2",
		ClientTable,
		field,
		by,
	)

	updated, err := cl.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("users: unable to rollback: %v\n", err)
		return
	}

	if _, err = updated.ExecContext(
		cl.ctx,
		client.Code,
		client.UUID,
	); err != nil {
		log.Printf("users: add customer failed: %v\n", err)
	}

	return
}

func (cl *clientsStorage) CreateClient(client *entities.Client) (err error) {
	var query string
	query = fmt.Sprintf(`
		INSERT INTO %s (phone_number, code) VALUES($1, $2) RETURNING "uuid";
	`, ClientTable)

	inserted, err := cl.database.Prepare(query)
	defer inserted.Close()
	if err != nil {
		log.Printf("users: unable to rollback: %v\n", err)
		return
	}

	if err = inserted.
		QueryRow(
			client.PhoneNumber,
			client.Code,
		).Scan(&client.UUID); err != nil {
		log.Printf("users: add customer failed: %v\n", err)
	}
	return
}

func (cl *clientsStorage) GetClientByField(field, value string) (*entities.Client, error) {
	query := fmt.Sprintf(
		`SELECT "uuid", code, phone_number, divice_token FROM %s WHERE %s = $1 LIMIT 1`, ClientTable, field)

	stmt, err := cl.database.PrepareContext(cl.ctx, query)
	defer stmt.Close()

	if err != nil {
		log.Printf("odb: %v\n", err)
		return nil, err
	}

	client := &entities.Client{PhoneNumber: handles.NewNullString(value)}
	err = stmt.QueryRowContext(cl.ctx, value).
		Scan(
			&client.UUID,
			&client.Code,
			&client.PhoneNumber,
			&client.NotificationToken)
	if err == sql.ErrNoRows {
		return nil, errors.New("Client is not exits")
	} else if err != nil {
		log.Printf("odb: %v\n", err)
		return nil, err
	}

	return client, err
}

func (storage *clientsStorage) SaveNotificationToken(uuid, notificationToken string) error {
	query := fmt.Sprintf(
		`UPDATE %s SET divice_token = $1, updated_at = $2 WHERE "uuid" = $3`,
		ClientTable,
	)
	updated, err := storage.database.
		Prepare(query)
	if err != nil {
		log.Printf("executor: unable to rollback: %v\n", err)
		return err
	}
	defer updated.Close()

	_, err = updated.Exec(notificationToken, time.Now(), uuid)
	if err != nil {
		log.Printf("clients: add user failed: %v\n", err)
		return err
	}
	return nil
}
