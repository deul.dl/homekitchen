package users

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
)

const (
	UserTable            = "users"
	ExecutorTable        = "executors"
	ExecutorAddressTable = "executor_addresses"
)

type Users interface {
	CreateUser(user *entities.User) error
	PutRole(user *entities.User, name string) (*entities.UserWithRole, error)
	GetUserByPhoneNumber(phoneNumber string) (*entities.User, error)
	GetRoleForUser(user *entities.User) (*entities.Role, error)
	GetRoles(user *entities.User) UserRoles
	GetUserByUUID(uuid string) (user *entities.User, err error)
	UpdateExecutor(executor *entities.Executor) error
	UpdateEmail(uuid, email string) error
	UpdateStatus(uuid, email string) error
	GetExecutors() Executors
	UpdatePhoneNumber(client *entities.Client) error
}

type userStorage struct {
	Users

	ctx      context.Context
	database *sql.DB
}

func New(ctx context.Context) Users {

	return &userStorage{ctx: ctx, database: db.Pool}
}

func (storage *userStorage) GetUserByUUID(uuid string) (user *entities.User, err error) {
	query := fmt.Sprintf(`
			SELECT
				"users"."uuid",
				"users"."email",
				"users"."phone_number",
				"users"."password_hash",
				"users"."status",
				"executors"."first_name",
				"executors"."last_name"
			FROM %s
			INNER JOIN
				%s
			ON
				"executors"."uuid" = "users"."uuid"
			WHERE "users"."uuid"=$1
			LIMIT 1
		`,
		UserTable, ExecutorTable)
	row := storage.database.
		QueryRowContext(storage.ctx, query, uuid)
	if row.Err() != nil {
		return nil, row.Err()
	}

	user = &entities.User{Executor: &entities.Executor{}}
	err = row.Scan(
		&user.UUID,
		&user.Email,
		&user.PhoneNumber,
		&user.PasswordHash,
		&user.Status,
		&user.Executor.FirstName,
		&user.Executor.LastName)

	if err == sql.ErrNoRows {
		return nil, errors.New("User is not exits")
	} else if err != nil {
		log.Printf("users: %v\n", err)
		return nil, err
	}
	return
}

func (storage *userStorage) GetUserByPhoneNumber(phoneNumber string) (user *entities.User, err error) {
	query := fmt.Sprintf(`
			SELECT
				uuid,
				email,
				phone_number,
				password_hash,
				status
			FROM %s
			WHERE phone_number = $1
			LIMIT 1
		`,
		UserTable)
	row := storage.database.
		QueryRowContext(storage.ctx, query, phoneNumber)
	if row.Err() != nil {
		return nil, row.Err()
	}

	user = &entities.User{}
	err = row.Scan(
		&user.UUID,
		&user.Email,
		&user.PhoneNumber,
		&user.PasswordHash,
		&user.Status)

	if err == sql.ErrNoRows {
		return nil, errors.New("User is not exits")
	} else if err != nil {
		log.Printf("users: %v\n", err)
		return nil, err
	}
	return
}

func (storage *userStorage) UpdateEmail(uuid string, email string) error {
	query := fmt.Sprintf(
		`UPDATE %s SET email = $1, updated_at = $2 WHERE "uuid" = $3`,
		UserTable,
	)
	updated, err := storage.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("users: unable to rollback: %v\n", err)
		return err
	}

	_, err = updated.Exec(
		handles.NewNullString(email),
		time.Now(),
		handles.NewNullString(uuid),
	)
	if err != nil {
		log.Printf("users: update executor failed: %v\n", err)
		return err
	}
	return nil
}

func (storage *userStorage) UpdateStatus(uuid string, status string) error {
	query := fmt.Sprintf(
		`UPDATE %s SET status = $1, updated_at = $2 WHERE "uuid" = $3`,
		UserTable,
	)
	updated, err := storage.database.Prepare(query)
	defer updated.Close()
	if err != nil {
		log.Printf("users: unable to rollback: %v\n", err)
		return err
	}

	_, err = updated.Exec(
		handles.NewNullString(status),
		time.Now(),
		handles.NewNullString(uuid),
	)
	if err != nil {
		log.Printf("users: update users failed: %v\n", err)
		return err
	}
	return nil
}

func (storage *userStorage) CreateUser(user *entities.User) error {
	creator := &creatorUser{data: user, database: storage.database, ctx: storage.ctx}
	return creator.run()
}

func (storage *userStorage) PutRole(user *entities.User, name string) (userWithRole *entities.UserWithRole, err error) {
	return storage.GetRoles(user).PutRole(name)
}

func (storage *userStorage) GetRoleForUser(user *entities.User) (*entities.Role, error) {
	return storage.GetRoles(user).GetRoleForUser()
}

func (storage *userStorage) UpdateExecutor(executor *entities.Executor) error {
	return storage.GetExecutors().Update(executor)
}

func (storage *userStorage) UpdatePhoneNumber(client *entities.Client) error {
	return newUpdatePhoneNumber(storage.ctx, storage.database, client).run()
}

func (storage *userStorage) GetRoles(user *entities.User) UserRoles {
	return NewRoles(user, storage.database, storage.ctx)
}

func (storage *userStorage) GetExecutors() Executors {
	return NewExecutors(storage.ctx, storage.database)
}
