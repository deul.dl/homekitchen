package users

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/homekitchen/homekitchen/internal/handles"
	"github.com/homekitchen/homekitchen/pkg/db"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/joho/godotenv"
)

func TestUpdateStatusSuccess(t *testing.T) {
	err := godotenv.Load("../../../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
		return
	}

	uuidWithHyphen := uuid.New().String()

	address := &entities.ExecutorAddress{
		UUID:     uuidWithHyphen,
		City:     handles.NewNullString("Topar"),
		Timezone: handles.NewNullString("UTC(+6)"),
	}
	firstName := "Victor"
	executor := &entities.Executor{
		UUID:            uuidWithHyphen,
		FirstName:       handles.NewNullString(firstName),
		LastName:        handles.NewNullString("Dmitrishen"),
		ExecutorAddress: address,
	}

	user := &entities.User{
		UUID:         uuidWithHyphen,
		PhoneNumber:  handles.NewNullString("87056278085"),
		Email:        "vityadm199824@gmail.com",
		PasswordHash: handles.NewNullString("12das34343@#!25.6"),
		Executor:     executor,
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop()
	users := New(ctx)

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})

	err = users.CreateUser(user)
	if err != nil {
		t.Errorf("err %v", err)

	}
	executor.FirstName = handles.NewNullString("Artem")
	err = users.UpdateEmail(user.UUID, "deul.dl@gmail.com")
	if err != nil {
		t.Errorf("err %v", err)
	}

	if executor.FirstName.String == firstName {
		t.Errorf("First Name doesn't hava to be similar")
	}

	db.NewODB(ctx).TruncateTables(
		[]string{
			UserTable,
			ExecutorTable,
			ExecutorAddressTable,
			UserRolesTable,
		})
}
