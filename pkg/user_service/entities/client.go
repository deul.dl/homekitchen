package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type Client struct {
	UUID              handles.NullString
	PhoneNumber       handles.NullString
	Code              handles.NullString
	NotificationToken handles.NullString
	CreatedAt         time.Time
	UpdatedAt         time.Time
}
