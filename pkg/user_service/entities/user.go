package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

type User struct {
	UUID         string
	PhoneNumber  handles.NullString
	PasswordHash handles.NullString
	Email        string
	Status       handles.NullString
	CreatedAt    time.Time
	UpdatedAt    time.Time
	Executor     *Executor
}

type UserWithRole struct {
	User
	Roles []*Role
}

const CUSTOMER = "Customer"
const EXECUTOR = "Executor"
const ADMIN = "Admin"

const IN_MODERATION = "in_moderation"
const BANNED = "banned"
const REFUSED = "refused"
const VERIFICATED = " verificated"
