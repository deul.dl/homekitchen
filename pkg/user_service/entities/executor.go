package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

//Executor : Bissenes Modal
type Executor struct {
	UUID            string
	FirstName       handles.NullString
	LastName        handles.NullString
	IIN             handles.NullString
	CreatedAt       time.Time
	UpdatedAt       time.Time
	ExecutorAddress *ExecutorAddress
}
