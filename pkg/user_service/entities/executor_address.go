package entities

import (
	"time"

	"github.com/homekitchen/homekitchen/internal/handles"
)

//ExecutorAddress : Bissenes Modal
type ExecutorAddress struct {
	UUID         string
	City         handles.NullString
	Timezone     handles.NullString
	Longitude    float64
	Latitude     float64
	LocationCode string
	CreatedAt    time.Time
	UpdatedAt    time.Time
}
