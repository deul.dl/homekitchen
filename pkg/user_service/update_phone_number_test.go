package user_service

import (
	"context"
	"testing"

	"github.com/homekitchen/homekitchen/pkg/auth_service"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
	"github.com/joho/godotenv"
)

func TestUpdatePhoneNumberSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	_, err = auth_service.New(ctx).SendCode("+77056278085")
	if err != nil {
		t.Error(err)
	}

	client, err := users.NewClients(ctx).
		GetClientByField("phone_number", "+77056278085")
	if err != nil {
		t.Error(err)
	}

	err = New(ctx).UpdatePhoneNumber(client, "+77056278085")
	if err != nil {
		t.Error(err)
	}
}

func TestRefuseSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	user, err := CreateUserSuccess(t)
	if err != nil {
		t.Error(err)
		return
	}

	err = New(ctx).RefuseModeration(user.UUID)
	if err != nil {
		t.Error(err)
	}
}

func TestBanSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	user, err := CreateUserSuccess(t)
	if err != nil {
		t.Error(err)
		return
	}

	err = New(ctx).RefuseModeration(user.UUID)
	if err != nil {
		t.Error(err)
	}
}

func TestVerifacateSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	user, err := CreateUserSuccess(t)
	if err != nil {
		t.Error(err)
		return
	}

	err = New(ctx).RefuseModeration(user.UUID)
	if err != nil {
		t.Error(err)
	}
}
