package user_service

import (
	"context"
	"fmt"
	"testing"

	"github.com/joho/godotenv"
)

func TestGetUserByUUIDSuccess(t *testing.T) {
	err := godotenv.Load("../.test.env")
	if err != nil {
		t.Error("Error loading .env file")
	}
	ctx, stop := context.WithCancel(context.Background())
	defer stop() //148370
	user, err := New(ctx).GetUserRoles("6aa33b3f-a889-490c-b1c0-da41224c6328")
	if err != nil {
		t.Error(err)
	}
	fmt.Print(user.Roles[0].Name)
	if user.UUID == "6aa33b3f-a889-490c-b1c0-da41224c6328" {
		t.Error("Error in GetUserRoles")
	}
}
