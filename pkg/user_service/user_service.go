package user_service

import (
	"context"
	"errors"

	"github.com/homekitchen/homekitchen/pkg/auth_service"
	"github.com/homekitchen/homekitchen/pkg/user_service/entities"
	"github.com/homekitchen/homekitchen/pkg/user_service/users"
)

type UserService interface {
	GetStorage() users.Users
	GetUserRoles(uuid string) (*entities.UserWithRole, error)
	UpdatePhoneNumber(client *entities.Client, oldPhoneNumber string) error
	UpdatePassword(uuid string, password string) error
	UpdateEmail(uuid string, email string) error
	Ban(uuid string) error
	RefuseModeration(uuid string) error
	Verificate(uuid string) error
	SaveFMCToken(clientUUID, token string) error
}

func New(ctx context.Context) UserService {

	return &userService{ctx: ctx, users: users.New(ctx)}
}

type userService struct {
	UserService

	ctx   context.Context
	users users.Users
}

func (service *userService) GetUserRoles(uuid string) (*entities.UserWithRole, error) {
	if uuid == "" {
		return nil, errors.New("UUID doesn't have to be empty")
	}
	user, err := service.GetStorage().GetUserByUUID(uuid)
	if err != nil {
		return nil, err
	}

	role, err := service.GetStorage().GetRoleForUser(user)
	if err != nil {
		return nil, err
	}

	return &entities.UserWithRole{
		User:  *user,
		Roles: []*entities.Role{role, &entities.Role{Name: entities.EXECUTOR}},
	}, nil
}

func (service *userService) UpdatePhoneNumber(client *entities.Client, oldPhoneNumber string) (err error) {
	if err = auth_service.
		New(service.ctx).
		VerificateCode(oldPhoneNumber, client.Code.String); err != nil {
		return
	}

	if err = service.users.UpdatePhoneNumber(client); err != nil {
		return
	}
	return
}

func (service *userService) Ban(uuid string) (err error) {
	if err = service.GetStorage().
		UpdateStatus(uuid, entities.BANNED); err != nil {
		return
	}
	return
}

func (service *userService) RefuseModeration(uuid string) (err error) {

	if err = service.GetStorage().
		UpdateStatus(uuid, entities.REFUSED); err != nil {
		return
	}
	return
}

func (service *userService) Verificate(uuid string) (err error) {
	if err = service.GetStorage().
		UpdateStatus(uuid, entities.VERIFICATED); err != nil {
		return
	}
	return
}

func (service *userService) SaveFMCToken(clientUUID, token string) error {
	return users.NewClients(service.ctx).
		SaveNotificationToken(clientUUID, token)
}

func (service *userService) GetStorage() users.Users {
	return service.users
}
